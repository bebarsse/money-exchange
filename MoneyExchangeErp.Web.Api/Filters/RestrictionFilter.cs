﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MoneyExchangeErp.Web.Api.Filters
{
    public class RestrictionFilter : ActionFilterAttribute
    {
        private string Roles;

        public RestrictionFilter(string Roles)
        {
            this.Roles = Roles;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            bool access = false;
            foreach (var role in Roles.Split('|'))
            {
                if (HttpContext.Current.User.IsInRole(role)) access = true;
            }
            if (access == false) actionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("Action non autorisée. Veuillez contacter le support technique."),
                ReasonPhrase = "Bad Request"
            };
            base.OnActionExecuting(actionContext);
        }
    }
}