﻿using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MoneyExchangeErp.Web.Api.Filters
{
    public class ExceptionHandlingFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            //Log Exception in Database
            UnityConfig.Resolve<IAppExceptionService>().Repository.Create(new Core.DTO.AppExceptionModel
            {
                Message = context.Exception.Message,
                StackTrace = context.Exception.StackTrace,
            });

            if (context.Exception is BusinessException)
            {
                var businessExceptionHandled = context.Exception as BusinessException;
                var message = string.Empty;
                switch (businessExceptionHandled.Type)
                {
                    case BusinessException.BusinessExceptionType.UnknownError:
                        message = "Une erreur s'est produite. Veuillez réessayer ou contacter le support technique.";
                        break;
                    case BusinessException.BusinessExceptionType.ActionUnauthorized:
                        message = "Action non autorisée";
                        break;
                    case BusinessException.BusinessExceptionType.UplodingError:
                        message = "Une erreur s'est produite lors du transfert du fichier. Veuillez contacter le support technique.";
                        break;
                    case BusinessException.BusinessExceptionType.CustomerExist:
                        message = "Le client existe déjà";
                        break;
                    case BusinessException.BusinessExceptionType.UsernameExist:
                        message = "Nom d'utilisateur existe déjà, Veuillez choisir un autre nom.";
                        break;
                    case BusinessException.BusinessExceptionType.IncorrectPassword:
                        message = "Le mot de passe actuel est incorrect";
                        break;
                    default:
                        break;
                }
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(message),
                    ReasonPhrase = "Exception"
                });

            }

            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent("Une erreur s'est produite. Veuillez réessayer ou contacter le support technique."),
                ReasonPhrase = "Critical Exception"
            });
        }
    }
}