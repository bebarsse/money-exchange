﻿using AutoMapper;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.App_Start;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace MoneyExchangeErp.Web.Api.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userService = UnityConfig.Resolve<UserService>();
            try
            {
                var user = await Task.FromResult(userService.Authenticate(context.UserName, context.Password));
                ClaimsIdentity ci = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, string.Concat(user.FirstName, " ", user.LastName)),
                }, context.Options.AuthenticationType);
                foreach (var role in user.Roles.Split('|'))
                {
                    ci.AddClaim(new Claim(ClaimTypes.Role, role));
                }
                AuthenticationProperties properties = CreateProperties(Mapper.Map<UserModel>(user));
                AuthenticationTicket ticket = new AuthenticationTicket(ci, properties);
                context.Validated(ticket);
            }
            catch(Exception ex)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(UserModel user)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "Id" , user.Id.ToString() },
                { "Civility" , user.Civility },
                { "FirstName", user.FirstName },
                { "LastName", user.LastName },
                { "Username", user.Username },
                { "UserRoles", user.Roles },
                { "Company_Id", user.Company_Id.ToString() },
                { "Agency_Id", user.Agency_Id.ToString() },
                { "AgencyName", user.AgencyName },
                { "CreatedAt", user.CreatedAt.ToShortDateString() },
            };
            return new AuthenticationProperties(data);
        }
    }
}