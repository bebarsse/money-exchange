﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Web.Api.Providers
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            int lifetime = 30;
            int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SessionLifetime"], out lifetime);
            context.Ticket.Properties.IssuedUtc = DateTime.UtcNow;
            context.Ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddMinutes(lifetime);

            context.SetToken(context.SerializeTicket());
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            try
            {
                context.DeserializeTicket(context.Token);
            }
            catch (Exception) { }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}