﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class DashboardController : BaseController
    {
        protected IDashboardService _service;
        protected ICashBoxService _cashBoxService;
        protected IOperationService _operationService;
        protected ITransactionService _transactionService;
        protected IExpenseService _expenseService;
        protected ICustomerService _customerService;

        public DashboardController(IDashboardService service, ICashBoxService cashBoxService, IOperationService operationService, ITransactionService transactionService, IExpenseService expenseService, ICustomerService customerService)
        {
            _service = service;
            _cashBoxService = cashBoxService;
            _operationService = operationService;
            _transactionService = transactionService;
            _expenseService = expenseService;
            _customerService = customerService;
        }

        // Operations
        [HttpGet]
        public IEnumerable<CashBoxModel> GetCashBoxes(int companyId)
        {
            return _cashBoxService.GetCashBoxes(companyId);
        }

        [HttpGet]
        public IEnumerable<OperationModel> GetLastOperations(int companyId)
        {
            return _operationService.GetLastOperations(companyId);
        }

        [HttpGet]
        public IEnumerable<DashboardOperationModel> GetOperationsTotalAmount(int companyId)
        {
            return _operationService.GetOperationsTotalAmount(companyId);
        }

        // Transactions
        [HttpGet]
        public IEnumerable<DashboardTransactionModel> GetDashboardTransactions(int companyId)
        {
            return _transactionService.GetDashboardTransactions(companyId);
        }

        [HttpGet]
        public IEnumerable<TransactionModel> GetLastTransactions(int companyId)
        {
            return _transactionService.GetLastTransactions(companyId);
        }

        // Expenses
        [HttpGet]
        public IEnumerable<DashboardExpenseModel> GetDashboardExpenses(int companyId)
        {
           return _expenseService.GetDashboardExpenses(companyId);
        }

        [HttpGet]
        public IEnumerable<ExpenseModel> GetLastExpenses(int companyId)
        {
            return _expenseService.GetLastExpenses(companyId);
        }

        // Customers
        [HttpGet]
        public IEnumerable<CustomerModel> GetBestCustomers(int companyId)
        {
            return _customerService.GetBestCustomers(companyId);
        }

        [HttpGet]
        public IEnumerable<CustomerModel> GetLastCustomers(int companyId)
        {
            return _customerService.GetLastCustomers(companyId);
        }

    }
}
