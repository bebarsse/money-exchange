﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class CategoryController : BaseController
    {
        protected ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }

        [HttpPost]
        public int CreateCategory(CategoryModel categoryModel)
        {
            return _service.CreateCategory(categoryModel);
        }

        [HttpPost]
        public void UpdateCategory(CategoryModel categoryModel)
        {
            _service.UpdateCategory(categoryModel);
        }

        [HttpPost]
        public void DeleteCategory(CategoryModel categoryModel)
        {
            _service.DeleteCategory(categoryModel);
        }

        [HttpPost]
        public DatatableOutModel GetCategories(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetCategories(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpGet]
        public IEnumerable<CategoryModel> GetCategoriesByCompany(int companyId)
        {
            return _service.GetCategories(companyId);
        }
    }
}
