﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static MoneyExchangeErp.Core.Exceptions.BusinessException;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class TransactionController : BaseController
    {
        protected ITransactionService _service;

        public TransactionController(ITransactionService service)
        {
            _service = service;
        }

        [HttpPost]
        public int CreateTransaction(TransactionModel transactionModel)
        {
            return _service.CreateTransaction(transactionModel);
        }

        [HttpPost]
        public void UpdateTransaction(TransactionModel transactionModel)
        {
            _service.UpdateTransaction(transactionModel);
        }

        [HttpPost]
        public void DeleteTransaction(TransactionModel transactionModel)
        {
            _service.DeleteTransaction(transactionModel);
        }

        [HttpPost]
        public DatatableOutModel GetTransactions(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetTransactions(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpPost, AllowAnonymous]
        public async Task<HttpResponseMessage> UploadTransactionFile()
        {
            try
            {
                // Check if the request contains multipart/form-data.
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }
                string fileSaveLocation = HttpContext.Current.Server.MapPath("~/App_Data/Transactions");
                var provider = new ExtendMultipartFormDataStreamProvider(fileSaveLocation);

                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);
                // Get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    _service.SetTransactionFilePath(int.Parse(HttpContext.Current.Request.Form[0]), file.LocalFileName.Split('\\').Last());
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                throw new BusinessException(BusinessExceptionType.UplodingError);
            }

        }

        [HttpGet, AllowAnonymous]
        public HttpResponseMessage DownloadTransactionFile(string filePath)
        {
            var absoluteFilePath = string.Concat(HttpContext.Current.Server.MapPath(string.Concat("~/App_Data/Transactions/", filePath)));
            HttpResponseMessage result = null;
            if (!File.Exists(absoluteFilePath))
            {
                throw new FileNotFoundException();
            }
            else
            {
                // Serve the file to the client
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(new FileStream(absoluteFilePath, FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
                {
                    FileName = filePath
                };
                result.Content.Headers.Add("content-length", new FileInfo(absoluteFilePath).Length.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            }
            return result;
        }
    }
}
