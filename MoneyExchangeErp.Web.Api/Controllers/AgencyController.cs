﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    
    public class AgencyController : BaseController
    {
        protected IAgencyService _service;

        public AgencyController(IAgencyService service)
        {
            _service = service;
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void CreateAgency(AgencyModel agencyModel)
        {
            _service.CreateAgency(agencyModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void UpdateAgency(AgencyModel agencyModel)
        {
            _service.UpdateAgency(agencyModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void DeleteAgency(AgencyModel agencyModel)
        {
            _service.DeleteAgency(agencyModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public DatatableOutModel GetAgencies(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetAgencies(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpGet]
        public IEnumerable<AgencyModel> GetAgenciesByCompany(int companyId)
        {
           return _service.GetAgencies(companyId);
        }

        [HttpGet]
        public bool CreateAgencyAllowed(int companyId)
        {
            return _service.CreateAgencyAllowed(companyId);
        }

    }
}
