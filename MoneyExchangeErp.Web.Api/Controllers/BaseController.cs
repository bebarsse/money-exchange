﻿using MoneyExchangeErp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    [Authorize]
    public class BaseController : ApiController
    {
        public BaseController()
        {

        }

        [AllowAnonymous]
        public string IsWorking()
        {
            return "Ok";
        }
    }
}
