﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class TaskController : BaseController
    {
        protected ITaskService _service;

        public TaskController(ITaskService service)
        {
            _service = service;
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void CreateTask(TaskModel taskModel)
        {
            _service.CreateTask(taskModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void UpdateTask(TaskModel taskModel)
        {
            _service.UpdateTask(taskModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void DeleteTask(TaskModel taskModel)
        {
            _service.DeleteTask(taskModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public DatatableOutModel GetTasks(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetTasks(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpGet]
        public UserTaskModel GetUserTasks(int userId)
        {
            return _service.GetUserTasks(userId);
        }

        [HttpPost]
        public void CloseTask(TaskModel taskModel)
        {
            _service.CloseTask(taskModel);
        }
    }
}
