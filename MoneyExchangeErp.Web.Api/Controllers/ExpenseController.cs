﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class ExpenseController : BaseController
    {
        protected IExpenseService _service;

        public ExpenseController(IExpenseService service)
        {
            _service = service;
        }

        [HttpPost]
        public void CreateExpense(ExpenseModel expenseModel)
        {
            _service.CreateExpense(expenseModel);
        }

        [HttpPost]
        public void UpdateExpense(ExpenseModel expenseModel)
        {
            _service.UpdateExpense(expenseModel);
        }

        [HttpPost]
        public void DeleteExpense(ExpenseModel expenseModel)
        {
            _service.DeleteExpense(expenseModel);
        }

        [HttpPost]
        public DatatableOutModel GetExpenses(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetExpenses(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

    }
}
