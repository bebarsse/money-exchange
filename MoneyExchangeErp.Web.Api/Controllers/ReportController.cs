﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class ReportController : BaseController
    {
        protected IReportService _service;

        public ReportController(IReportService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<DailyReportModel> GetDailyReport(int companyId, DateTime day)
        {
            return _service.GetDailyReport(companyId, day);
        }

        [HttpGet]
        [RestrictionFilter("Admin")]
        public IEnumerable<MonthlyReportModel> GetMonthlyReport(int companyId, int month, int year)
        {
            return _service.GetMonthlyReport(companyId, new DateTime(year, month + 1, 1));
        }

        [HttpGet]
        [RestrictionFilter("Admin")]
        public IEnumerable<ExpenseReportModel> GetExpenseReport(int companyId, int year)
        {
            return _service.GetExpenseReport(companyId, new DateTime(year, 1, 1));
        }

        [HttpGet]
        [RestrictionFilter("Admin")]
        public IEnumerable<ProfitReportModel> GetProfitReport(int companyId, int year)
        {
            return _service.GetProfitReport(companyId, new DateTime(year, 1, 1));
        }

    }
}
