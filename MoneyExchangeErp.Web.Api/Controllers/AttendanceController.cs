﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class AttendanceController : BaseController
    {
        protected IAttendanceService _service;

        public AttendanceController(IAttendanceService service)
        {
            _service = service;
        }

        [HttpPost]
        public void CreateAttendance(AttendanceModel attendanceModel)
        {
            _service.CreateAttendance(attendanceModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void UpdateAttendance(AttendanceModel attendanceModel)
        {
            _service.UpdateAttendance(attendanceModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void DeleteAttendance(AttendanceModel attendanceModel)
        {
            _service.DeleteAttendance(attendanceModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void DeleteAttendances(HourRecordModel hourRecordModel)
        {
            _service.DeleteAttendaces(hourRecordModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public DatatableOutModel GetAttendances(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetAttendances(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpGet]
        public AttendanceModel GetLastAttendance(int userId)
        {
            return _service.GetLastAttendance(userId);
        }
    }
}
