﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class OperatorController : BaseController
    {
        protected IOperatorService _service;

        public OperatorController(IOperatorService service)
        {
            _service = service;
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void CreateOperator(OperatorModel operatorModel)
        {
            _service.CreateOperator(operatorModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void UpdateOperator(OperatorModel operatorModel)
        {
            _service.UpdateOperator(operatorModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void DeleteOperator(OperatorModel operatorModel)
        {
            _service.DeleteOperator(operatorModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public DatatableOutModel GetOperators(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetOperators(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpGet]
        public IEnumerable<OperatorModel> GetOperators(int companyId)
        {
            return _service.GetOperators(companyId);
        }

        [HttpGet]
        public OperatorModel GetOperatorById(int operatorId)
        {
            return _service.GetOperatorById(operatorId);
        }
    }
}
