﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.Filters;
using System.Web.Http;
using static MoneyExchangeErp.Core.Exceptions.BusinessException;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    [RestrictionFilter("SuperAdmin")]
    public class CompanyController : BaseController
    {
        protected ICompanyService _service;

        public CompanyController(ICompanyService service)
        {
            _service = service;
        }

        [HttpPost]
        public void CreateCompany(CompanyModel companyModel)
        {
            _service.CreateCompany(companyModel);
        }

        [HttpPost]
        public void UpdateCompany(CompanyModel companyModel)
        {
            _service.UpdateCompany(companyModel);
        }

        [HttpPost]
        public void DeleteCompany(CompanyModel companyModel)
        {
            _service.DeleteCompany(companyModel);
        }

        [HttpPost]
        public DatatableOutModel GetCompanies(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetCompanies(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }
        
    }
}
