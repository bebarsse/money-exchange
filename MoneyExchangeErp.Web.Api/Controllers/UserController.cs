﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using MoneyExchangeErp.Services;
using MoneyExchangeErp.Web.Api.App_Start;
using MoneyExchangeErp.Web.Api.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class UserController : BaseController
    {
        protected IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void CreateUser(UserModel userModel)
        {
            _service.CreateUser(userModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void UpdateUser(UserModel userModel)
        {
            _service.UpdateUser(userModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void DeleteUser(UserModel userModel)
        {
            _service.DeleteUser(userModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public void ToggleBanUser(UserModel userModel)
        {
            _service.ToggleBanUser(userModel);
        }

        [HttpPost]
        [RestrictionFilter("Admin")]
        public DatatableOutModel GetUsers(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetUsers(inModel, out total, out totalFiltered));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpPost]
        public void UpdateProfilInformations(UserModel userModel)
        {
            _service.UpdateProfilInformations(userModel);
        }

        [HttpPost]
        public void UpdateProfilPassword(UserModel userModel)
        {
            _service.UpdateProfilPassword(userModel);
        }

        [HttpGet]
        public IEnumerable<UserModel> GetUsersByCompany(int companyId)
        {
           return  _service.GetUsersByCompany(companyId);
        }

    }
}
