﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class OperationController : BaseController
    {
        protected IOperationService _service;

        public OperationController(IOperationService service)
        {
            _service = service;
        }

        [HttpPost]
        public void CreateOperation(OperationModel operationModel)
        {
            _service.CreateOperation(operationModel);
        }

        [HttpPost]
        public void UpdateOperation(OperationModel operationModel)
        {
            _service.UpdateOperation(operationModel);
        }

        [HttpPost]
        public void DeleteOperation(OperationModel operationModel)
        {
            _service.DeleteOperation(operationModel);
        }

        [HttpPost]
        public DatatableOutModel GetProvisions(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetOperations(inModel, out total, out totalFiltered, "P"));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpPost]
        public DatatableOutModel GetWithdrawals(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetOperations(inModel, out total, out totalFiltered, "W"));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }

        [HttpPost]
        public DatatableOutModel GetTransfers(DatatableInModel inModel)
        {
            int total, totalFiltered;
            var outModel = new DatatableOutModel(_service.GetOperations(inModel, out total, out totalFiltered, "T"));
            outModel.draw = inModel.draw;
            outModel.recordsTotal = total;
            outModel.recordsFiltered = totalFiltered;
            return outModel;
        }
    }
}
