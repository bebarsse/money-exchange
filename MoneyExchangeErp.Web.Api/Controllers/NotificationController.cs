﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api.Controllers
{
    public class NotificationController : BaseController
    {
        protected INotificationService _service;

        public NotificationController(INotificationService service)
        {
            _service = service;
        }

        [HttpGet]
        public NotificationAlertModel GetNotificationsAlert(int userId)
        {
            return _service.GetNotificationsAlert(userId);
        }

        [HttpGet]
        public IEnumerable<NotificationModel> GetNotifications(int userId)
        {
            return _service.GetNotifications(userId);
        }

        [HttpGet]
        public void SetViewed(int userId)
        {
            _service.SetViewed(userId);
        }
    }
}
