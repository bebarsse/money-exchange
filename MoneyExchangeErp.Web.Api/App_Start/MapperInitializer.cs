﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Helpers;
using MoneyExchangeErp.Data;
using System;

namespace MoneyExchangeErp.Web.Api
{
    public static class MapperInitializer
    {

        public static void Init()
        {
            Mapper.Initialize(cfg => {
                // Exception
                cfg.CreateMap<AppException, AppExceptionModel>().ReverseMap();

                // Company
                cfg.CreateMap<Company, CompanyModel>().ReverseMap();

                // Agency
                cfg.CreateMap<Agency, AgencyModel>().ReverseMap();
                cfg.CreateMap<AgencyModel, Agency>()
                    .ForMember(dst => dst.Users, opt => opt.Ignore());

                // Operator
                cfg.CreateMap<Operator, OperatorModel>().ReverseMap();

                // Operation
                cfg.CreateMap<Operation, OperationModel>().ReverseMap();

                // Expense
                cfg.CreateMap<Expense, ExpenseModel>().ReverseMap();
                // Category
                cfg.CreateMap<Category, CategoryModel>().ReverseMap();

                // Customer
                cfg.CreateMap<Customer, CustomerModel>();
                cfg.CreateMap<CustomerModel, Customer>()
                    .ForMember(dst => dst.FilePath, opt => opt.Ignore());

                // Transaction
                cfg.CreateMap<Transaction, TransactionModel>();
                cfg.CreateMap<TransactionModel, Transaction>()
                    .ForMember(dst => dst.Profit, opt => opt.MapFrom(src => Math.Round(src.Profit, 2)))
                    .ForMember(dst => dst.Amount, opt => opt.MapFrom(src => Math.Round(src.Amount, 2)))
                    .ForMember(dst => dst.FilePath, opt => opt.Ignore());

                // CashBox
                cfg.CreateMap<CashBox, CashBoxModel>().ReverseMap();

                // Task
                cfg.CreateMap<Task, TaskModel>().ReverseMap();

                // Attendance
                cfg.CreateMap<Attendance, AttendanceModel>().ReverseMap();

                // Task
                cfg.CreateMap<Notification , NotificationModel>().ReverseMap();

                //User
                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<UserModel, User>()
                    .ForMember(dst => dst.Password, opt => { opt.Condition(src => src.PasswordToHash != null); opt.MapFrom(src => Security.CalculateMD5Hash(src.PasswordToHash));} );
            });
        }
    }
}
