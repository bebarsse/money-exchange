﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using MoneyExchangeErp.Web.Api.Providers;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoneyExchangeErp.Web.Api
{
    public partial class Startup
    {
        static Startup()
        {
            int lifetime = 30;
            int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["TokenLifetime"], out lifetime);
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(lifetime),
                AllowInsecureHttp = true,
                RefreshTokenProvider = new RefreshTokenProvider()
            };
        }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }


        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseOAuthBearerTokens(OAuthOptions);
        }
    }
}