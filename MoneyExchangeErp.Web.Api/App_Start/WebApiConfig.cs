﻿using MoneyExchangeErp.Web.Api.Filters;
using System.Web.Http;

namespace MoneyExchangeErp.Web.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Global Filters
            config.Filters.Add(new ExceptionHandlingFilter());
        }
    }
}
