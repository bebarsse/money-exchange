﻿using Microsoft.Practices.Unity;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Helpers;
using MoneyExchangeErp.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data.Unity
{
    public class DataUnityConfig
    {
        private static IUnityContainer _container;

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<MoneyExchangeEntities>();
            container.RegisterType<DbContext, MoneyExchangeEntities>();
            container.RegisterType<IRepository<AppExceptionModel, AppException>, Repository<AppExceptionModel, AppException>>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IRepository<UserModel, User>, Repository<UserModel, User>>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IRepository<CompanyModel, Company>, Repository<CompanyModel, Company>>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IRepository<AgencyModel, Agency>, Repository<AgencyModel, Agency>>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IRepository<OperatorModel, Operator>, Repository<OperatorModel, Operator>>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IRepository<CustomerModel, Customer>, Repository<CustomerModel, Customer>>(new UnityPerRequestLifetimeManager());

            _container = container;
        }

        internal static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
