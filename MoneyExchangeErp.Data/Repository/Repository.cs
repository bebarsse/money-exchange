﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using System;
using System.Data.Entity;
using System.Reflection;

namespace MoneyExchangeErp.Data.Repository
{
    public interface IRepository<TModel, TEntity> where TModel : class where TEntity : class
    {
        void Create(TModel model);
        void Update(TModel model);
        TModel FindModel(int id);
        TEntity FindEntity(int id);
        //IEnumerable<T> Get(Func<T, bool> predicate);
        void Delete(TModel model);
    }

    public class Repository<TModel, TEntity> : Profile, IRepository<TModel, TEntity> where TModel : class where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public void Create(TModel model)
        {
            model.GetType().GetProperty("CreatedAt", BindingFlags.Public | BindingFlags.Instance).SetValue(model, DateTime.Now);
            model.GetType().GetProperty("IsDeleted", BindingFlags.Public | BindingFlags.Instance).SetValue(model, false);
            _dbSet.Add(Mapper.Map<TEntity>(model));
            _context.SaveChanges();
        }

        public void Update(TModel model)
        {
            //Exclude Property to be updated
            //_context.Entry(entity).Property("Password").IsModified = false;
            _context.Entry(Mapper.Map<TEntity>(model)).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public TModel FindModel(int id)
        {
            return  Mapper.Map<TModel>(_dbSet.Find(id));
        }

        public TEntity FindEntity(int id)
        {
            return _dbSet.Find(id);
        }

        //public IEnumerable<T> Get(Func<T, bool> predicate)
        //{
        //    return _dbSet.AsNoTracking().Where(predicate).ToList();
        //}

        public void Delete(TModel model)
        {
            _dbSet.Remove(Mapper.Map<TEntity>(model));
            _context.SaveChanges();
        }

        public void SoftDelete(TModel model)
        {
            model.GetType().GetProperty("IsDeleted", BindingFlags.Public | BindingFlags.Instance).SetValue(model, true);
            model.GetType().GetProperty("DeletedAt", BindingFlags.Public | BindingFlags.Instance).SetValue(model, DateTime.Now);
            _context.Entry(Mapper.Map<TEntity>(model)).State = EntityState.Modified;
            _context.SaveChanges();
        }


    }
}
