﻿using Microsoft.Linq.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data
{
    public partial class Company
    {
        public User Administrator
        {
            get
            {
                return this.Agencies.SingleOrDefault(a => a.IsMain == true).Users.SingleOrDefault(u => u.Roles.Split('|').Contains("Admin"));
            }
        }

        public string AdministratorName
        {
            get
            {
                return this.Administrator.Civility + " " + this.Administrator.FirstName + " " + this.Administrator.LastName; 
            }
        }

        // Calculated Properties

        private static readonly CompiledExpression<Company, string> VirtualAdministratorNameExpression = DefaultTranslationOf<Company>.Property(c => c.VirtualAdministratorName).Is(c => c.Administrator.FirstName + " " + c.Administrator.LastName);
        public string VirtualAdministratorName
        {
            get { return VirtualAdministratorNameExpression.Evaluate(this); }
        }

        private static readonly CompiledExpression<Company, string> VirtualReverseAdministratorNameExpression = DefaultTranslationOf<Company>.Property(c => c.VirtualReverseAdministratorName).Is(c => c.Administrator.LastName + " " + c.Administrator.FirstName);
        public string VirtualReverseAdministratorName
        {
            get { return VirtualReverseAdministratorNameExpression.Evaluate(this); }
        }

    }
}
