﻿using Microsoft.Linq.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data
{
    public partial class Expense
    {
        public string AgencyName
        {
            get
            {
                return this.Agency.Name;
            }
        }

        public string ReceiverCategoryNameName
        {
            get
            {
                return this.Category.Name;
            }
        }

    }
}
