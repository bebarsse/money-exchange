﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data
{
    public partial class CashBox
    {
        public string AgencyName
        {
            get
            {
                return this.Agency.Name;
            }
        }
    }
}
