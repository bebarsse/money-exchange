﻿using Microsoft.Linq.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data
{
    public partial class Operation
    {
        public string AgencyName
        {
            get
            {
                return this.Agency.Name;
            }
        }

        public string ReceiverName
        {
            get
            {
                return (this.Receiver != null ) ? this.Company.Agencies.SingleOrDefault(a => a.Id == this.Receiver).Name : null;
            }
        }

    }
}
