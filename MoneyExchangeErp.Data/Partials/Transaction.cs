﻿using Microsoft.Linq.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data
{
    public partial class Transaction
    {
        public string CustomerName
        {
            get
            {
                return this.Customer.CompleteName;
            }
        }

        public string OperatorName
        {
            get
            {
                return this.Operator.Name;
            }
        }

        public string OperatorType
        {
            get
            {
                return this.Operator.Category;
            }
        }

        public string AgencyName
        {
            get
            {
                return this.Agency.Name;
            }
        }

        // Calculated properties

        private static readonly CompiledExpression<Transaction, string> VirtualCustomerNameExpression = DefaultTranslationOf<Transaction>.Property(t => t.VirtualCustomerName).Is(t => t.Customer.FirstName + " " + t.Customer.LastName);
        public string VirtualCustomerName
        {
            get { return VirtualCustomerNameExpression.Evaluate(this); }
        }

        private static readonly CompiledExpression<Transaction, string> VirtualReverseCustomerNameExpression = DefaultTranslationOf<Transaction>.Property(t => t.VirtualReverseCustomerName).Is(t => t.Customer.LastName + " " + t.Customer.FirstName);
        public string VirtualReverseCustomerName
        {
            get { return VirtualReverseCustomerNameExpression.Evaluate(this); }
        }

    }
}
