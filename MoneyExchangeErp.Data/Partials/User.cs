﻿using Microsoft.Linq.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data
{
    public partial class User
    {
        public int Company_Id
        {
            get
            {
                return this.Agency.CompanyId;
            }
        }

        public int Agency_Id
        {
            get
            {
                return this.Agency.Id;
            }
        }

        public string AgencyName
        {
            get
            {
                return this.Agency.Name;
            }
        }

        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        // Calculated Properties
        private static readonly CompiledExpression<User, string> VirtualFullNameExpression = DefaultTranslationOf<User>.Property(u => u.VirtualFullName).Is(u => u.FirstName + " " + u.LastName);
        public string VirtualFullName
        {
            get { return VirtualFullNameExpression.Evaluate(this); }
        }

        private static readonly CompiledExpression<User, string> VirtualReverseFullNameExpression = DefaultTranslationOf<User>.Property(u => u.VirtualReverseFullName).Is(u => u.LastName + " " + u.FirstName);
        public string VirtualReverseFullName
        {
            get { return VirtualReverseFullNameExpression.Evaluate(this); }
        }

    }
}
