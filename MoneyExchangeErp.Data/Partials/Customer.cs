﻿using Microsoft.Linq.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Data
{
    public partial class Customer
    {
        public string CompleteName
        {
            get
            {
                return string.Concat(this.Civility, " ", this.FirstName, " ", this.LastName);
            }
        }

        public string AgencyName
        {
            get
            {
                if (this.Agency != null)
                    return this.Agency.Name;
                else
                    return null;
            }
        }

        public double NumberOfTransactions
        {
            get
            {
                return this.Transactions.Where(t => t.IsDeleted == false).Count();
            }
        }

        public DateTime? LastTransaction
        {
            get
            {
                if (this.Transactions.Where(t => t.IsDeleted == false).OrderByDescending(t => t.Date).FirstOrDefault() != null)
                    return this.Transactions.Where(t => t.IsDeleted == false).OrderByDescending(t => t.Date).FirstOrDefault().Date;
                else
                    return null;
            }
        }

        // Calculated Properties

        private static readonly CompiledExpression<Customer, string> VirtualCustomerNameExpression = DefaultTranslationOf<Customer>.Property(c => c.VirtualCustomerName).Is(c => c.FirstName + " " + c.LastName);
        public string VirtualCustomerName
        {
            get { return VirtualCustomerNameExpression.Evaluate(this); }
        }

        private static readonly CompiledExpression<Customer, string> VirtualReverseCustomerNameExpression = DefaultTranslationOf<Customer>.Property(c => c.VirtualReverseCustomerName).Is(c => c.LastName + " " + c.FirstName);
        public string VirtualReverseCustomerName
        {
            get { return VirtualReverseCustomerNameExpression.Evaluate(this); }
        }

    }
}
