﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MoneyExchangeErp.Core.Helpers
{
    public class UnityPerRequestLifetimeManager : LifetimeManager
    {
        private readonly string key = Guid.NewGuid().ToString();
        private Dictionary<string, object> objectsMap = new Dictionary<string, object>();
        public override object GetValue()
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items.Contains(key)) return HttpContext.Current.Items[key];
            }
            else
            {
                if (objectsMap != null && objectsMap.ContainsKey(key))
                    return objectsMap[key];
            }
            return null;
        }

        public override void RemoveValue()
        {
            if (HttpContext.Current != null)
                HttpContext.Current.Items.Remove(key);
            else if (objectsMap != null)
                objectsMap.Remove(key);
        }

        public override void SetValue(object newValue)
        {
            if (HttpContext.Current != null)
                HttpContext.Current.Items[key] = newValue;
            else if (objectsMap != null && !objectsMap.ContainsKey(key))
                objectsMap.Add(key, newValue);
            else if (objectsMap != null && objectsMap.ContainsKey(key))
                objectsMap[key] = newValue;
        }
    }
}
