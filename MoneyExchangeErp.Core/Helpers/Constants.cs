﻿using MoneyExchangeErp.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.Helpers
{
    public static class Constants
    {
        
        public static IEnumerable<OperatorModel> _DefalutOperatorsCashPlus()
        {
            var list = new List<OperatorModel>();
            list.Add(new OperatorModel
            {
                Name = "CashPlus",
                Category= "Transfert",
                TransactionType = "IO",
                IsMain = true,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "WesternUnion",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.9,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "MoneyGram",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.9,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Money Exchange",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Ria",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Moneytrans",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.6,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "MoneyExpress",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "MoneyInternational",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Monty",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Wari",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "SmallWorld",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Pintail",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Neosurf",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Titanes",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "TransferZero",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Europhil",
                Category = "Transfert",
                TransactionType = "O",
                IsByPercentage = true,
                Profit = 0.5,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Redal",
                Category = "Service",
                TransactionType = "I",
                IsByPercentage = false,
                Profit = 0.8,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "RADEEF",
                Category = "Service",
                TransactionType = "I",
                IsByPercentage = false,
                Profit = 0.8,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            list.Add(new OperatorModel
            {
                Name = "Inwi",
                Category = "Service",
                TransactionType = "I",
                IsByPercentage = false,
                Profit = 1,
                IsMain = false,
                CreatedAt = DateTime.Now,
                IsDeleted = false
            });
            return list;
        }
    }
}
