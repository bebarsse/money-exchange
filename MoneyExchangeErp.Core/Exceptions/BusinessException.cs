﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.Exceptions
{
    public class BusinessException : Exception
    {
        public BusinessExceptionType Type { get; set; }
        public BusinessException(BusinessExceptionType type)
        {
            Type = type;
        }

        public enum BusinessExceptionType
        {
            UnknownError,
            ActionUnauthorized,
            UplodingError,
            CustomerExist,
            UsernameExist,
            IncorrectPassword
        }
    }
}
