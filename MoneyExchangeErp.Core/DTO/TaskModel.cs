﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public System.DateTime Date { get; set; }
        public string Importance { get; set; }
        public string Status { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int Company_Id { get; set; }
        public int User_Id { get; set; }
        public string UserName { get; set; }
       
    }
}
