﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MoneyExchangeErp.Core.DTO
{
    public class AgencyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string Email { get; set; }
        public bool IsMain { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int ConnectedUserId { get; set; }
        public IEnumerable<UserModel> Users { get; set; }
        public IEnumerable<UserModel> MatchingUsers
        {
            get
            {
                if (Users == null)
                    return null;
                else
                    return this.Users.Where(u => u.IsDeleted == false);
            }
        }
    }
}
