﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class DashboardTransactionModel
    {
        public string AgencyName { get; set; }
        public int DailyTransactions { get; set; }
        public int MonthlyTransactions { get; set; }
        public double MonthlyVolume { get; set; }
    }
}
