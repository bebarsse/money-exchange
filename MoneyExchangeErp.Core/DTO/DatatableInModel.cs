﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class DatatableInModel
    {
        public int companyId { get; set; }
        public int userId { get; set; }
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public IEnumerable<DatatableInColumnsModel> columns { get; set; }
        public IEnumerable<DatatableInOrdreModel> order { get; set; }
        public DatatableInSearchModel search { get; set; }
        public IEnumerable<DatatableInFiltersModel> filters { get; set; }
    }

    public class DatatableInColumnsModel
    {
        public string data { get; set; }
    }

    public class DatatableInOrdreModel
    {
        public int column { get; set; }
        public string dir { get; set; }
    }

    public class DatatableInSearchModel
    {
        public bool regex { get; set; }
        public string value { get; set; }
    }

    public class DatatableInFiltersModel
    {
        public string name { get; set; }
        public string value { get; set; }
    }




}
