﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class UserTaskModel
    {
        public int TasksCount { get; set; }
        public int HighTasksCount { get; set; }
        public int MediumTasksCount { get; set; }
        public int LowTasksCount { get; set; }
        public IEnumerable<TaskModel> Tasks { get; set; }
    }
}
