﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class MonthlyReportModel
    {
        public string AgencyName { get; set; }
        public IEnumerable<MonthlyTransactionsModel> MonthlyTransactions { get; set; }
        public IEnumerable<MonthlyExpenseModel> MonthlyExpenses { get; set; }
        public double GrandTotalProfit { get; set; }
        public double GrandTotalExpenses { get; set; }

    }

    public class MonthlyTransactionsModel
    {
        public string OperatorName { get; set; }
        public string TransactionType { get; set; }
        public string OperatorType { get; set; }
        public int Number { get; set; }
        public double Volume { get; set; }
        public double Profit { get; set; }
    }

    public class MonthlyExpenseModel
    {
        public string CategoryName { get; set; }
        public int Number { get; set; }
        public double Volume { get; set; }
    }

    
}
