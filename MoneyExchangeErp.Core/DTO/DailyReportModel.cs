﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class DailyReportModel
    {
        public string AgencyName { get; set; }
        public IEnumerable<DailyTransactionsModel> DailyTransactions { get; set; }
        public IEnumerable<DailyOperationModel> DailyOperations { get; set; }
        public IEnumerable<DailyExpenseModel> DailyExpenses { get; set; }
        public double GrandTotalIn { get; set; }
        public double GrandTotalOut { get; set; }
        public double GrandTotalProfit { get; set; }
        public double GrandTotalExpenses { get; set; }
        public double InitialCashBox { get; set; }
        public double FinalCashBox { get; set; }
    }

    public class DailyTransactionsModel
    {
        public string OperatorName { get; set; }
        public string TransactionType { get; set; }
        public string OperatorType { get; set; }
        public int Number { get; set; }
        public double Volume { get; set; }
        public double Profit { get; set; }
    }

    public class DailyOperationModel
    {
        public string OperationName { get; set; }
        public int Number { get; set; }
        public double Volume { get; set; }
    }

    public class DailyExpenseModel
    {
        public string CategoryName { get; set; }
        public int Number { get; set; }
        public double Volume { get; set; }
    }
}
