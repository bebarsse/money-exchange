﻿using System;

namespace MoneyExchangeErp.Core.DTO
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string Civility { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> BirthDay { get; set; }
        public string CIN { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Comment { get; set; }
        public string FilePath { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int Company_Id { get; set; }
        public int Agency_Id { get; set; }
        public string CompleteName { get; set; }
        public string AgencyName { get; set; }
        public int NumberOfTransactions { get; set; }
        public DateTime LastTransaction { get; set; }

    }
}
