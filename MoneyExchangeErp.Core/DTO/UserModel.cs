﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Civility { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string PasswordToHash { get; set; }
        public string CurrentPassword { get; set; }
        public string Roles { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsMain { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int Company_Id { get; set; }
        public int Agency_Id { get; set; }
        public string FullName { get; set; }
        public string AgencyName { get; set; }

    }
}
