﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class AttendanceModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public System.DateTime Date { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int User_Id { get; set; }

    }
}
