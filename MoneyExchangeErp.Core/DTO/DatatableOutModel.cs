﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class DatatableOutModel
    {
        public DatatableOutModel(IEnumerable<object> elements)
        {
            data = elements;
        }
        public IEnumerable<object> data { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public int draw { get; set; }
    }
}
