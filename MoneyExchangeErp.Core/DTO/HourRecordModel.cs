﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class HourRecordModel
    {
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public string AgencyName { get; set; }

        public IEnumerable<AttendanceModel> Attendances { get; set; }
    }
}
