﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class OperationModel
    {
        public int Id { get; set; }
        public Nullable<int> Receiver { get; set; }
        public string Type { get; set; }
        public System.DateTime Date { get; set; }
        public double Amount { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int Company_Id { get; set; }
        public int Agency_Id { get; set; }
        public string AgencyName { get; set; }
        public string ReceiverName { get; set; }
    }
}
