﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class NotificationModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public bool IsViewed { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int User_Id { get; set; }
    }

    public enum NotificationType
    {
        Task,
        Transfer
    }
}
