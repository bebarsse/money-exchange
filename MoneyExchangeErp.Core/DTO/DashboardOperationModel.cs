﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class DashboardOperationModel
    {
        public string AgencyName { get; set; }
        public double MonthlyProvisions { get; set; }
        public double MonthlyWithdrawals { get; set; }
    }
}
