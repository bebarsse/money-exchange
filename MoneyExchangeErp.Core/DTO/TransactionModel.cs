﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class TransactionModel
    {
        public int Id { get; set; }
        public System.DateTime Date { get; set; }
        public string TransactionType { get; set; }
        public double Amount { get; set; }
        public double Fees { get; set; }
        public string FeesPaidBy { get; set; }
        public Nullable<bool> WithSMSOption { get; set; }
        public double Profit { get; set; }
        public string Comment { get; set; }
        public string FilePath { get; set; }
        public string AddedBy { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedAt { get; set; }

        public int Company_Id { get; set; }
        public Nullable<int> Operator_Id { get; set; }
        public Nullable<int> Customer_Id { get; set; }
        public Nullable<int> Agency_Id { get; set; }
        public string CustomerName { get; set; }
        public string OperatorName { get; set; }
        public string AgencyName { get; set; }
    }
}
