﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class ExpenseReportModel
    {
        public string AgencyName { get; set; }
        public IEnumerable<MonthlyExpenseReportModel> MonthlyExpenses { get; set; }
        public IEnumerable<CategoryExpenseReportModel> CategoryExpenses { get; set; }
        public double GrandTotal { get; set; }
    }

    public class MonthlyExpenseReportModel
    {
        public string Month { get; set; }
        public double Volume { get; set; }
    }

    public class CategoryExpenseReportModel
    {
        public string CategoryName { get; set; }
        public double Volume { get; set; }
    }
}
