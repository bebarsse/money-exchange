﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class ProfitReportModel
    {
        public string AgencyName { get; set; }
        public IEnumerable<MonthlyProfitReportModel> MonthlyProfits { get; set; }
        public IEnumerable<OperatorProfitReportModel> OperatorProfits { get; set; }
        public IEnumerable<MonthlyExpenseReportModel> MonthlyExpenses { get; set; }
        public double GrandTotalProfit { get; set; }
        public double GrandTotalExpense { get; set; }
    }

    public class MonthlyProfitReportModel
    {
        public string Month { get; set; }
        public double Volume { get; set; }
    }

    public class OperatorProfitReportModel
    {
        public string OperatorName { get; set; }
        public double Volume { get; set; }
    }
}
