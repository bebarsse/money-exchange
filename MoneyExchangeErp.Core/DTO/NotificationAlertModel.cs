﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class NotificationAlertModel
    {
        public int NotificationsCount { get; set; }
        public IEnumerable<NotificationModel> LastNotifications { get; set; }
    }
}
