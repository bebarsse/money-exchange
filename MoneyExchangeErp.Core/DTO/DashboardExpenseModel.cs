﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Core.DTO
{
    public class DashboardExpenseModel
    {
        public string AgencyName { get; set; }
        public double DailyExpenses { get; set; }
        public double MonthlyExpenses { get; set; }
    }
}
