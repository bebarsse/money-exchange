﻿MoneyExchangeApp.directive('enterKey', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.enterKey);
                });

                event.preventDefault();
            }
        });
    };
});

MoneyExchangeApp.directive('meSelect', function ($parse) {
    return {
        restrict: 'A',
        scope:{
            property: '@property'
        },
        link: function (scope, element, attrs) {
            scope.$parent.$watch(String(attrs.ngModel), function (newVal, oldVal) {
                //var test = $parse(attribute)($scope);
                $parse(scope.property).assign(scope.$parent, newVal.value);
            });
        }
    }
});

MoneyExchangeApp.directive('meBindSelect', function ($parse) {
    return {
        restrict: 'A',
        scope: {
            property: '@property'
        },
        link: function (scope, element, attrs) {
            scope.$parent.$watch(String(attrs.ngModel), function (newVal, oldVal) {
                $parse(scope.property).assign(scope.$parent, newVal.value);
            });
        }
    }
});

MoneyExchangeApp.directive('passwordChecker', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var initialized = false;

            $(element).keydown(function () {
                if (initialized === false) {
                    // set base options
                    $(element).pwstrength({
                        raisePower: 1.4,
                        minChar: 8,
                        verdicts: ["Faible", "Normal", "Moyen", "Fort", "Très fort"],
                        scores: [17, 26, 40, 50, 60]
                    });

                    // add your own rule to calculate the password strength
                    $(element).pwstrength("addRule", "demoRule", function (options, word, score) {
                        return word.match(/[a-z].[0-9]/) && score;
                    }, 10, true);

                    // set as initialized 
                    initialized = true;
                }
            });
        }
    }
});

MoneyExchangeApp.directive('capitalizeFirst', function ($parse) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var capitalize = function (inputValue) {
                if (inputValue === null || inputValue === undefined) { return; }
                var capitalized = inputValue.charAt(0).toUpperCase() +
                    inputValue.substring(1);
                if (capitalized !== inputValue) {
                    //modelCtrl.$setViewValue(capitalized);
                    //modelCtrl.$render();
                }
                return capitalized;
            }
            modelCtrl.$parsers.push(capitalize);
            capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
        }
    };
});

MoneyExchangeApp.directive('trim', function ($parse) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var trim = function (inputValue) {
                if (inputValue === undefined) { inputValue = ''; }
                var trimed = inputValue.replace(/\s/g, '');
                if (trimed !== inputValue) {
                    modelCtrl.$render();
                }
                return trimed;
            }
            modelCtrl.$parsers.push(trim);
            trim($parse(attrs.ngModel)(scope));
        }
    };
});

MoneyExchangeApp.directive('upperCase', function ($parse) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var trim = function (inputValue) {
                if (inputValue === undefined) { inputValue = ''; }
                var trimed = inputValue.toUpperCase();
                if (trimed !== inputValue) {
                    modelCtrl.$render();
                }
                return trimed;
            }
            modelCtrl.$parsers.push(trim);
            trim($parse(attrs.ngModel)(scope));
        }
    };
});

MoneyExchangeApp.directive('touchSpin', function () {
    return {
        restrict: 'A',
        scope: {
            postfix: '@postfix'
        },
        link: function (scope, element, attrs) {
            $(element).TouchSpin({
                buttondown_class: 'btn red',
                buttonup_class: 'btn blue',
                min: 0,
                max: 1000000000000000000000,
                step: 0.01,
                decimals: 2,
                booster:true,
                boostat: 10000,
                maxboostedstep: 1000000000000000000000,
                postfix: scope.postfix
            });
        }
    }
});

MoneyExchangeApp.directive('ngThumb', ['$window', function ($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function (item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function (file) {
            var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|pdf|'.indexOf(type) !== -1;
        },
        isPdf: function (file) {
            var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|pdf|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function (scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = (!helper.isPdf(params.file)) ? event.target.result : window.location.origin.concat('/assets/images/pdf.png');
            };

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            };
        }
    };
}]);

MoneyExchangeApp.directive('datePicker', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            setTodayDate: '=setTodayDate',
            withClearBtn: '=withClearBtn',
            max: '=max'
        },
        link: function (scope, element, attrs, ngModelController) {

            ngModelController.$parsers.push(function (data) {
                if (data === '')
                    return null;

                // Check if data respect the view format 
                //var regex = new RegExp("^[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]$", "i");
                //if (!regex.test(data)) {
                //    alert('coucou');
                //}
                //convert data from view format to model format
                else {
                    // Current Time
                    var currentTime = new Date();
                    dateArray = data.split("/");
                    return dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0] + " " + currentTime.getHours() + ":" + currentTime.getMinutes() + ":" + currentTime.getSeconds();
                }
            });

            $(element).pickadate({
                monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                // Buttons
                today: 'Aujourd\'hui',
                clear: (scope.withClearBtn) ? 'Effacer' : false,
                close: 'Fermer',
                format: 'dd/mm/yyyy',
                selectMonths: true,
                selectYears: 100,
                max: scope.max,
                editable:false,
                onStart: function () {
                    if (scope.setTodayDate) {
                        this.set('select', new Date()); 
                    }
                }
            });
            
        }
    }
});

MoneyExchangeApp.directive('sticky', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).sticky({ topSpacing: 100, bottomSpacing: 0 });

        }
    }
});

//Filters
MoneyExchangeApp.filter('sumByColumn', function () {
    return function (collection, column) {
        if (collection == undefined)
            return 0;
        else {
            var total = 0;
            collection.forEach(function (item) {
                total += parseFloat(item[column]);
            });
            return total;
        }
        
    };
});