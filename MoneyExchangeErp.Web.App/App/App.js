﻿var MoneyExchangeApp = angular.module("MoneyExchangeApp", [
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'ui.select',
    'angularFileUpload',
    'ngSanitize',
    'ui-notification',
    'LocalStorageModule',
    'ui.validate',
    'datatables',
    'datatables.bootstrap',
    'datatables.buttons',
    'mwl.confirm',
    'ngMask'
]);

MoneyExchangeApp.config(['$ocLazyLoadProvider', '$httpProvider', '$locationProvider', 'localStorageServiceProvider', 'NotificationProvider', '$compileProvider', function ($ocLazyLoadProvider, $httpProvider, $locationProvider, localStorageServiceProvider, NotificationProvider, $compileProvider) {

    $httpProvider.interceptors.push('AuthenticationHttpInterceptor');

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    localStorageServiceProvider.setPrefix('com.moneyexchangeerp.webapp');

    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before' // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
    });

    NotificationProvider.setOptions({
        delay: 5000,
        startTop: 20,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'bottom',
        replaceMessage:true
    });

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);

}]);

MoneyExchangeApp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

MoneyExchangeApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar state
        },
    };

    $rootScope.settings = settings;

    return settings;
}]);

MoneyExchangeApp.constant('Constants', {
     C_APIURL: 'http://dev.api.moneyexchange/'
});

MoneyExchangeApp.controller('AppController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function () {
        Metronic.initComponents(); // init core components
    });
}]);

/* Setup Layout Part - Header */
MoneyExchangeApp.controller('HeaderController', ['$rootScope', '$scope', '$state', 'AuthenticationProvider', '$timeout', '$interval', 'APIProvider', 'Notification',  function ($rootScope, $scope, $state, AuthenticationProvider, $timeout, $interval, APIProvider, Notification) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initHeader();
    });

    $scope.Logout = function () {
        AuthenticationProvider.logout();
        $state.go('Login');
    };

    var tick = function () {
        $rootScope.clock = Date.now();
        $timeout(tick, $scope.tickInterval); // reset the timer
    };

    // Start the timer
    $timeout(tick, $scope.tickInterval);

    // Notification alert
    var GetNotificationsAlert = function () {
        APIProvider.get('Notification/GetNotificationsAlert', { userId: $rootScope.user.Id }).then(function (notificationAlert) {
            $scope.NotificationAlert = notificationAlert;
            $rootScope.user.NotificationsCount = notificationAlert.NotificationsCount;
        }, function (rejection) {
            if(rejection != undefined)
                Notification.error({ message: rejection, title: 'Erreur' });
            else
                Notification.error({ message: 'Votre session est expirée. Merci de s\'authentifier.', title: 'Session Expirée' });
        });
    };
    GetNotificationsAlert();

    var NotificationRefresher = $interval(function () {
        GetNotificationsAlert();
    }.bind(this), 10000);

    $scope.$on('$destroy', function(){
        $interval.cancel(NotificationRefresher);
    });

    // Set notification viewed action
    $scope.SetViewed = function () {
        APIProvider.get('Notification/SetViewed', { userId: $rootScope.user.Id }).then(function (response) {
            GetNotificationsAlert();
        }, function (rejection) {
            Notification.error({ message: rejection, title : 'Erreur'});
        });
    };

}]);

/* Setup Layout Part - Sidebar */
MoneyExchangeApp.controller('SidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Sidebar */
MoneyExchangeApp.controller('PageHeadController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
    });
}]);

/* Setup Layout Part - Footer */
MoneyExchangeApp.controller('FooterController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initFooter();
    });
}]);

MoneyExchangeApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/acces_rapide");

    $stateProvider
        // Login
        .state('Login', {
            url: "/connexion",
            templateUrl: "/App/Views/Login/Login.Index.html",
            controller: "AuthenticationController",
            data: { IsLoginPage: true },
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '/Assets/Css/login2.css',
                            '/App/Controllers/AuthenticationController.js'
                        ]
                    });
                }]
            }
        })
        // Layout
        .state('Layout', {
            url: '',
            abstract: true,
            templateUrl: "/App/Views/Shared/Layout.html",
            data: { IsLoginPage: false },
            resolve: {
                security: ['$rootScope', '$q', '$state', 'AuthenticationProvider', function ($rootScope, $q, $state, AuthenticationProvider) {
                    if (AuthenticationProvider.getConnectedUser() === null)
                        return $q.reject("Not Authorized");
                    else
                        $rootScope.user = AuthenticationProvider.getConnectedUser();
                }]
            }
        })

        // Attendance
        .state('Layout.ListAttendance', {
            url: "/pointages",
            templateUrl: "/App/Views/Attendance/Attendance.List.html",
            data: { pageTitle: 'Pointage', pageSubTitle: 'Gestion des pointages' },
            controller: "AttendanceController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/AttendanceController.js',
                            '/App/Services/DatatableService.js'
                        ]
                    });
                }]
            }
        })

        .state('Layout.CreateAttendance', {
            url: "/pointage",
            templateUrl: "/App/Views/Attendance/Attendance.Create.html",
            data: { pageTitle: 'Pointage', pageSubTitle: 'Régler mon pointage' },
            controller: "AttendanceController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/AttendanceController.js',
                            '/App/Services/AttendanceService.js'
                        ]
                    });
                }]
            }
        })

        // Quick Access
        .state('Layout.QuickAccess', {
            url: "/acces_rapide",
            templateUrl: "/App/Views/QuickAccess/QuickAccess.Index.html",
            data: { pageTitle: 'Accès Rapide', pageSubTitle: 'Raccourcis' },
            controller: "QuickAccessController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/QuickAccessController.js'
                        ]
                    });
                }]
            }
        })

        // Dashboard
        .state('Layout.Dashboard', {
            url: "/tableau_de_bord",
            templateUrl: "/App/Views/Dashboard/Dashboard.Index.html",
            data: { pageTitle: 'Tableau de bord', pageSubTitle: 'Statistiques en temps réel' },
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/DashboardController.js',
                            '/App/Services/CashBoxService.js',
                            '/App/Services/AgencyService.js',
                            '/App/Services/OperationService.js',
                            '/App/Services/TransactionService.js',
                            '/App/Services/ExpenseService.js',
                            '/App/Services/CustomerService.js'
                        ]
                    });
                }]
            }
        })

        // Daily Report
        .state('Layout.DailyReport', {
            url: "/rapport_journalier",
            templateUrl: "/App/Views/Report/DailyReport.Index.html",
            data: { pageTitle: 'Rapport', pageSubTitle: 'Journalier' },
            controller: "ReportController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ReportController.js',
                            '/App/Services/ReportService.js'
                        ]
                    });
                }]
            }
        })

        // Monthly Report
        .state('Layout.MonthlyReport', {
            url: "/rapport_mensuel",
            templateUrl: "/App/Views/Report/MonthlyReport.Index.html",
            data: { pageTitle: 'Rapport', pageSubTitle: 'Mensuel' },
            controller: "ReportController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ReportController.js',
                            '/App/Services/ReportService.js'
                        ]
                    });
                }]
            }
        })

        // Expense Report
        .state('Layout.ExpenseReport', {
            url: "/rappprt_dépenses",
            templateUrl: "/App/Views/Report/ExpenseReport.Index.html",
            data: { pageTitle: 'Rapport Dépenses', pageSubTitle: 'Annuel' },
            controller: "ReportController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ReportController.js',
                            '/App/Services/ReportService.js'
                        ]
                    });
                }]
            }
        })

        // Profit Report
        .state('Layout.ProfitReport', {
            url: "/rappprt_commission",
            templateUrl: "/App/Views/Report/ProfitReport.Index.html",
            data: { pageTitle: 'Rapport Commission', pageSubTitle: 'Annuel' },
            controller: "ReportController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ReportController.js',
                            '/App/Services/ReportService.js'
                        ]
                    });
                }]
            }
        })

        // Provision
        .state('Layout.ListProvision', {
            url: "/caisse/alimentations",
            templateUrl: "/App/Views/Provision/Provision.List.html",
            data: { pageTitle: 'Alimentations', pageSubTitle: 'Liste des alimentations' },
            controller: "ProvisionController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ProvisionController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateProvision', {
            url: "/caisse/alimentations/ajouter",
            templateUrl: "/App/Views/Provision/Provision.Create.html",
            data: { pageTitle: 'Alimentations', pageSubTitle: 'Ajouter une alimentation' },
            controller: "ProvisionController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ProvisionController.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })

        // Withdrawal
        .state('Layout.ListWithdrawal', {
            url: "/caisse/retraits",
            templateUrl: "/App/Views/Withdrawal/Withdrawal.List.html",
            data: { pageTitle: 'Retraits', pageSubTitle: 'Liste des retraits' },
            controller: "WithdrawalController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/WithdrawalController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateWithdrawal', {
            url: "/caisse/retraits/ajouter",
            templateUrl: "/App/Views/Withdrawal/Withdrawal.Create.html",
            data: { pageTitle: 'Retaits', pageSubTitle: 'Ajouter un retrait' },
            controller: "WithdrawalController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/WithdrawalController.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })

        // Transfer
        .state('Layout.ListTransfer', {
            url: "/caisse/transferts",
            templateUrl: "/App/Views/Transfer/Transfer.List.html",
            data: { pageTitle: 'Transferts', pageSubTitle: 'Liste des transferts' },
            controller: "TransferController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/TransferController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateTransfer', {
            url: "/caisse//transfert/ajouter",
            templateUrl: "/App/Views/Transfer/Transfer.Create.html",
            data: { pageTitle: 'Transferts', pageSubTitle: 'Ajouter un transfert' },
            controller: "TransferController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/TransferController.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })

        // Expense
        .state('Layout.ListExpense', {
            url: "/depenses",
            templateUrl: "/App/Views/Expense/Expense.List.html",
            data: { pageTitle: 'Dépenses', pageSubTitle: 'Liste des dépenses' },
            controller: "ExpenseController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ExpenseController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/CategoryService.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateExpense', {
            url: "/depenses/ajouter",
            templateUrl: "/App/Views/Expense/Expense.Create.html",
            data: { pageTitle: 'Dépenses', pageSubTitle: 'Ajouter une dépense' },
            controller: "ExpenseController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/ExpenseController.js',
                            '/App/Services/CategoryService.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })

        // Category
        .state('Layout.ListCategory', {
            url: "/depenses/categories",
            templateUrl: "/App/Views/Category/Category.List.html",
            data: { pageTitle: 'Catégories', pageSubTitle: 'Liste des catégories' },
            controller: "CategoryController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/CategoryController.js',
                            '/App/Services/DatatableService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateCategory', {
            url: "/depenses/categories/ajouter",
            templateUrl: "/App/Views/Category/Category.Create.html",
            data: { pageTitle: 'Catégories', pageSubTitle: 'Ajouter une catégorie' },
            controller: "CategoryController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/CategoryController.js'
                        ]
                    });
                }]
            }
        })

        // Transaction
        .state('Layout.ListTransaction', {
            url: "/transactions",
            templateUrl: "/App/Views/Transaction/Transaction.List.html",
            data: { pageTitle: 'Transactions', pageSubTitle: 'Liste des transactions' },
            controller: "TransactionController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/TransactionController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/OperatorService.js',
                            '/App/Services/FileUploaderService.js',
                            '/App/Services/AgencyService.js',
                            '/App/Services/CustomerService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateTransaction', {
            url: "/transactions/ajouter",
            templateUrl: "/App/Views/Transaction/Transaction.Create.html",
            data: { pageTitle: 'Transactions', pageSubTitle: 'Ajouter une transaction' },
            controller: "TransactionController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/TransactionController.js',
                            '/App/Services/OperatorService.js',
                            '/App/Services/CustomerService.js',
                            '/App/Services/FileUploaderService.js'
                        ]
                    });
                }]
            }
        })
        

        // Operator
        .state('Layout.ListOperator', {
            url: "/operateurs",
            templateUrl: "/App/Views/Operator/Operator.List.html",
            data: { pageTitle: 'Opérateurs', pageSubTitle: 'Liste des opérateurs' },
            controller: "OperatorController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/OperatorController.js',
                            '/App/Services/DatatableService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateOperator', {
            url: "/operateurs/ajouter",
            templateUrl: "/App/Views/Operator/Operator.Create.html",
            data: { pageTitle: 'Opérateurs', pageSubTitle: 'Ajouter un opérateur' },
            controller: "OperatorController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/OperatorController.js',
                        ]
                    });
                }]
            }
        })
        //Customer
        .state('Layout.ListCustomer', {
            url: "/clients",
            templateUrl: "/App/Views/Customer/Customer.List.html",
            data: { pageTitle: 'Clients', pageSubTitle: 'Liste des clients' },
            controller: "CustomerController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/CustomerController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/FileUploaderService.js',
                            '/App/Services/AgencyService.js',
                            '/App/Services/CustomerService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateCustomer', {
            url: "/clients/ajouter",
            templateUrl: "/App/Views/Customer/Customer.Create.html",
            data: { pageTitle: 'Clients', pageSubTitle: 'Ajouter un client' },
            controller: "CustomerController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/CustomerController.js',
                            '/App/Services/FileUploaderService.js',
                            '/App/Services/CustomerService.js'
                        ]
                    });
                }]
            }
        })

        // --- Administration -----
        // Agency
        .state('Layout.ListAgency', {
            url: "/agences",
            templateUrl: "/App/Views/Agency/Agency.List.html",
            data: { pageTitle: 'Agences', pageSubTitle: 'Liste des agences' },
            controller: "AgencyController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/AgencyController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/Helpers.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateAgency', {
            url: "/agences/ajouter",
            templateUrl: "/App/Views/Agency/Agency.Create.html",
            data: { pageTitle: 'Agences', pageSubTitle: 'Ajouter une agence' },
            controller: "AgencyController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/AgencyController.js',
                            '/App/Services/AgencyService.js',
                            '/App/Services/Helpers.js'
                        ]
                    });
                }]
            }
        })

        // User
        .state('Layout.ListUser', {
            url: "/utilisateurs",
            templateUrl: "/App/Views/User/User.List.html",
            data: { pageTitle: 'Utilisateurs', pageSubTitle: 'Liste des utilisateurs' },
            controller: "UserController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/UserController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateUser', {
            url: "/utilisateurs/ajouter",
            templateUrl: "/App/Views/User/User.Create.html",
            data: { pageTitle: 'Utilisateurs', pageSubTitle: 'Ajouter un utilisateur' },
            controller: "UserController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/UserController.js',
                            '/App/Services/AgencyService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.UserProfil', {
            url: "/mon_profil",
            templateUrl: "/App/Views/User/UserProfil.Index.html",
            data: { pageTitle: 'Profil', pageSubTitle: 'Mon compte' },
            controller: "UserController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/UserController.js'
                        ]
                    });
                }]
            }
        })

        // Task
        .state('Layout.ListTask', {
            url: "/tâches",
            templateUrl: "/App/Views/Task/Task.List.html",
            data: { pageTitle: 'Tâches', pageSubTitle: 'Liste des tâches' },
            controller: "TaskController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/TaskController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/UserService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.CreateTask', {
            url: "/tâches/ajouter",
            templateUrl: "/App/Views/Task/Task.Create.html",
            data: { pageTitle: 'Tâches', pageSubTitle: 'Ajouter une tâche' },
            controller: "TaskController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/TaskController.js',
                            '/App/Services/UserService.js'
                        ]
                    });
                }]
            }
        })
        .state('Layout.UserTasks', {
            url: "/mes_tâches",
            templateUrl: "/App/Views/Task/UserTasks.Index.html",
            data: { pageTitle: 'Tâches', pageSubTitle: 'Mes tâches' },
            controller: "TaskController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/TaskController.js',
                            '/App/Services/TaskService.js'
                        ]
                    });
                }]
            }
        })

        // Notification
        .state('Layout.ListNotification', {
            url: "/notifications",
            templateUrl: "/App/Views/Notification/Notification.List.html",
            data: { pageTitle: 'Notifications', pageSubTitle: 'Liste des notifications' },
            controller: "NotificationController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/NotificationController.js'
                        ]
                    });
                }]
            }
        })

        // Super Administration
        .state('Layout.ListCompany', {
            url: "/societes",
            templateUrl: "/App/Views/Company/Company.List.html",
            data: { pageTitle: 'Sociétés', pageSubTitle: 'Liste des sociétés' },
            controller: "CompanyController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MoneyExchangeApp',
                        files: [
                            '/App/Controllers/CompanyController.js',
                            '/App/Services/DatatableService.js',
                            '/App/Services/Helpers.js'
                        ]
                    });
                }]
            }
        })
        // Create Company
        .state('Layout.CreateCompany', {
            url: "/societes/ajouter",
            templateUrl: "/App/Views/Company/Company.Create.html",
            data: { pageTitle: 'Sociétés', pageSubTitle: 'Ajouter une société' },
            controller: "CompanyController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'MoneyExchangeApp',
                            files: [
                                '/App/Controllers/CompanyController.js',
                                '/App/Services/Helpers.js'
                            ]
                        });
                }]
            }
        })

}]);

/* Init global settings and run the app */
MoneyExchangeApp.run(['$rootScope', 'settings', '$state', function ($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
}]);


