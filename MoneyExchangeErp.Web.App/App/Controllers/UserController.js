﻿MoneyExchangeApp.controller('UserController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector, localStorageService) {

    if ($state.current.name !== 'Layout.UserProfil') {
        // Init user model
        $scope.User = {};
    }

    // State : List
    if ($state.current.name === 'Layout.ListUser') {

        // Init filters
        $scope.Filters = {};

        // Init agencies
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            $scope.Agencies = agencies;
            $scope.Agencies.push({ Id: '*', Name: 'Indifférent', City: '' });
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/User/GetUsers', [0, 1, 2, 3], 'Liste des utilisateurs', ['Nom utilisateur', 'Username']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");

            if (!data.IsMain) {
                if (data.IsBlocked)
                    var banButton = '<button class="btn btn-icon-only btn-circle btn-outline me-green" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir débloquer cet utilisateur?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="ToggleBan(' + obj + ');" placement="left" popover="Débloquer" popover-trigger="mouseenter"><i class="fa fa-unlock"></i></button>';
                else
                    var banButton = '<button class="btn btn-icon-only btn-circle btn-outline me-dark" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir bloquer cet utilisateur?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="ToggleBan(' + obj + ');" placement="left" popover="Bloquer" popover-trigger="mouseenter"><i class="fa fa-ban"></i></button>';
            } else {
                var banButton = '';
            }
            
            return banButton + '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cet utilisateur?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter" ng-hide="' + data.IsMain + '"><i class="fa fa-trash"></i></button>';
        };
        var statusRender = function actionsHtml(data, type, full, meta) {
            if (data === true)
                return '<span class="label label-danger">Suspendu</span>';
            else
                return '<span class="label label-success">Actif</span>';
        };
        var rolesRender = function actionsHtml(data, type, full, meta) {
            if (data.Roles.split('|').includes('Admin'))
                return 'Administrateur';
            else
                return 'Agent';
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'FullName', DisplayName: 'Nom Complet', RenderWith: null },
            { ModelName: 'Username', DisplayName: 'Username', RenderWith: null },
            { ModelName: null, DisplayName: 'Profil', RenderWith: rolesRender },
            { ModelName: 'AgencyName', DisplayName: 'Agence', RenderWith: null },
            { ModelName: 'IsBlocked', DisplayName: 'Statut', RenderWith: statusRender },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Read action
        $scope.Read = function (user) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/User/User.Read.html',
                controller: function ($scope, $modalInstance, User) {
                    $scope.User = User;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    User: function () {
                        return user;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (user) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/User/User.Update.html',
                controller: function ($scope, $modalInstance, User, Agencies) {
                    $scope.User = User;
                    $scope.Agencies = Agencies;
                    $scope.UpdateUser = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            APIProvider.post('User/UpdateUser', $scope.User).then(function (response) {
                                Notification.success({ message: 'Utilisateur modfié.', title: 'Ok !' });
                                $modalInstance.close();
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    User: function () {
                        return user;
                    },
                    Agencies: function () {
                        $scope.AgenciesClone = angular.copy($scope.Agencies);
                        $scope.AgenciesClone.splice(-1, 1);
                        return $scope.AgenciesClone;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (user) {
            $scope.Loading = true;
            APIProvider.post('User/DeleteUser', user).then(function (response) {
                Notification.success({ message: 'Utilisateur Supprimé.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };

        // Toggle ban action
        $scope.ToggleBan = function (User) {
            $scope.User = User;
            $scope.Loading = true;
            APIProvider.post('User/ToggleBanUser', $scope.User).then(function (response) {
                if ($scope.User.IsBlocked)
                    Notification.success({ message: 'Compte utilisateur débloqué.', title: 'Ok !' });
                else
                    Notification.success({ message: 'Compte utilisateur bloqué.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };

    }

    // State : Create
    if ($state.current.name === 'Layout.CreateUser') {

        // Init agencies
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            $scope.Agencies = agencies;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                APIProvider.post('User/CreateUser', $scope.User).then(function (response) {
                    Notification.success({ message: 'Utilisateur enregistré.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };
    }

    // State : User Profil
    if ($state.current.name === 'Layout.UserProfil') {
        // Init user model
        $scope.UserProfil = angular.copy($rootScope.user);

        $scope.CancelProfilInformations = function () {
            $scope.UserProfil = angular.copy($rootScope.user);
        };

        $scope.CancelProfilPassword = function () {
            $scope.UserProfil.CurrentPassword = null;
            $scope.UserProfil.PasswordToHash = null;
            $scope.UserProfil.PasswordConfirm = null;
        };

        $scope.UpdateProfilInformations = function (formModel) {
            if (formModel.$valid) {
                $scope.LoadingProfilInformations = true;
                APIProvider.post('User/UpdateProfilInformations', $scope.UserProfil).then(function (response) {
                    // Update user rootscope
                    $rootScope.user.Civility = $scope.UserProfil.Civility;
                    $rootScope.user.FirstName = $scope.UserProfil.FirstName;
                    $rootScope.user.LastName = $scope.UserProfil.LastName;
                    $rootScope.user.Username = $scope.UserProfil.Username;

                    // Update localstorage
                    localStorageService.set('connectedUser', $rootScope.user);

                    Notification.success({ message: 'Informations modifiées.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.LoadingProfilInformations = false;
                });
            }
        };

        $scope.UpdateProfilPassword = function (formModel) {
            if (formModel.$valid) {
                $scope.LoadingProfilPassword = true;
                APIProvider.post('User/UpdateProfilPassword', $scope.UserProfil).then(function (response) {
                    Notification.success({ message: 'Mot de passe modifié.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.LoadingProfilPassword = false;
                });
            }
        };
    }
});