﻿MoneyExchangeApp.controller('WithdrawalController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector, $filter) {

    // Init operator model
    $scope.Withdrawal = {};

    // State : List
    if ($state.current.name == 'Layout.ListWithdrawal') {

        // Init filters
        $scope.Filters = {};

        // Init agencies
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            $scope.Agencies = agencies;
            $scope.Agencies.push({ Id: '*', Name: 'Indifférent', City: '' });
            if (!$rootScope.user.UserRoles.split('|').includes('Admin')) {
                $scope.Filters.Agency = $rootScope.user.Agency_Id;
            }
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Operation/GetWithdrawals', [1, 2, 3], 'Liste des retraits', ['']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            angular.element("#datatable_filter").hide();
        });

        // Datatable Columns Builder
        var withdrawalIconRender = function actionsHtml(data, type, full, meta) {
            return '<i class="fa fa-long-arrow-left font-red"></i>';
        };
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cette opération ?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-trash"></i></button>';
        };
        var dateRender = function actionsHtml(data, type, full, meta) {
            return $filter('date')(data, 'dd/MM/yyyy HH:mm');
        };
        var currencyRender = function actionsHtml(data, type, full, meta) {
            return $filter('currency')(data, '', 2) + ' DH';
        };
       
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: null, DisplayName: '', RenderWith: withdrawalIconRender },
            { ModelName: 'Date', DisplayName: 'Date', RenderWith: dateRender },
            { ModelName: 'AgencyName', DisplayName: 'Agence', RenderWith: null },
            { ModelName: 'Amount', DisplayName: 'Montant', RenderWith: currencyRender },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Read action
        $scope.Read = function (withdrawal) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Withdrawal/Withdrawal.Read.html',
                controller: function ($scope, $modalInstance, Withdrawal) {
                    $scope.Withdrawal = Withdrawal;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    Withdrawal: function () {
                        return withdrawal;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (withdrawal) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Withdrawal/Withdrawal.Update.html',
                controller: function ($scope, $modalInstance, Withdrawal, Agencies) {
                    $scope.Agencies = Agencies;
                    $scope.Withdrawal = Withdrawal;
                    $scope.UpdateWithdrawal = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            APIProvider.post('Operation/UpdateOperation', $scope.Withdrawal).then(function (response) {
                                Notification.success({ message: 'Retrait modifié.', title: 'Ok !' });
                                $modalInstance.close();
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    Withdrawal: function () {
                        return withdrawal;
                    },
                    Agencies: function () {
                        $scope.AgenciesClone = angular.copy($scope.Agencies);
                        $scope.AgenciesClone.splice(-1, 1);
                        return $scope.AgenciesClone;
                    }

                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (withdrawal) {
            $scope.Loading = true;
            APIProvider.post('Operation/DeleteOperation', withdrawal).then(function (response) {
                Notification.success({ message: 'Retrait supprimé.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name == 'Layout.CreateWithdrawal') {

        // Init agencies
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            $scope.Agencies = agencies;
            if (!$rootScope.user.UserRoles.split('|').includes('Admin')) {
                $scope.Withdrawal.Agency_Id = $rootScope.user.Agency_Id;
            }
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                $scope.Withdrawal.Company_Id = $rootScope.user.Company_Id;
                $scope.Withdrawal.Type = 'W';
                APIProvider.post('Operation/CreateOperation', $scope.Withdrawal).then(function (response) {
                    Notification.success({ message: 'Retrait enregistré.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };
    }

});