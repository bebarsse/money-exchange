﻿MoneyExchangeApp.controller('AttendanceController', function ($rootScope, $scope, $state, $filter, $compile, $modal, $timeout, Notification, $injector, APIProvider) {

    // Init attendance model
    $scope.Attendance = {};

    // State : List
    if ($state.current.name === 'Layout.ListAttendance') {

        // Init filters
        $scope.Filters = {};

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Attendance/GetAttendances', [0, 1, 2], 'Gestion des pointages', ['Utilisateur']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cette tâche ?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter"><i class="fa fa-trash"></i></button>';
        };
        var dateRender = function actionsHtml(data, type, full, meta) {
            return $filter('date')(data, 'dd/MM/yyyy');
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'Date', DisplayName: 'Date', RenderWith: dateRender },
            { ModelName: 'UserName', DisplayName: 'Utilisateur', RenderWith: null },
            { ModelName: 'AgencyName', DisplayName: 'Agence', RenderWith: null },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Read action
        $scope.Read = function (hourRecord) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Attendance/Attendance.Read.html',
                controller: function ($scope, $modalInstance, HourRecord) {
                    $scope.HourRecord = HourRecord;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    HourRecord: function () {
                        return hourRecord;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (hourRecord) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Attendance/Attendance.Update.html',
                controller: function ($scope, $modalInstance, HourRecord) {
                    $scope.HourRecord = HourRecord;
                    $scope.UpdateAttendance = function(attendance) {
                        $scope.Loading = true;
                        // Prepare date
                        var date = new Date(attendance.Date);
                        date.setHours(attendance.Time.split(':')[0]);
                        date.setMinutes(attendance.Time.split(':')[1]);
                        attendance.Date = $filter('date')(date, 'yyyy-MM-dd HH:mm');

                        APIProvider.post('Attendance/UpdateAttendance', attendance).then(function (response) {
                            Notification.success({ message: 'Pointage modfié.', title: 'Ok !' });
                            $state.reload();
                        }, function (rejection) {
                            Notification.error({ message: rejection, title: 'Erreur' });
                        }).finally(function () {
                            $scope.Loading = false;
                        });
                    };

                    $scope.DeleteAttendance = function (attendance) {
                        $scope.Loading = true;
                        APIProvider.post('Attendance/DeleteAttendance', attendance).then(function (response) {
                            Notification.success({ message: 'Pointage Supprimé.', title: 'Ok !' });
                            $state.reload();
                            $modalInstance.close();
                        }, function (rejection) {
                            Notification.error({ message: rejection, title: 'Erreur' });
                        }).finally(function () {
                            $scope.Loading = false;
                        });
                    };

                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    HourRecord: function () {
                        return hourRecord;
                    }
                }
            });
            modalInstance.result.then(function () {
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (hourRecord) {
            $scope.Loading = true;
            APIProvider.post('Attendance/DeleteAttendances', hourRecord).then(function (response) {
                Notification.success({ message: 'Pointage supprimé.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name === 'Layout.CreateAttendance') {

        // Init attendance type
        $scope.LoadingAttendanceType = true;
        $injector.get('AttendanceService').GetLastAttendance($rootScope.user.Id).then(function (lastAttendance) {
            $scope.Attendance.Type = (lastAttendance != null && lastAttendance.Type == 'CI') ? 'CO' : 'CI';
            $scope.LoadingAttendanceType = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });


        // Create action
        $scope.Create = function () {
            $scope.Loading = true;
            $scope.Attendance.User_Id = $rootScope.user.Id;
            $scope.Attendance.Date = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
            APIProvider.post('Attendance/CreateAttendance', $scope.Attendance).then(function (response) {
                Notification.success({ message: 'Pointage enregistré.', title: 'Ok !' });
                $timeout(function () {
                    $scope.Loading = false;
                    $state.reload();
                }, 1500);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
            });
        };
    }
    
});