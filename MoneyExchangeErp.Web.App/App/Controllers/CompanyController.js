﻿MoneyExchangeApp.controller('CompanyController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector) {

    // Init company model
    $scope.Company = {};

    // Init cities
    $scope.Cities = $injector.get('Helpers').GetCities();

    // State : List
    if ($state.current.name == 'Layout.ListCompany') {

        $scope.Cities.push({ name: 'Tout le Maroc', value: '*' });

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Company/GetCompanies', [0, 1, 2, 3, 4, 5, 6], 'Liste des sociétés', ['Nom société', 'Nom du Gérant', 'Tél. Résidentiel', 'Tél. Cellulaire', 'Email']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cette société?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter"><i class="fa fa-trash"></i></button>';
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'CompanyName', DisplayName: 'Société', RenderWith: null },
            { ModelName: 'ParentCompany', DisplayName: 'Société mère', RenderWith: null },
            { ModelName: 'City', DisplayName: 'Ville', RenderWith: null },
            { ModelName: 'AdministratorName', DisplayName: 'Nom du Gérant', RenderWith: null },
            { ModelName: 'Phone', DisplayName: 'Tél. Résidentiel', RenderWith: null },
            { ModelName: 'Phone', DisplayName: 'Tél. Cellulaire', RenderWith: null },
            { ModelName: 'Email', DisplayName: 'Email', RenderWith: null },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Reset Datatable
        //$scope.DtReset = function (initDatatable) {
        //    angular.element('.dataTables_filter input')[0].value = '';
        //    $scope.DtInstance.DataTable.search('').draw();
        //};

        // Read action
        $scope.Read = function (company) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Company/Company.Read.html',
                controller: function ($scope, $modalInstance, Company) {
                    $scope.Company = Company;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    Company: function () {
                        return company;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (company) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Company/Company.Update.html',
                controller: function ($scope, $modalInstance, Company, Cities) {
                    $scope.Company = Company;
                    $scope.Cities = Cities;
                    $scope.UpdateCompany = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            APIProvider.post('Company/UpdateCompany', $scope.Company).then(function (response) {
                                Notification.success({ message: 'Société modfiée.', title: 'Ok !' });
                                $modalInstance.close();
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    Company: function () {
                        return company;
                    },
                    Cities: function () {
                        $scope.CitiesClone = angular.copy($scope.Cities);
                        $scope.CitiesClone.splice(-1, 1);
                        return $scope.CitiesClone;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (company) {
            $scope.Loading = true;
            APIProvider.post('Company/DeleteCompany', company).then(function (response) {
                Notification.success({ message: 'Société supprimée.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    if ($state.current.name == 'Layout.CreateCompany') {

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                APIProvider.post('Company/CreateCompany', $scope.Company).then(function (response) {
                    Notification.success({ message: 'Agence enregistrée.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };
    }
});