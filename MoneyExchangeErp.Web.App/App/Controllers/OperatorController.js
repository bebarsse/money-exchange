﻿MoneyExchangeApp.controller('OperatorController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector) {

    // Init operator model
    $scope.Operator = {};

    // State : List
    if ($state.current.name == 'Layout.ListOperator') {

        // Init filters
        $scope.Filters = {};

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Operator/GetOperators', [0, 1, 2, 3, 4], 'Liste des opérateurs', ['Nom opérateur']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            if (full.IsMain)
                return null;
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cet opérateur?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter"><i class="fa fa-trash"></i></button>';
        };
        var TypeRender = function actionsHtml(data, type, full, meta) {
            var correspondingLabel = null;
            switch (data) {
                case 'I':
                    correspondingLabel = '<span class="label label-success">In</span>';
                    break;
                case 'O':
                    correspondingLabel = '<span class="label label-danger">Out</span>';
                    break;
                case 'IO':
                    correspondingLabel = '<span class="label label-warning">In / Out</span>';
                    break;
            }
            return correspondingLabel;
        };
        var ProfitRender = function actionsHtml(data, type, full, meta) {
            if (full.IsMain === true)
                return '---';
            var correspondingLabel = null;
            if (data.IsByPercentage)
                correspondingLabel = data.Profit + ' %';
            else
                correspondingLabel = data.Profit + ' DH';
            return correspondingLabel;
        };
        var ProfitTypeRender = function actionsHtml(data, type, full, meta) {
            if (full.IsMain === true)
                return '---';
            var correspondingLabel = null;
            if (data)
                correspondingLabel = 'Par %';
            else
                correspondingLabel = 'Par transaction';
            return correspondingLabel;
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'Name', DisplayName: 'Nom de l\'opérateur', RenderWith: null },
            { ModelName: 'Category', DisplayName: 'Catégorie', RenderWith: null },
            { ModelName: 'TransactionType', DisplayName: 'Type des transactions', RenderWith: TypeRender },
            { ModelName: null, DisplayName: 'Commission', RenderWith: ProfitRender },
            { ModelName: 'IsByPercentage', DisplayName: 'Type de commission', RenderWith: ProfitTypeRender },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload action
        $scope.Reload = function (filters) {
            $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                "sName": "advancedFilter",
                "fn": function (aoData) {
                    angular.forEach(filters, function (value, key) {
                        var obj = {};
                        obj[key] = value;
                        angular.extend(aoData, obj);
                    });
                }
            });
            $scope.DtInstance.dataTable.fnUpdate();
        };

        // Read action
        $scope.Read = function (operator) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Operator/Operator.Read.html',
                controller: function ($scope, $modalInstance, Operator) {
                    $scope.Operator = Operator;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    Operator: function () {
                        return operator;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (operator) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Operator/Operator.Update.html',
                controller: function ($scope, $modalInstance, Operator) {
                    $scope.Operator = Operator;
                    $scope.UpdateOperator = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            APIProvider.post('Operator/UpdateOperator', $scope.Operator).then(function (response) {
                                Notification.success({ message: 'Opérateur modifié.', title: 'Ok !' });
                                $modalInstance.close();
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    Operator: function () {
                        return operator;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (operator) {
            $scope.Loading = true;
            APIProvider.post('Operator/DeleteOperator', operator).then(function (response) {
                Notification.success({ message: 'Opérateur supprimé.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name == 'Layout.CreateOperator') {

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                $scope.Operator.Company_Id = $rootScope.user.Company_Id;
                APIProvider.post('Operator/CreateOperator', $scope.Operator).then(function (response) {
                    Notification.success({ message: 'Opérateur enregistré.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };
    }

});