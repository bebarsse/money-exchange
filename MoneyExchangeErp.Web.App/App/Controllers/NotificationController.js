﻿MoneyExchangeApp.controller('NotificationController', function ($rootScope, $scope, APIProvider, Notification, $interval) {

    $scope.Loading = true;
    var GetNotifications = function () {
        APIProvider.get('Notification/GetNotifications', { userId: $rootScope.user.Id }).then(function (notifications) {
            $scope.Notifications = notifications;
            $scope.Loading = false;
        }, function (rejection) {
            Notification.error({ message: rejection, tittle: 'Erreur' });
        });
    };
    GetNotifications();


    var NotificationRefresher = $interval(function () {
        GetNotifications();
        console.log('test');
    }.bind(this), 10000);

    $scope.$on('$destroy', function () {
        $interval.cancel(NotificationRefresher);
    });

});