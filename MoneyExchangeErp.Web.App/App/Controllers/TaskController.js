﻿MoneyExchangeApp.controller('TaskController', function ($rootScope, $scope, $state, $injector, $compile, $modal, $filter, Notification, APIProvider) {

    if ($state.current.name !== 'Layout.UserTasks') {

        // Init task model
        $scope.Task = {};

        // Init users
        $injector.get('UserService').GetUsers($rootScope.user.Company_Id).then(function (users) {
            $scope.Users = users;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    }

    // State : List
    if ($state.current.name === 'Layout.ListTask') {

        // Init filters
        $scope.Filters = {};

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Task/GetTasks', [0, 1, 2, 3, 4], 'Liste des tâches', ['Tâche', 'Utilisateur']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cette tâche ?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter"><i class="fa fa-trash"></i></button>';
        };
        var ImportanceRender = function actionsHtml(data, type, full, meta) {
            if (data === 'High')
                return '<span class="label bg-red">Urgent</span>';
            else if (data === 'Medium')
                return '<span class="label bg-yellow">Moyen</span>';
            else
                return '<span class="label bg-blue">Faible</span>';
        };
        var statusRender = function actionsHtml(data, type, full, meta) {
            if (data === 'Pending')
                return '<span class="label bg-grey">En Cours</span>';
            else
                return '<span class="label bg-green">Tâche Terminée</span>';
        };
        var dateRender = function actionsHtml(data, type, full, meta) {
            return $filter('date')(data, 'dd/MM/yyyy HH:mm');
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'Title', DisplayName: 'Tâche', RenderWith: null },
            { ModelName: 'UserName', DisplayName: 'Utilisateur', RenderWith: null },
            { ModelName: 'Date', DisplayName: 'Date', RenderWith: dateRender },
            { ModelName: 'Importance', DisplayName: 'Importance', RenderWith: ImportanceRender },
            { ModelName: 'Status', DisplayName: 'Statut', RenderWith: statusRender },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Read action
        $scope.Read = function (task) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Task/Task.Read.html',
                controller: function ($scope, $modalInstance, Task) {
                    $scope.Task = Task;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    Task: function () {
                        return task;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (task) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Task/Task.Update.html',
                controller: function ($scope, $modalInstance, Task, Users) {
                    $scope.Users = Users;
                    $scope.Task = Task;
                    $scope.UpdateTask = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            APIProvider.post('Task/UpdateTask', $scope.Task).then(function (response) {
                                Notification.success({ message: 'Tâche modfiée.', title: 'Ok !' });
                                $modalInstance.close();
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    Task: function () {
                        return task;
                    },
                    Users: function () {
                        return $scope.Users;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (task) {
            $scope.Loading = true;
            APIProvider.post('Task/DeleteTask', task).then(function (response) {
                Notification.success({ message: 'Tâche Supprimée.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name === 'Layout.CreateTask') {

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                $scope.Task.Company_Id = $rootScope.user.Company_Id;
                APIProvider.post('Task/CreateTask', $scope.Task).then(function (response) {
                    Notification.success({ message: 'Tâche enregistrée.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };
    }

    // State : UserTask
    if ($state.current.name === 'Layout.UserTasks') {

        // Init user tasks
        $scope.LoadingTasks = true;
        $injector.get('TaskService').GetUserTasks($rootScope.user.Id).then(function (userTask) {
            $scope.UserTask = userTask;
            $scope.LoadingTasks = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        $scope.CloseTask = function (task) {
            $scope.Loading = true;
            APIProvider.post('Task/CloseTask', task).then(function (response) {
                Notification.success({ message: 'Tâche fermée.', title: 'Ok !' });
                $state.reload();
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        }
    }

});