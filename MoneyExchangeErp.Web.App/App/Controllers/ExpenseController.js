﻿MoneyExchangeApp.controller('ExpenseController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector, $filter) {

    // Init operator model
    $scope.Expense = {};

    // State : List
    if ($state.current.name == 'Layout.ListExpense') {

        // Init filters
        $scope.Filters = {};

        // Init categories
        $injector.get('CategoryService').GetCategories($rootScope.user.Company_Id).then(function (categories) {
            $scope.Categories = categories;
            $scope.Categories.push({ Id: '*', Name: 'Indifférent' });
            if (!$rootScope.user.UserRoles.split('|').includes('Admin')) {
                $scope.Filters.Agency = $rootScope.user.Agency_Id;
            }

        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Init agencies
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            $scope.Agencies = agencies;
            $scope.Agencies.push({ Id: '*', Name: 'Indifférent', City: '' });
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Expense/GetExpenses', [0, 1, 2, 3, 4], 'Liste des dépenses', ['Désignation']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cette dépense ?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-trash"></i></button>';
        };
        var dateRender = function actionsHtml(data, type, full, meta) {
            return $filter('date')(data, 'dd/MM/yyyy HH:mm');
        };
        var currencyRender = function actionsHtml(data, type, full, meta) {
            return $filter('currency')(data, '', 2) + ' DH';
        };
       
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'Date', DisplayName: 'Date', RenderWith: dateRender },
            { ModelName: 'AgencyName', DisplayName: 'Agence', RenderWith: null },
            { ModelName: 'Name', DisplayName: 'Désignation', RenderWith: null },
            { ModelName: 'Amount', DisplayName: 'Montant', RenderWith: currencyRender },
            { ModelName: 'CategoryName', DisplayName: 'Catégorie', RenderWith: null },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Read action
        $scope.Read = function (expense) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Expense/Expense.Read.html',
                controller: function ($scope, $modalInstance, Expense) {
                    $scope.Expense = Expense;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    Expense: function () {
                        return expense;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (expense) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Expense/Expense.Update.html',
                controller: function ($scope, $modalInstance, Expense, Agencies, Categories) {
                    $scope.Agencies = Agencies;
                    $scope.Categories = Categories;
                    $scope.Expense = Expense;
                    $scope.UpdateExpense = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            APIProvider.post('Expense/UpdateExpense', $scope.Expense).then(function (response) {
                                Notification.success({ message: 'Dépense modifiée.', title: 'Ok !' });
                                $modalInstance.close();
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    Expense: function () {
                        return expense;
                    },
                    Agencies: function () {
                        $scope.AgenciesClone = angular.copy($scope.Agencies);
                        $scope.AgenciesClone.splice(-1, 1);
                        return $scope.AgenciesClone;
                    },
                    Categories: function () {
                        $scope.CategoriesClone = angular.copy($scope.Categories);
                        $scope.CategoriesClone.splice(-1, 1);
                        return $scope.CategoriesClone;
                    }

                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (expense) {
            $scope.Loading = true;
            APIProvider.post('Expense/DeleteExpense', expense).then(function (response) {
                Notification.success({ message: 'Dépense supprimée.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name == 'Layout.CreateExpense') {

        // Init categories
        $injector.get('CategoryService').GetCategories($rootScope.user.Company_Id).then(function (categories) {
            $scope.Categories = categories;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Init agencies
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            $scope.Agencies = agencies;
            if (!$rootScope.user.UserRoles.split('|').includes('Admin')) {
                $scope.Expense.Agency_Id = $rootScope.user.Agency_Id;
            }
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                $scope.Expense.Company_Id = $rootScope.user.Company_Id;
                APIProvider.post('Expense/CreateExpense', $scope.Expense).then(function (response) {
                    Notification.success({ message: 'Dépense enregistrée.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };

        // Create new Category
        $scope.CreateCategory = function () {

            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Expense/Expense.Category.Create.html',
                controller: function ($scope, $modalInstance) {

                    $scope.Create = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            $scope.Category.Company_Id = $rootScope.user.Company_Id;
                            APIProvider.post('Category/CreateCategory', $scope.Category).then(function (response) {
                                Notification.success({ message: 'Catégorie enregistrée.', title: 'Ok !' });
                                $modalInstance.close(response);
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };

                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                size: 'lg',
                resolve: {
                    
                }
            });

            modalInstance.result.then(function (categoryId) {
                $injector.get('CategoryService').GetCategories($rootScope.user.Company_Id).then(function (categories) {
                    $scope.Categories = categories;
                    $scope.Expense.Category_Id = categoryId;
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                });
            }, function () {
            });

        };
    }

});