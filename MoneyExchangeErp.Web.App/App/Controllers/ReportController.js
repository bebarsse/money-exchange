﻿MoneyExchangeApp.controller('ReportController', function ($rootScope, $scope, $http, $q, $timeout, $state, $filter, Notification, ReportService) {

    // State : Daily Report
    if ($state.current.name == 'Layout.DailyReport') {

        $scope.GetReport = function (index, day) {
            ReportService.GetDailyReport($rootScope.user.Company_Id, day).then(function (dailyReport) {
                $scope.DailyReports = dailyReport;
                if (!$rootScope.user.UserRoles.split('|').includes('Admin')) {
                    angular.forEach($scope.DailyReports, function (report, key) {
                        if (report.AgencyName == $rootScope.user.AgencyName) {
                            index = key;
                        }
                    });
                }
                $scope.SetCurrentAgency(index, $scope.DailyReports[index]);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
                deferred.reject(rejection);
            });
        };

        $scope.Index = 0;
        $scope.GetReport($scope.Index, $filter('date')(new Date(), "yyyy-MM-dd HH:mm:ss"));
        

        $scope.SetCurrentAgency = function (index, agency) {
            $scope.Index = index;
            $scope.CurrentAgency = agency;
        };
    }

    // State : Monthly Report
    if ($state.current.name == 'Layout.MonthlyReport') {

        // Init Months
        $scope.Months = [{ 'name': 'Janvier', 'value': 0 },
            { 'name': 'Février', 'value': 1 },
            { 'name': 'Mars', 'value': 2 },
            { 'name': 'Avril', 'value': 3 },
            { 'name': 'Mai', 'value': 4 },
            { 'name': 'Juin', 'value': 5 },
            { 'name': 'Juillet', 'value': 6 },
            { 'name': 'Août', 'value': 7 },
            { 'name': 'Septembre', 'value': 8 },
            { 'name': 'Octobre', 'value': 9 },
            { 'name': 'Novembre', 'value': 10 },
            { 'name': 'Décembre', 'value': 11 }];
        $scope.Month = new Date().getMonth();
        
        // Init Years
        $scope.Year = new Date().getFullYear();

        $scope.Years = [];
        for (var i = 0; i < 10; i++) {
            $scope.Years.push($scope.Year - i);
        }

        $scope.GetMonthlyReport = function (index, month, year) {
            ReportService.GetMonthlyReport($rootScope.user.Company_Id, month, year).then(function (monthlyReport) {
                $scope.MonthlyReports = monthlyReport;
                $scope.SetCurrentAgency(index, $scope.MonthlyReports[index]);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            });
        };

        $scope.Index = 0;
        $scope.GetMonthlyReport($scope.Index, $scope.Month, $scope.Year);

        $scope.SetCurrentAgency = function (index, agency) {
            $scope.Index = index;
            $scope.CurrentAgency = agency;
        };
    }

    // State : Expense Report
    if ($state.current.name == 'Layout.ExpenseReport') {

        // Init Years
        var currentYear = new Date().getFullYear();
        $scope.Year = currentYear;

        $scope.Years = [];
        for (var i = 0; i < 10; i++) {
            $scope.Years.push(currentYear - i);
        }

        $scope.GetExpenseReport = function (index, year) {
            $scope.Loading = true;
            ReportService.GetExpenseReport($rootScope.user.Company_Id, year).then(function (expenseReport) {
                $scope.ExpenseReport = expenseReport;
                $scope.SetCurrentAgency(index, $scope.ExpenseReport[index]);
                $scope.Loading = false;
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            });
        };

        $scope.Index = 0;
        $scope.GetExpenseReport($scope.Index, currentYear);

        $scope.SetCurrentAgency = function (currentIndex, agency) {
            $scope.Index = currentIndex;
            $scope.CurrentAgency = agency;
            // Monthly chart data
            $scope.CurrentAgency.MonthlyExpenseChartData = []
            angular.forEach(['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre',], function (month) {
                $scope.CurrentAgency.MonthlyExpenseChartData.push({ 'Month': month, 'Volume': 0 });
            });
            angular.forEach($scope.CurrentAgency.MonthlyExpenses, function (expense) {
                var index = $scope.CurrentAgency.MonthlyExpenseChartData.indexOf($filter('filter')($scope.CurrentAgency.MonthlyExpenseChartData, { Month: expense.Month }, true)[0]);
                $scope.CurrentAgency.MonthlyExpenseChartData[index].Volume = expense.Volume;
            });
            ReportService.GetMonthlyExpenseChart($scope.CurrentAgency.MonthlyExpenseChartData);

            // Category chart data
            $scope.CurrentAgency.CategoryExpenseChartData = []
            angular.forEach($scope.CurrentAgency.CategoryExpenses, function (expense) {
                $scope.CurrentAgency.CategoryExpenseChartData.push({ 'Category': expense.CategoryName, 'Volume': expense.Volume });
            });

            ReportService.GetCategoryExpenseChart($scope.CurrentAgency.CategoryExpenseChartData);
        };

    }

    // State : Profit Report
    if ($state.current.name == 'Layout.ProfitReport') {

        // Init Years
        $scope.Year = new Date().getFullYear();

        $scope.Years = [];
        for (var i = 0; i < 10; i++) {
            $scope.Years.push($scope.Year - i);
        }

        $scope.GetProfitReport = function (index, year) {
            $scope.Loading = true;
            ReportService.GetProfitReport($rootScope.user.Company_Id, year).then(function (profitReport) {
                $scope.ProfitReport = profitReport;
                $scope.SetCurrentAgency(index, $scope.ProfitReport[index]);
                $scope.Loading = false;

            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            });
        };

        $scope.Index = 0;
        $scope.GetProfitReport($scope.Index, $scope.Year);

        $scope.SetCurrentAgency = function (currentIndex, agency) {
            $scope.Index = currentIndex;
            $scope.CurrentAgency = agency;

            // Monthly chart data
            $scope.CurrentAgency.MonthlyProfitChartData = []
            angular.forEach(['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre',], function (month) {
                $scope.CurrentAgency.MonthlyProfitChartData.push({ 'Month': month, 'Profit': 0, 'Expense': 0});
            });
            angular.forEach($scope.CurrentAgency.MonthlyProfits, function (profit) {
                var index = $scope.CurrentAgency.MonthlyProfitChartData.indexOf($filter('filter')($scope.CurrentAgency.MonthlyProfitChartData, { Month: profit.Month }, true)[0]);
                $scope.CurrentAgency.MonthlyProfitChartData[index].Profit = profit.Volume;
            });
            angular.forEach($scope.CurrentAgency.MonthlyExpenses, function (expense) {
                var index = $scope.CurrentAgency.MonthlyProfitChartData.indexOf($filter('filter')($scope.CurrentAgency.MonthlyProfitChartData, { Month: expense.Month }, true)[0]);
                $scope.CurrentAgency.MonthlyProfitChartData[index].Expense = expense.Volume;
            });
            ReportService.GetMonthlyProfitChart($scope.CurrentAgency.MonthlyProfitChartData);

            // Category chart data
            $scope.CurrentAgency.OperatorProfitChartData = []
            angular.forEach($scope.CurrentAgency.OperatorProfits, function (profit) {
                $scope.CurrentAgency.OperatorProfitChartData.push({ 'Operator': profit.OperatorName, 'Volume': profit.Volume });
            });

            ReportService.GetOperatorProfitChart($scope.CurrentAgency.OperatorProfitChartData);
            
        };

    }

});