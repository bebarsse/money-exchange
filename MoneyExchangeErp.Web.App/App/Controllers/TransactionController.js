﻿MoneyExchangeApp.controller('TransactionController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector, $filter) {
    
    // Init operator model
    $scope.Transaction = {};

    // Inject FileUploaderService
    var FileUploaderService = $injector.get('FileUploaderService');

    // Inject CustomerService
    var CustomerService = $injector.get('CustomerService');

    // Inject OperatorService
    var OperatorService = $injector.get('OperatorService');

    // Init operators
    OperatorService.GetOperators($rootScope.user.Company_Id).then(function (operators) {
        if ($state.current.name == 'Layout.ListTransaction') {
            operators.push({ Id: '*', Name: 'Indifférent' });
            $scope.Operators = operators;
        }
        else
            $scope.Operators = operators;
    }, function (rejection) {
        Notification.error({ message: rejection, title: 'Erreur' });
    });

    // State : List
    if ($state.current.name == 'Layout.ListTransaction') {

        // Init filters
        $scope.Filters = {};

        // Inject AgencyService
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            agencies.push({ Id: '*', Name: 'Indifférent', City: '' });
            $scope.Agencies = agencies;
            if (!$rootScope.user.UserRoles.split('|').includes('Admin')) {
                $scope.Filters.Agency = $rootScope.user.Agency_Id;
            }
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Inject DatatableService 
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Transaction/GetTransactions', [0, 1, 2, 3, 4, 5, 6], 'Liste des transactions', ['C.I.N client', 'Nom client', 'Nom agent']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer cette transaction?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-trash"></i></button>';
        };
        var transactionTypeRender = function actionsHtml(data, type, full, meta) {
            var correspondingLabel = null;
            switch (data) {
                case 'I':
                    correspondingLabel = '<span class="label label-success">In</span>';
                    break;
                case 'O':
                    correspondingLabel = '<span class="label label-danger">Out</span>';
                    break;
                case 'IO':
                    correspondingLabel = '<span class="label label-warning">In / Out</span>';
                    break;
            }
            return correspondingLabel;
        };
        var dateRender = function actionsHtml(data, type, full, meta) {
            return $filter('date')(data, 'dd/MM/yyyy HH:mm');
        };
        var currencyRender = function actionsHtml(data, type, full, meta) {
            return $filter('currency')(data, '', 2) + ' DH';
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'Date', DisplayName: 'Date/Heure', RenderWith: dateRender },
            { ModelName: 'CustomerName', DisplayName: 'Client', RenderWith: null },
            { ModelName: 'OperatorName', DisplayName: 'Nom de l\'opérateur', RenderWith: null },
            { ModelName: 'TransactionType', DisplayName: 'Type de la trs', RenderWith: transactionTypeRender },
            { ModelName: 'Amount', DisplayName: 'Montant', RenderWith: currencyRender },
            { ModelName: 'Profit', DisplayName: 'Commission', RenderWith: currencyRender },
            { ModelName: 'AgencyName', DisplayName: 'Agence', RenderWith: null },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        // Read action
        $scope.Read = function (transaction) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Transaction/Transaction.Read.html',
                controller: function ($scope, $modalInstance, Transaction, CustomerService) {
                    CustomerService.GetCustomerById(Transaction.Customer_Id).then(function (customer) {
                        $scope.Transaction.Customer = customer;
                    }, function (rejection) {
                        Notification.error({ message: rejection, title: 'Erreur' });
                    });
                    $scope.Transaction = Transaction;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                    $scope.DownloadTransactionFile = function (filePath) {
                        if (filePath) {
                            APIProvider.get('Transaction/DownloadTransactionFile', { filePath: filePath }, { responseType: 'blob' }).then(function (response) {
                                var blob = new Blob([response], { type: "application/octet-stream" });
                                $scope.UrlDownload = (window.URL || window.webkitURL).createObjectURL(blob);
                            }, function (rejection) {
                                Notification.error({
                                    message: 'Une erreur s\'est produite. Veuillez réessayer ou contacter le support technique.', title: 'Erreur'
                                });
                            }).finally(function () {

                            });
                        }
                    };
                    $scope.DownloadTransactionFile($scope.Transaction.FilePath);
                },
                size: 'lg',
                resolve: {
                    Transaction: function () {
                        return transaction;
                    },
                    CustomerService: function () {
                        return CustomerService;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (transaction) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Transaction/Transaction.Update.html',
                controller: function ($scope, $modalInstance, Transaction, Operators, OperatorService, FileUploaderService) {
                    //Inject FileUploaderService
                    $scope.uploader = FileUploaderService.Init('Transaction/UploadTransactionFile');
                    // Init
                    $scope.Customer = {};
                    $scope.Operators = Operators;
                    $scope.Transaction = Transaction;
                    OperatorService.GetOperatorById(Transaction.Operator_Id).then(function (operator) {
                        $scope.Transaction.Operator = operator;
                    }, function (rejection) {
                        Notification.error({ message: rejection, title: 'Erreur' });
                    });
                    CustomerService.GetCustomerById(Transaction.Customer_Id).then(function (customer) {
                        $scope.Transaction.Customer = {};
                        $scope.Transaction.Customer.CIN = customer.CIN;
                    }, function (rejection) {
                        Notification.error({ message: rejection, title: 'Erreur' });
                    });

                    // Check if customer exist
                    $scope.$watch('Transaction.Customer.CIN', function (newValue, oldValue) {
                        if (newValue != undefined) {
                            $scope.CheckerLoading = true;
                            CustomerService.GetCustomerByCin($rootScope.user.Company_Id, newValue)
                                .then(function (customer) {
                                    if (customer) {
                                        $scope.Transaction.Customer = customer;
                                        $scope.CustomerExist = true;
                                    } else {
                                        $scope.CustomerExist = false;
                                        $scope.Customer.CIN = angular.copy($scope.Transaction.Customer.CIN);
                                    }
                                }, function (rejection) {
                                    Notification.error({ message: rejection, title: 'Erreur' });
                                })
                                .finally(function () {
                                    $scope.CheckerLoading = false;
                                });
                        } else {
                            $scope.CustomerExist = false;
                        }
                    }, true);

                    // Download transaction file if exist
                    $scope.DownloadTransactionFile = function (filePath) {
                        if (filePath) {
                            APIProvider.get('Transaction/DownloadTransactionFile', { filePath: filePath }, { responseType: 'blob' }).then(function (response) {
                                var blob = new Blob([response], { type: "application/octet-stream" });
                                $scope.UrlDownload = (window.URL || window.webkitURL).createObjectURL(blob);
                            }, function (rejection) {
                                Notification.error({
                                    message: 'Une erreur s\'est produite. Veuillez réessayer ou contacter le support technique.', title: 'Erreur'
                                });
                            }).finally(function () {

                            });
                        }
                    };
                    $scope.DownloadTransactionFile($scope.Transaction.FilePath);

                    // Check all customer field
                    $scope.$watchGroup(['Customer.Civility', 'Customer.FirstName', 'Customer.LastName', 'Customer.CIN'], function (newValues, oldValues) {
                        if (newValues[0] == undefined || newValues[0] == '' || newValues[1] == undefined || newValues[1] == '' || newValues[2] == undefined || newValues[2] == '' || newValues[3] == undefined || newValues[3] == '') {
                            $scope.CreateCustomerBtnDisabled = true;
                        } else {
                            $scope.CreateCustomerBtnDisabled = false;
                        }
                    }, true);

                    // Calculate transaction fees and profit
                    $scope.$watchGroup(['Transaction.Operator', 'Transaction.Amount', 'Transaction.TransactionType', 'Transaction.FeesPaidBy', 'Transaction.WithSMSOption'], function (newValues, oldValues) {
                        if (newValues[0] != undefined) {
                            $scope.IsMainOperator = newValues[0].IsMain;
                            if (!newValues[0].IsMain) {
                                $scope.Transaction.TransactionType = newValues[0].TransactionType;
                            }
                        }
                        $scope.Transaction.Fees = calculateFees(newValues[0], newValues[1], newValues[2], newValues[3], newValues[4]);
                        $scope.Transaction.Profit = calculateProfit(newValues[0], newValues[1], newValues[2], newValues[3], newValues[4], $scope.Transaction.Fees)
                    }, true);

                    // Create Customer
                    $scope.CreateCustomer = function () {
                        $scope.CreateCustomerLoading = true;
                        $scope.Customer.Company_Id = $rootScope.user.Company_Id;
                        $scope.Customer.Agency_Id = $rootScope.user.Agency_Id;
                        APIProvider.post('Customer/CreateCustomer', $scope.Customer).then(function (response) {
                            Notification.success({ message: 'Client enregistré.', title: 'Ok !' });
                            $scope.CustomerExist = true;
                            $scope.Transaction.Customer.CIN = $scope.Customer.CIN;

                            // Set the new User
                            CustomerService.GetCustomerByCin($rootScope.user.Company_Id, $scope.Transaction.Customer.CIN)
                                .then(function (customer) {
                                    if (customer) {
                                        $scope.Transaction.Customer = customer;
                                        $scope.CustomerExist = true;
                                    } else {
                                        $scope.CustomerExist = false;
                                        $scope.Customer.CIN = angular.copy($scope.Transaction.Customer.CIN);
                                    }
                                }, function (rejection) {
                                    Notification.error({ message: rejection, title: 'Erreur' });
                                })
                                .finally(function () {
                                    $scope.CheckerLoading = false;
                                });

                            // Clear customer informations
                            $scope.Customer = {};
                        }, function (rejection) {
                            Notification.error({ message: rejection, title: 'Erreur' });
                        }).finally(function () {
                            $scope.CreateCustomerLoading = false;
                        });
                    };

                    // Update Transaction
                    $scope.UpdateTransaction = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            $scope.Transaction.Customer_Id = $scope.Transaction.Customer.Id;
                            $scope.Transaction.Operator_Id = $scope.Transaction.Operator.Id;
                            $scope.Transaction.AddedBy = $rootScope.user.FirstName.concat(' ').concat($rootScope.user.LastName);

                            if ($scope.Transaction.Operator.IsMain && $scope.Transaction.FeesPaidBy == 'R' && $scope.Transaction.TransactionType == 'I') {
                                $scope.Transaction.Fees = 0;
                            }

                            if ($scope.Transaction.Operator.IsMain && $scope.Transaction.FeesPaidBy == 'S' && $scope.Transaction.TransactionType == 'I') {
                                $scope.Transaction.Fees = ($scope.Transaction.Fees / 2);
                            }

                            if ($scope.Transaction.TransactionType == 'O') {
                                $scope.Transaction.Fees = 0;
                            }

                            if (!$scope.Transaction.Operator.IsMain) {
                                $scope.Transaction.FeesPaidBy = null;
                                $scope.Transaction.withSMSOption = null;
                            }
                            // Update File
                            if ($scope.uploader.queue.length != 0) {
                                $scope.uploader.onBeforeUploadItem = function (item) {
                                    item.formData.push({ Id: $scope.Transaction.Id });
                                };
                                $scope.uploader.onSuccessItem = function (item, response, status, headers) {
                                    APIProvider.post('Transaction/UpdateTransaction', $scope.Transaction).then(function (response) {
                                        Notification.success({ message: 'Transaction modfiée.', title: 'Ok !' });
                                        $modalInstance.close();
                                    }, function (rejection) {
                                        Notification.error({ message: rejection, title: 'Erreur' });
                                    }).finally(function () {
                                        $scope.Loading = false;
                                    });
                                }
                                $scope.uploader.uploadAll();
                            } else {
                                APIProvider.post('Transaction/UpdateTransaction', $scope.Transaction).then(function (response) {
                                    Notification.success({ message: 'Transaction modfiée.', title: 'Ok !' });
                                    $modalInstance.close();
                                }, function (rejection) {
                                    Notification.error({ message: rejection, title: 'Erreur' });
                                }).finally(function () {
                                    $scope.Loading = false;
                                });
                            }

                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                size: 'lg',
                resolve: {
                    Transaction: function () {
                        return transaction;
                    },
                    Operators: function () {
                        $scope.OperatorsClone = angular.copy($scope.Operators);
                        $scope.OperatorsClone.splice(-1, 1);
                        return $scope.OperatorsClone;
                    },
                    OperatorService: function () {
                        return OperatorService;
                    },
                    FileUploaderService: function () {
                        return FileUploaderService;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        };

        // Delete action
        $scope.Delete = function (transaction) {
            $scope.Loading = true;
            APIProvider.post('Transaction/DeleteTransaction', transaction).then(function (response) {
                Notification.success({ message: 'Transaction Supprimée.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name == 'Layout.CreateTransaction') {

        // Inject FileUploaderService
        $scope.uploader = FileUploaderService.Init('Transaction/UploadTransactionFile');

        // Check if customer exist
        $scope.$watch('Transaction.Customer.CIN', function (newValue, oldValue) {
            if (newValue != undefined) {
                $scope.CheckerLoading = true;
                CustomerService.GetCustomerByCin($rootScope.user.Company_Id, newValue)
                    .then(function (customer) {
                        if (customer) {
                            $scope.Transaction.Customer = customer;
                            $scope.CustomerExist = true;
                        } else {
                            $scope.CustomerExist = false;
                        }
                    }, function (rejection) {
                        Notification.error({ message: rejection, title: 'Erreur' });
                    })
                    .finally(function () {
                        $scope.CheckerLoading = false;
                    });
            } else {
                $scope.CustomerExist = false;
            }
        }, true);

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid && $scope.Transaction.Amount != 0 && $scope.CustomerExist) {
                $scope.Loading = true;
                $scope.Transaction.Company_Id = $rootScope.user.Company_Id;
                $scope.Transaction.Agency_Id = $rootScope.user.Agency_Id;
                $scope.Transaction.Operator_Id = $scope.Transaction.Operator.Id;
                $scope.Transaction.Customer_Id = $scope.Transaction.Customer.Id;
                $scope.Transaction.AddedBy = $rootScope.user.FirstName.concat(' ').concat($rootScope.user.LastName);

                if ($scope.Transaction.Operator.IsMain && $scope.Transaction.FeesPaidBy == 'R' && $scope.Transaction.TransactionType == 'I') {
                    $scope.Transaction.Fees = 0;
                }

                if ($scope.Transaction.Operator.IsMain && $scope.Transaction.FeesPaidBy == 'S' && $scope.Transaction.TransactionType == 'I') {
                    $scope.Transaction.Fees = ($scope.Transaction.Fees / 2);
                }

                if ($scope.Transaction.TransactionType == 'O') {
                    $scope.Transaction.Fees = 0;
                } 

                if (!$scope.Transaction.Operator.IsMain) {
                    $scope.Transaction.FeesPaidBy = null;
                    $scope.Transaction.withSMSOption = null;
                }
                APIProvider.post('Transaction/CreateTransaction', $scope.Transaction).then(function (response) {
                    $scope.uploader.onBeforeUploadItem = function (item) {
                        item.formData.push({ Id: response });
                    };
                    $scope.uploader.uploadAll();
                    Notification.success({ message: 'Transaction Enregistrée.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };

        // Create new Customer
        $scope.CreateCustomer = function () {

            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Transaction/Transaction.Customer.Create.html',
                controller: function ($scope, $modalInstance, CIN) {

                    // Init customer model
                    $scope.Customer = {};
                    $scope.Customer.CIN = CIN;

                    // Inject FileUploaderService
                    $scope.customerUploader = $injector.get('FileUploaderService').Init('Customer/UploadCustomerFile');

                    // Check if customer exist
                    $scope.$watch('Customer.CIN', function (newValue, oldValue) {
                        if (newValue != undefined) {
                            CustomerService.GetCustomerByCin($rootScope.user.Company_Id, newValue).then(function (customer) {
                                $scope.CustomerExist = (customer) ? true : false
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            });
                        }
                    }, true);

                    // Create customer action
                    $scope.Create = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            $scope.Customer.Company_Id = $rootScope.user.Company_Id;
                            $scope.Customer.Agency_Id = $rootScope.user.Agency_Id;
                            APIProvider.post('Customer/CreateCustomer', $scope.Customer).then(function (response) {
                                $scope.Customer.Id = response;
                                $scope.customerUploader.onBeforeUploadItem = function (item) {
                                    item.formData.push({ Id: response });
                                };
                                $scope.customerUploader.uploadAll();
                                Notification.success({ message: 'Client enregistré.', title: 'Ok !' });
                                $modalInstance.close($scope.Customer);
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur !' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };

                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                size: 'lg',
                resolve: {
                    CIN: function () {
                        return $scope.Transaction.Customer.CIN;
                    }
                }
            });

            modalInstance.result.then(function (customer) {
                $scope.Transaction.Customer = customer;
                $scope.CustomerExist = true;
            }, function () {
            });

        };

        //View Logic
        $scope.IsMainOperator = false;
        $scope.Transaction.Amount = 0;
        $scope.Transaction.Fees = 0;
        $scope.Transaction.Profit = 0;

        // Calculate transaction fees and profit
        $scope.$watchGroup(['Transaction.Operator', 'Transaction.Amount', 'Transaction.TransactionType', 'Transaction.FeesPaidBy', 'Transaction.WithSMSOption'], function (newValues, oldValues) {
            if (newValues[0] != undefined) {
                $scope.IsMainOperator = newValues[0].IsMain;
                if (!newValues[0].IsMain) {
                    $scope.Transaction.TransactionType = newValues[0].TransactionType;
                }
            }
            $scope.Transaction.Fees = calculateFees(newValues[0], newValues[1], newValues[2], newValues[3], newValues[4]);
            $scope.Transaction.Profit = calculateProfit(newValues[0], newValues[1], newValues[2], newValues[3], newValues[4], $scope.Transaction.Fees)
        }, true);
    }

    //--- Internal Functions
    var calculateFees = function (operator, amount, transactionType, feesPaidBy, withSMSOption) {
        if (operator == undefined || amount == undefined || amount == 0)
            return 0;
        if (operator.IsMain) {
            var fees = 0;
            switch (operator.Name) {
                case 'CashPlus':
                    fees = getCahsPlusFees(amount);
                    break;
            }

            if (withSMSOption) {
                fees += 3;
            }

            return fees;

        } else {
            return 0;
        }
    };

    var calculateProfit = function (operator, amount, transactionType, feesPaidBy, withSMSOption, fees) {
        if (operator == undefined) {
            return 0;
        }
        if (operator.IsMain) {
            if (transactionType == 'I') {
                return (withSMSOption) ? (fees - 3) * 0.2 : fees * 0.2;
            }

            if (transactionType == 'O') {
                if (feesPaidBy == 'E') {
                    return fees * 0.55;
                } else {
                    //return (withSMSOption) ? (getCahsPlusFees(getCahsPlusFees(amount) + amount) + 3) * 0.55 : getCahsPlusFees(getCahsPlusFees(amount) + amount) * 0.55;
                    return getCahsPlusFees(getCahsPlusFees(amount) + amount) * 0.55;
                }
            }

            return 0;
        } else {
            if (operator.IsByPercentage) {
                return (amount * operator.Profit) / 100;
            } else {
                return operator.Profit;
            }
        }
    }

    var getCahsPlusFees = function (amount) {
        if (amount <= 500) {
            return 28;
        } else if (amount <= 1000) {
            return 37;
        } else if (amount <= 3000) {
            return 47;
        } else if (amount <= 5000) {
            return 57;
        } else if (amount <= 10000) {
            return 67;
        }else {
            return amount * 0.67 / 100;
        }
    };
    
});