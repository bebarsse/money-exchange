﻿MoneyExchangeApp.controller('AuthenticationController', function ($rootScope, $scope, $http, $timeout, $state, AuthenticationProvider, Notification, $location) {

    // Login action
    $scope.Login = function (isValid, username, password, rememberMe) {
        if (isValid) {
            $scope.loading = true;
            AuthenticationProvider.login(username, password, rememberMe)
            .then(function () {
                $state.go('Layout.QuickAccess');
            }, function () {
                Notification.error({ message: 'Username ou Password incorrect.', title: 'Erreur' });
            })
            .finally(function () {
                $scope.loading = false;
            })
        }
    };

    $scope.RetrievePassword = function (isValid) {
        if (isValid) {
            alert('Email retrieved');
        }
    };

});