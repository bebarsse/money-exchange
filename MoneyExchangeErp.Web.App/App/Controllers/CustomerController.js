﻿MoneyExchangeApp.controller('CustomerController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector, $filter) {

    // Init customer model
    $scope.Customer = {};

    // Init FileUploaderService
    var FileUploaderService = $injector.get('FileUploaderService');

    // Inject CustomerService
    var CustomerService = $injector.get('CustomerService');

    // State : List
    if ($state.current.name == 'Layout.ListCustomer') {

        // Init filters
        $scope.Filters = {};

        // Inject AgencyService
        $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
            agencies.push({ Id: '*', Name: 'Indifférent', City: 'Indifférent' });
            $scope.Agencies = agencies;
            if (!$rootScope.user.UserRoles.split('|').includes('Admin')) {
                $scope.Filters.Agency = $rootScope.user.Agency_Id;
            }
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Customer/GetCustomers', [0, 1, 2, 3, 4, 5], 'Liste des clients', ['Nom Client', 'C.I.N', 'Téléphone 1', 'Téléphone 2', 'Email']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="Êtes-vous sûr de vouloir supprimer ce client?" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter" ng-show="user.UserRoles.split(\'|\').includes(\'Admin\')"><i class="fa fa-trash"></i></button>';
        };
        var dateRender = function actionsHtml(data, type, full, meta) {
            return $filter('date')(data, 'dd/MM/yyyy');
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'CompleteName', DisplayName: 'Nom complet', RenderWith: null },
            { ModelName: 'CIN', DisplayName: 'C.I.N', RenderWith: null },
            { ModelName: 'BirthDay', DisplayName: 'Date de naissance', RenderWith: dateRender },
            { ModelName: 'Phone1', DisplayName: 'Téléphone 1', RenderWith: null },
            { ModelName: 'Phone2', DisplayName: 'Téléphone 2', RenderWith: null },
            { ModelName: 'Email', DisplayName: 'Email', RenderWith: null },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Read action
        $scope.Read = function (customer) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Customer/Customer.Read.html',
                controller: function ($scope, $modalInstance, Customer, Notification) {
                    $scope.Customer = Customer;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                    $scope.DownloadCustomerFile = function (filePath) {
                        if (filePath) {
                            APIProvider.get('Customer/DownloadCustomerFile', { filePath: filePath }, { responseType: 'blob' }).then(function (response) {
                                var blob = new Blob([response], { type: "application/octet-stream" });
                                $scope.UrlDownload = (window.URL || window.webkitURL).createObjectURL(blob);
                            }, function (rejection) {
                                Notification.error({
                                    message: 'Une erreur s\'est produite. Veuillez réessayer ou contacter le support technique.', title: 'Erreur'
                                });
                            }).finally(function () {

                            });
                        }
                    };
                    $scope.DownloadCustomerFile($scope.Customer.FilePath);
                },
                size: 'lg',
                resolve: {
                    Customer: function () {
                        return customer;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (customer) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Customer/Customer.Update.html',
                controller: function ($scope, $modalInstance, Customer, CustomerService, FileUploaderService) {
                    $scope.Customer = Customer;
                    $scope.Customer.InitialCIN = Customer.CIN;
                    $scope.Customer.BirthDay = $filter('date')($scope.Customer.BirthDay, 'yyyy-MM-dd');
                    //Inject FileUploaderService
                    $scope.uploader = FileUploaderService.Init('Customer/UploadCustomerFile');
                    $injector.get('AgencyService').GetAgencies($rootScope.user.Company_Id).then(function (agencies) {
                        $scope.Agencies = agencies;
                    }, function (rejection) {
                        Notification.error({ message: rejection, title: 'Erreur' });
                    });
                    // Check if customer exist
                    $scope.$watch('Customer.CIN', function (newValue, oldValue) {
                        if (newValue != undefined) {
                            CustomerService.GetCustomerByCin($rootScope.user.Company_Id, newValue).then(function (customer) {
                                $scope.CustomerExist = (customer) ? true : false
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            });
                        }
                    }, true);

                    // Download customer file if exist
                    $scope.DownloadCustomerFile = function (filePath) {
                        if (filePath) {
                            APIProvider.get('Customer/DownloadCustomerFile', { filePath: filePath }, { responseType: 'blob' }).then(function (response) {
                                var blob = new Blob([response], { type: "application/octet-stream" });
                                $scope.UrlDownload = (window.URL || window.webkitURL).createObjectURL(blob);
                            }, function (rejection) {
                                Notification.error({
                                    message: 'Une erreur s\'est produite. Veuillez réessayer ou contacter le support technique.', title: 'Erreur'
                                });
                            }).finally(function () {

                            });
                        }
                    };
                    $scope.DownloadCustomerFile($scope.Customer.FilePath);
                    $scope.UpdateCustomer = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            // Update File
                            if ($scope.uploader.queue.length != 0) {
                                $scope.uploader.onBeforeUploadItem = function (item) {
                                    item.formData.push({ Id: $scope.Customer.Id });
                                };
                                $scope.uploader.onSuccessItem = function (item, response, status, headers) {
                                    APIProvider.post('Customer/UpdateCustomer', $scope.Customer).then(function (response) {
                                        Notification.success({ message: 'Client modfié.', title: 'Ok !' });
                                        $modalInstance.close();
                                    }, function (rejection) {
                                        Notification.error({ message: rejection, title: 'Erreur' });
                                    }).finally(function () {
                                        $scope.Loading = false;
                                    });
                                }
                                $scope.uploader.uploadAll();
                            } else {
                                APIProvider.post('Customer/UpdateCustomer', $scope.Customer).then(function (response) {
                                    Notification.success({ message: 'Client modfié.', title: 'Ok !' });
                                    $modalInstance.close();
                                }, function (rejection) {
                                    Notification.error({ message: rejection, title: 'Erreur' });
                                }).finally(function () {
                                    $scope.Loading = false;
                                });
                            }

                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    Customer: function () {
                        return customer;
                    },
                    CustomerService: function () {
                        return CustomerService;
                    },
                    FileUploaderService: function () {
                        return FileUploaderService;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (customer) {
            $scope.Loading = true;
            APIProvider.post('Customer/DeleteCustomer', customer).then(function (response) {
                Notification.success({ message: 'Client supprimé.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name == 'Layout.CreateCustomer') {

        // Inject FileUploaderService
        $scope.uploader = FileUploaderService.Init('Customer/UploadCustomerFile');

        // Check if customer exist
        $scope.$watch('Customer.CIN', function (newValue, oldValue) {
            if (newValue != undefined) {
                CustomerService.GetCustomerByCin($rootScope.user.Company_Id, newValue).then(function (customer) {
                    $scope.CustomerExist = (customer) ? true : false
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                });
            }
        }, true);

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                $scope.Customer.Company_Id = $rootScope.user.Company_Id;
                $scope.Customer.Agency_Id = $rootScope.user.Agency_Id;
                APIProvider.post('Customer/CreateCustomer', $scope.Customer).then(function (response) {
                    $scope.uploader.onBeforeUploadItem = function (item) {
                        item.formData.push({ Id: response });
                    };
                    $scope.uploader.uploadAll();
                    Notification.success({ message: 'Client enregistré.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur !' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };
    }
    
});