﻿MoneyExchangeApp.controller('DashboardController', function ($rootScope, $scope, $http, $interval, $injector, Notification, CashBoxService, OperationService, TransactionService, ExpenseService, CustomerService) {

    $scope.DateTimeNow = new Date();

    $scope.LoadingCashBoxes = true;
    $scope.LoadingLastOperations = true;
    $scope.LoadingOperationsTotalAmount = true;

    $scope.LoadingTransactions = true;
    $scope.LoadingLastTransactions = true;

    $scope.LoadingExpenses = true;
    $scope.LoadingLastExpenses = true;

    $scope.LoadingBestCustomers = true;
    $scope.LoadingLastCustomers = true;


    // CashBox
    var GetCashBoxes = function () {
        CashBoxService.GetCashBoxes($rootScope.user.Company_Id).then(function (cashBoxes) {
            $scope.CashBoxes = cashBoxes;
            $scope.LoadingCashBoxes = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetCashBoxes();

    var GetLastOperations = function () {
        OperationService.GetLastOperations($rootScope.user.Company_Id).then(function (operations) {
            $scope.Operations = operations;
            $scope.LoadingLastOperations = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetLastOperations();

    var GetOperationsTotalAmount = function () {
        OperationService.GetOperationsTotalAmount($rootScope.user.Company_Id).then(function (operationsTotalAmount) {
            $scope.OperationsTotalAmount = operationsTotalAmount;
            $scope.ProvisionsTotalAmount = 0;
            $scope.WithdrawalsTotalAmount = 0;
            angular.forEach(operationsTotalAmount, function (operation) {
                $scope.ProvisionsTotalAmount += operation.MonthlyProvisions;
                $scope.WithdrawalsTotalAmount += operation.MonthlyWithdrawals;
            });
            $scope.LoadingOperationsTotalAmount = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetOperationsTotalAmount();

    // Transactions
    var GetDashboardTransactions = function () {
        TransactionService.GetDashboardTransactions($rootScope.user.Company_Id).then(function (dashboardTransactions) {
            $scope.DashboardTransactions = dashboardTransactions;
            $scope.TransactionsTotalNumber = 0;
            $scope.TransactionsTotalVolume = 0;
            angular.forEach(dashboardTransactions, function (transaction) {
                $scope.TransactionsTotalNumber += transaction.MonthlyTransactions;
                $scope.TransactionsTotalVolume += transaction.MonthlyVolume;
            });
            $scope.LoadingTransactions = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetDashboardTransactions();

    var GetLastTransactions = function () {
        TransactionService.GetLastTransactions($rootScope.user.Company_Id).then(function (lastTransactions) {
            $scope.LastTransactions = lastTransactions;
            $scope.LoadingLastTransactions = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetLastTransactions();

    // Expenses
    var GetDashboardExpenses = function () {
        ExpenseService.GetDashboardExpenses($rootScope.user.Company_Id).then(function (expenses) {
            $scope.DashboardExpenses = expenses;
            $scope.ExpensesTotalAmount = 0;
            angular.forEach(expenses, function (expense) {
                $scope.ExpensesTotalAmount += expense.MonthlyExpenses;
            });
            $scope.LoadingExpenses = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetDashboardExpenses();

    var GetLastExpenses = function () {
        ExpenseService.GetLastExpenses($rootScope.user.Company_Id).then(function (lastExpenses) {
            $scope.LastExpenses = lastExpenses;
            $scope.LoadingLastExpenses = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetLastExpenses();

    // Customers
    var GetBestCustomers = function () {
        CustomerService.GetBestCustomers($rootScope.user.Company_Id).then(function (customers) {
            $scope.BestCustomers = customers;
            $scope.LoadingBestCustomers = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetBestCustomers();

    var GetLastCustomers = function () {
        CustomerService.GetLastCustomers($rootScope.user.Company_Id).then(function (customers) {
            $scope.LastCustomers = customers;
            $scope.LoadingLastCustomers = false;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });
    };
    GetLastCustomers();

    // Refresher
    var DashbordRefresh = $interval(function () {
        GetCashBoxes();
        GetLastOperations();
        GetOperationsTotalAmount();
        GetDashboardTransactions();
        GetLastTransactions();
        GetDashboardExpenses();
        GetLastExpenses();
        GetBestCustomers();
        GetLastCustomers();
    }.bind(this), 10000);

    $scope.$on('$destroy', function () {
        $interval.cancel(DashbordRefresh)
    });

});