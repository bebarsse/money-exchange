﻿MoneyExchangeApp.controller('AgencyController', function ($rootScope, $scope, $http, $timeout, $parse, $compile, $modal, APIProvider, Notification, $state, $injector) {

    // Init agency model
    $scope.Agency = {};

    // Init Cities
    $scope.Cities = $injector.get('Helpers').GetCities();

    // State : List
    if ($state.current.name == 'Layout.ListAgency') {

        $scope.Cities.push({name: 'Tout le Maroc', value: '*' });

        // Init filters
        $scope.Filters = {};

        // Inject DatatableService
        var DatatableService = $injector.get('DatatableService');

        // Datatable Options Builder
        $scope.DtOptions = DatatableService.OptionsBuilder('/Agency/GetAgencies', [0, 1, 2, 3, 4], 'Liste des agences', ['Nom agence', 'Tél. Résidentiel', 'Téléphone cellulaire', 'Email']).withOption('createdRow', function createdRow(row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
            $compile(angular.element("#datatable-search"))($scope);
        });

        // Datatable Columns Builder
        var actionsRender = function actionsHtml(data, type, full, meta) {
            var obj = JSON.stringify(data).replace(new RegExp("'", 'g'), "\\'").replace(new RegExp("\"", 'g'), "'");
            return '<button class="btn btn-icon-only btn-circle btn-outline me-grey" ng-click="Read(' + obj + ')" popover="Visualiser" popover-trigger="mouseenter"><i class="fa fa-file-text-o"></i></button>&nbsp;' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-yellow" ng-click="Update(' + obj + ')" popover="Mofidier" popover-trigger="mouseenter"><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-icon-only btn-circle btn-outline me-red" mwl-confirm title="<i class=\'fa fa-exclamation-triangle font-red\'></i> <span class=\'font-red\'> &nbsp; Attention ! </span>" message="La suppression de cette agence entraînera la suppression de tous les utilissateurs affectés à cette dernière" confirm-text="<i class=\'fa fa-check\'> </i>Oui" confirm-button-type="btn btn-outline me-red" cancel-text="<i class=\'fa fa-times\'> </i>Non" cancel-button-type="btn btn-outline me-blue" on-confirm="Delete(' + obj + ');" placement="left" popover="Supprimer" popover-trigger="mouseenter"><i class="fa fa-trash"></i></button>';
        };
        $scope.DtColumns = DatatableService.ColumnsBuilder([
            { ModelName: 'Name', DisplayName: 'Agence', RenderWith: null },
            { ModelName: 'City', DisplayName: 'Ville', RenderWith: null },
            { ModelName: 'Phone', DisplayName: 'Tél. Résidentiel', RenderWith: null },
            { ModelName: 'Cell', DisplayName: 'Tél. cellulaire', RenderWith: null },
            { ModelName: 'Email', DisplayName: 'Email', RenderWith: null },
            { ModelName: null, DisplayName: 'Actions', RenderWith: actionsRender }]);

        // Datatable Instance
        $scope.DtInstance = {};

        // Reload Datatable action
        $scope.Reload = function (filters) {
            if ($scope.DtInstance.dataTable) {
                $scope.DtInstance.dataTable.fnSettings().aoServerParams.push({
                    "sName": "advancedFilter",
                    "fn": function (aoData) {
                        angular.forEach(filters, function (value, key) {
                            var obj = {};
                            obj[key] = value;
                            angular.extend(aoData, obj);
                        });
                    }
                });
                $scope.DtInstance.dataTable.fnUpdate();
            }
        };

        $scope.Reload($scope.Filters);

        // Read action
        $scope.Read = function (agency) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Agency/Agency.Read.html',
                controller: function ($scope, $modalInstance, Agency) {
                    $scope.Agency = Agency;
                    $scope.Close = function () {
                        $modalInstance.close();
                    };
                },
                size: 'lg',
                resolve: {
                    Agency: function () {
                        return agency;
                    }
                }
            });
        };

        // Update action
        $scope.Update = function (agency) {
            var modalInstance = $modal.open({
                templateUrl: '/App/Views/Agency/Agency.Update.html',
                controller: function ($scope, $modalInstance, Agency, Cities) {
                    $scope.Agency = Agency;
                    $scope.Cities = Cities;
                    $scope.UpdateAgency = function (formModel) {
                        if (formModel.$valid) {
                            $scope.Loading = true;
                            APIProvider.post('Agency/UpdateAgency', $scope.Agency).then(function (response) {
                                Notification.success({ message: 'Agence modfiée.', title: 'Ok !' });
                                $modalInstance.close();
                            }, function (rejection) {
                                Notification.error({ message: rejection, title: 'Erreur' });
                            }).finally(function () {
                                $scope.Loading = false;
                            });
                        }
                    };
                    $scope.Cancel = function () {
                        $modalInstance.dismiss('cancel');
                    }
                },
                size: 'lg',
                resolve: {
                    Agency: function () {
                        return agency;
                    },
                    Cities: function () {
                        $scope.CitiesClone = angular.copy($scope.Cities);
                        $scope.CitiesClone.splice(-1, 1);
                        return $scope.CitiesClone;
                    },
                }
            });

            modalInstance.result.then(function () {
                $scope.DtInstance.DataTable.draw(false);
            }, function () {
            });
        }

        // Delete action
        $scope.Delete = function (agency) {
            $scope.Loading = true;
            APIProvider.post('Agency/DeleteAgency', agency).then(function (response) {
                Notification.success({ message: 'Agence supprimée.', title: 'Ok !' });
                $scope.DtInstance.DataTable.draw(false);
            }, function (rejection) {
                Notification.error({ message: rejection, title: 'Erreur' });
            }).finally(function () {
                $scope.Loading = false;
            });
        };
    }

    // State : Create
    if ($state.current.name == 'Layout.CreateAgency') {

        // Inject AgencyService
        var AgencyService = $injector.get('AgencyService');

        // Check if create agency is allowed
        AgencyService.CreateAgencyAllowed($rootScope.user.Company_Id).then(function (response) {
            $scope.CreateAgencyAllowed = response;
        }, function (rejection) {
            Notification.error({ message: rejection, title: 'Erreur' });
        });

        // Create action
        $scope.Create = function (formModel) {
            if (formModel.$valid) {
                $scope.Loading = true;
                $scope.Agency.ConnectedUserId = $rootScope.user.Id;
                APIProvider.post('Agency/CreateAgency', $scope.Agency).then(function (response) {
                    Notification.success({ message: 'Agence enregistrée.', title: 'Ok !' });
                    $state.reload();
                }, function (rejection) {
                    Notification.error({ message: rejection, title: 'Erreur' });
                }).finally(function () {
                    $scope.Loading = false;
                });
            }
        };
    }
    
});