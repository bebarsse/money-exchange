﻿MoneyExchangeApp.factory('Helpers', function (APIProvider, Notification, $q) {
    return {
        GetCities: function () {
            return [{ name: 'Casablanca', value: 'Casablanca' },
            { name: 'Agadir', value: 'Agadir' },
            { name: 'Al Hoceïma', value: 'Al Hoceïma' },
            { name: 'Béni Mellal', value: 'Béni Mellal' },
            { name: 'El Jadida', value: 'El Jadida' },
            { name: 'Errachidia', value: 'Errachidia' },
            { name: 'Fès', value: 'Fès' },
            { name: 'Kénitra', value: 'Kénitra' },
            { name: 'Khénifra', value: 'Khénifra' },
            { name: 'Khouribga', value: 'Khouribga' },
            { name: 'Marrakech', value: 'Marrakech' },
            { name: 'Meknès', value: 'Meknès' },
            { name: 'Nador', value: 'Nador' },
            { name: 'Ouarzazate', value: 'Ouarzazate' },
            { name: 'Oujda', value: 'Oujda' },
            { name: 'Rabat', value: 'Rabat' },
            { name: 'Safi', value: 'Safi' },
            { name: 'Settat', value: 'Settat' },
            { name: 'Salé', value: 'Salé' },
            { name: 'Tanger', value: 'Tanger' },
            { name: 'Taza', value: 'Taza' },
            { name: 'Tétouan', value: 'Tétouan' }];
        }
    }
});