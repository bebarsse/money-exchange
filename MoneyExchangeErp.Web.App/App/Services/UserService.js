﻿MoneyExchangeApp.factory('UserService', function (APIProvider, Notification, $q) {
    return {
        GetUsers: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('User/GetUsersByCompany', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});