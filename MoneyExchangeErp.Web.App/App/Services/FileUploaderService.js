﻿MoneyExchangeApp.factory('FileUploaderService', function ($rootScope, APIProvider, FileUploader, Notification, Constants) {
    return {
        Init: function (url) {
            FileUploader.FileSelect.prototype.isEmptyAfterSelection = function () {
                return true;
            };

            var uploader = new FileUploader({
                url: Constants.C_APIURL + url,
                queueLimit: 1,
                onErrorItem: function (item, response, status, headers) {
                    Notification.error({ message: response, title: 'Erreur !' });
                }
            });

            uploader.filters.push({
                name: 'ExtensionFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|pdf|'.indexOf(type) !== -1;
                }
            });

            uploader.filters.push({
                'name': 'FileSizeFilter',
                'fn': function (item) {
                    return item.size <= 1048576;
                }
            });

            uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                if (angular.equals(filter.name, 'ExtensionFilter'))
                    Notification.error({ message: 'Le transfert du fichier devrait être dans l\'un des formats suivants: JPG, PNG ou PDF.', title: 'Extension non supportée' });
                if (angular.equals(filter.name, 'FileSizeFilter'))
                    Notification.error({ message: 'La taille du fichier ne doit pas dépasser 1MB.', title: 'Fichier volumineux' });
            };

            return uploader;
        },
    }
});