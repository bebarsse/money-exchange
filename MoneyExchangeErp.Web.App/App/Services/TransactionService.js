﻿MoneyExchangeApp.factory('TransactionService', function (APIProvider, Notification, $q) {
    return {
        GetDashboardTransactions: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetDashboardTransactions', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetLastTransactions: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetLastTransactions', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});