﻿MoneyExchangeApp.factory('OperationService', function (APIProvider, Notification, $q) {
    return {
        GetLastOperations: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetLastOperations', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetOperationsTotalAmount: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetOperationsTotalAmount', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});