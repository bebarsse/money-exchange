﻿MoneyExchangeApp.factory('DatatableService', function ($rootScope, APIProvider, DTOptionsBuilder, DTColumnBuilder, $compile) {
    return {
        OptionsBuilder: function (apiEndpoint, columnsToExport, fileExportedTitle, searchAttributesTooltip) {
            var serachTooltip = '';
            angular.forEach(searchAttributesTooltip, function (value, key) {
                serachTooltip = serachTooltip.concat(value).concat('<br/>');
            });
            var dtOptions = DTOptionsBuilder.newOptions()
                .withOption('fnServerData', function (url, params, fnCallback) {
                    var datatableParams = {
                        companyId: $rootScope.user.Company_Id,
                        userId: $rootScope.user.Id,
                        draw: params[0].value,
                        columns: params[1].value,
                        order: params[2].value,
                        start: params[3].value,
                        length: params[4].value,
                        search: params[5].value,
                        filters:[]
                    };
                    //Push Filters
                    for (var i = 6; i < params.length; i++) {
                        datatableParams.filters.push(params[i]);
                    }
                    
                    APIProvider.post(apiEndpoint, datatableParams)
                    .then(function (data) {
                        fnCallback(data);
                        $rootScope.$broadcast('Test', [1, 2, 3]);
                    });
                })
                .withDataProp('data')
                .withOption('serverSide', true)
                .withOption('processing', true)
                .withOption('searching', true)
                //.withOption('stateSave', false)
                .withOption('lengthMenu', [[5, 15, 20, -1],[5, 15, 20, "Tous"]])
                .withOption('initComplete', function () {
                    angular.element('.dataTables_filter input').attr('placeholder', 'Recherche');
                })
                .withBootstrap()
                .withOption('responsive', true)
                .withLanguage({
                    "sEmptyTable": "Aucune donnée n'est disponible",
                    "sInfo": "_TOTAL_ Lignes",
                    "sInfoEmpty": "0 Lignes",
                    "sInfoFiltered": "(Filtré de  _MAX_  entrées)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "Afficher _MENU_ par page",
                    "sLoadingRecords": "Chargement...",
                    "sProcessing": "<div  class='page-spinner-bar' style='position:unset; width: auto'><div class='bounce1' ></div><div class='bounce2'></div><div class='bounce3'></div></div >",
                    "sSearch": '<span id="datatable-search" class="fa fa-search font-green" tooltip-html-unsafe="Recherche par : <br/>' + serachTooltip + '"   tooltip-trigger="mouseenter" tooltip-placement="right"></span>',
                    "sZeroRecords": "Aucune donnée n'est disponible",
                    "oPaginate": {
                        "sFirst": "<<",
                        "sLast": ">>",
                        "sNext": ">",
                        "sPrevious": "<"
                    },
                    "oAria": {
                        "sSortAscending": ": activate to sort column ascending",
                        "sSortDescending": ": activate to sort column descending"
                    }
                })
                .withButtons([
                    {
                        extend: 'collection',
                        text: '<i class="fa fa-download"></i> &nbsp; Exporter',
                        buttons: [
                            {
                                extend: 'copy',
                                exportOptions: {
                                    columns: columnsToExport,
                                },
                                text: '<i class="fa fa-clipboard"></i> &nbsp; Copier'
                            },
                            {
                                extend: 'print',
                                exportOptions: {
                                    columns: columnsToExport,
                                },
                                text: '<i class="fa fa-print"></i> &nbsp; Imprimer',
                                title: fileExportedTitle
                            },
                            {
                                extend: 'pdf',
                                exportOptions: {
                                    columns: columnsToExport,
                                },
                                text: '<i class="fa fa-file-pdf-o"></i> &nbsp; Pdf',
                                title: fileExportedTitle,
                            },
                            {
                                extend: 'excel',
                                exportOptions: {
                                    columns: columnsToExport,
                                },
                                text: '<i class="fa fa-file-excel-o"></i> &nbsp; Excel',
                                title: fileExportedTitle
                            }
                        ]
                    }

                ]);

            return dtOptions;
        },
        ColumnsBuilder: function (columnsObj) {
            var dtColumns = [];
            angular.forEach(columnsObj, function (value, key) {
                if (value.RenderWith !== null) {
                    if (value.DisplayName == 'Actions') {
                        dtColumns.push(DTColumnBuilder.newColumn(value.ModelName).withTitle(value.DisplayName).withOption('width', '16%').withClass('text-center').notSortable()
                            .renderWith(value.RenderWith));
                    } else {
                        dtColumns.push(DTColumnBuilder.newColumn(value.ModelName).withTitle(value.DisplayName).withClass('text-center').notSortable()
                            .renderWith(value.RenderWith));
                    }
                }
                else
                    dtColumns.push(DTColumnBuilder.newColumn(value.ModelName).withTitle(value.DisplayName).withClass('text-center').notSortable());
            });
            return dtColumns;
        }
    }
});