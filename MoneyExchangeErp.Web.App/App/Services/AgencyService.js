﻿MoneyExchangeApp.factory('AgencyService', function (APIProvider, Notification, $q) {
    return {
        GetAgencies: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Agency/GetAgenciesByCompany', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        CreateAgencyAllowed: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Agency/CreateAgencyAllowed', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});