﻿MoneyExchangeApp.factory('ExpenseService', function (APIProvider, Notification, $q) {
    return {
        GetDashboardExpenses: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetDashboardExpenses', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetLastExpenses: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetLastExpenses', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});