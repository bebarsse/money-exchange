﻿MoneyExchangeApp.factory('CategoryService', function (APIProvider, Notification, $q) {
    return {
        GetCategories: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Category/GetCategoriesByCompany', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});