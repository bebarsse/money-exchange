﻿MoneyExchangeApp.factory('TaskService', function (APIProvider, $q) {
    return {
        GetUserTasks: function (userId) {
            var deferred = $q.defer();
            APIProvider.get('Task/GetUserTasks', { userId: userId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});