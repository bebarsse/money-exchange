﻿MoneyExchangeApp.factory('CashBoxService', function (APIProvider, Notification, $q) {
    return {
        GetCashBoxes: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetCashBoxes', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});