﻿MoneyExchangeApp.factory('OperatorService', function (APIProvider, Notification, $q) {
    return {
        GetOperators: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Operator/GetOperators', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetOperatorById: function (operatorId) {
            var deferred = $q.defer();
            APIProvider.get('Operator/GetOperatorById', { operatorId : operatorId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});