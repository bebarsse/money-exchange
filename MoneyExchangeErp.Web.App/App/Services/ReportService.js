﻿MoneyExchangeApp.factory('ReportService', function (APIProvider, Notification, $q) {
    return {
        GetDailyReport: function (companyId, day) {
            var deferred = $q.defer();
            APIProvider.get('Report/GetDailyReport', { companyId: companyId, day: day }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetMonthlyReport: function (companyId, month, year) {
            var deferred = $q.defer();
            APIProvider.get('Report/GetMonthlyReport', { companyId: companyId, month: month, year:year }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetExpenseReport: function (companyId, year) {
            var deferred = $q.defer();
            APIProvider.get('Report/GetExpenseReport', { companyId: companyId, year: year }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetMonthlyExpenseChart: function (data) {
            return AmCharts.makeChart('monthly-expense-chart', {
                "type": "serial",
                "theme": "light",
                "dataProvider": data,
                "valueAxes": [{
                    "id": "volumeAxis",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "position": "left",
                    "title": 'Volume en DH'
                }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "[[category]]: <b>[[value]] DH</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "Volume"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": 'Month',
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20
                },
                "export": {
                    "enabled": true
                }

            });
        },
        GetCategoryExpenseChart: function (data) {
            return AmCharts.makeChart("category-expense-chart", {
                "type": "pie",
                "theme": "light",

                "fontFamily": 'Open Sans',

                "color": '#888',

                "dataProvider": data,
                "valueField": "Volume",
                "titleField": "Category",
                "outlineAlpha": 0.4,
                "depth3D": 15,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "angle": 30,
                "exportConfig": {
                    menuItems: [{
                        icon: '/lib/3/images/export.png',
                        format: 'png'
                    }]
                }
            });

        },
        GetProfitReport: function (companyId, year) {
            var deferred = $q.defer();
            APIProvider.get('Report/GetProfitReport', { companyId: companyId, year: year }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetMonthlyProfitChart: function (data) {
            return AmCharts.makeChart("monthly-profit-chart", {
                "type": "serial",
                "theme": "light",
                "categoryField": "Month",
                "rotate": true,
                "startDuration": 1,
                "categoryAxis": {
                    "gridPosition": "start",
                    "position": "left"
                },
                "trendLines": [],
                "graphs": [
                    {
                        "balloonText": "Commission:[[value]] DH",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-1",
                        "lineAlpha": 0.2,
                        "title": "Commission",
                        "type": "column",
                        "valueField": "Profit"
                    },
                    {
                        "balloonText": "Dépense:[[value]] DH",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-2",
                        "lineAlpha": 0.2,
                        "title": "Dépense",
                        "type": "column",
                        "valueField": "Expense"
                    }
                ],
                "guides": [],
                "valueAxes": [
                    {
                        "id": "ValueAxis-1",
                        "position": "top",
                        "axisAlpha": 0
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "titles": [],
                "dataProvider":data,
            });
        },
        GetOperatorProfitChart: function (data) {
            return AmCharts.makeChart("operator-profit-chart", {
                "type": "pie",
                "theme": "light",

                "fontFamily": 'Open Sans',

                "color": '#888',

                "dataProvider": data,
                "valueField": "Volume",
                "titleField": "Operator",
                "outlineAlpha": 0.4,
                "depth3D": 15,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "angle": 30,
            });

        },
    }
});