﻿MoneyExchangeApp.factory('CustomerService', function (APIProvider, Notification, $q) {
    return {
        GetCustomerByCin: function (companyId, cin) {
            var deferred = $q.defer();
            APIProvider.get('Customer/GetCustomerByCin', {companyId : companyId, cin : cin }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }, 
        GetCustomerById: function (customerId) {
            var deferred = $q.defer();
            APIProvider.get('Customer/GetCustomerById', { customerId : customerId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetBestCustomers: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetBestCustomers', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        },
        GetLastCustomers: function (companyId) {
            var deferred = $q.defer();
            APIProvider.get('Dashboard/GetLastCustomers', { companyId: companyId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});