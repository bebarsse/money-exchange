﻿MoneyExchangeApp.factory('AttendanceService', function (APIProvider, $q) {
    return {
        GetLastAttendance: function (userId) {
            var deferred = $q.defer();
            APIProvider.get('Attendance/GetLastAttendance', { userId: userId }).then(function (response) {
                deferred.resolve(response);
            }, function (rejection) {
                deferred.reject(rejection);
            }).finally(function () {
            });
            return deferred.promise;
        }
    }
});