﻿MoneyExchangeApp.factory('AuthenticationHttpInterceptor', function ($injector, $q, $location) {
    return {
        request: function (config) {
            var authService = $injector.get('AuthenticationProvider');
            var connectedUser = authService.getConnectedUser();
            if (connectedUser != null) {
                config.headers.Authorization = 'Bearer ' + connectedUser.access_token;
            }

            return config || $q.when(config);
        },

        responseError: function (rejection) {
            var deferred = $q.defer();
            if (rejection.status == 401) {
                $injector.get('AuthenticationProvider').refreshSession().then(function () {
                    $injector.get('$http')(rejection.config).then(function (response) {
                        deferred.resolve(response);
                    }, function (err) {
                        deferred.reject(err);
                        });
                }, function (err) {
                    $injector.get('AuthenticationProvider').logout();
                    $location.path('/connexion')
                    deferred.reject(err);
                });

                return deferred.promise;
            }
            else return $q.reject(rejection);
        }
    }
});

MoneyExchangeApp.factory('AuthenticationProvider', function ($rootScope, $http, $q, localStorageService, Constants) {
    var service = {
        connectedUser: null,
        refreshingSession: false,
        refreshPromise: null,

        setConnectedUser: function (user) {
            service.connectedUser = user;
            localStorageService.remove('connectedUser');
            if (user != null && user.remember) localStorageService.add('connectedUser', user)
        },

        getConnectedUser: function () {
            if (service.connectedUser == null) {
                service.connectedUser = localStorageService.get('connectedUser');
            }
            return service.connectedUser;
        },

        refreshSession: function () {
            if (!service.refreshingSession) {
                service.refreshingSession = true;
                service.refreshPromise = $q.defer();
                if (service.connectedUser) {
                    $http({
                        url: Constants.C_APIURL + '/token',
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        data: $.param({
                            "grant_type": 'refresh_token',
                            "refresh_token": service.connectedUser.refresh_token
                        })
                    })
                    .then(function (data) {
                        service.setConnectedUser(angular.extend(data.data, { remember: service.connectedUser.remember }));
                        service.refreshPromise.resolve(true);
                    }, function () {
                        service.refreshPromise.reject(false);
                    })
                    .finally(function () {
                        service.refreshingSession = false;
                    })
                }
            }
            return service.refreshPromise.promise;
        },

        login: function (username, password, remember) {
            var deferred = $q.defer();

            $http({
                url: Constants.C_APIURL + '/token',
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param({
                    "grant_type": "password",
                    "username": username,
                    "password": password
                })
            }).then(function (data) {
                data = data.data;
                data.remember = remember;

                service.setConnectedUser(data);

                deferred.resolve(data);
            }, function (rejection) {
                rejection = rejection.data;
                deferred.reject(rejection);
            })

            return deferred.promise;
        },

        logout: function () {
            service.setConnectedUser(null);
            localStorageService.clearAll();
        }
    };

    return service;
});