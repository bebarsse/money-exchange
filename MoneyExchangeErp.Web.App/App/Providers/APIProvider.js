﻿MoneyExchangeApp.factory('APIProvider', function ($http, Constants, $q) {

    var regexIso8601 = /^(?:[1-9]\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|[+-][01]\d:[0-5]\d)$/;

    function convertDateStringsToDates(input) {
        // Ignore things that aren't objects.
        if (typeof input !== "object") return input;

        for (var key in input) {
            if (!input.hasOwnProperty(key)) continue;

            var value = input[key];
            var match;
            // Check for string properties which look like dates.
            // TODO: Improve this regex to better match ISO 8601 date strings.
            if (typeof value === "string" && (match = value.match(regexIso8601))) {
                // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.
                var milliseconds = Date.parse(match[0]);
                if (!isNaN(milliseconds)) {
                    input[key] = new Date(milliseconds);
                }
            } else if (typeof value === "object") {
                // Recurse into object
                convertDateStringsToDates(value);
            }
        }
    }

    return {
        get: function (url, param, config) {
            var deferred = $q.defer();
            if (param) url += '?' + $.param(param);
            $http.get(Constants.C_APIURL + url, config).then(function (response) {
                data = response.data;
                if (Array.isArray(data)) {
                    for (var emt in data) convertDateStringsToDates(emt);
                }
                else convertDateStringsToDates(data);
                deferred.resolve(data);
            }, function (rejection) {
                deferred.reject(rejection.data);
            })

            return deferred.promise;
        },
        post: function (url, model) {
            var deferred = $q.defer();
            $http({
                url: Constants.C_APIURL + url,
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param(model)
            }).then(function (response) {
                data = response.data;
                if (Array.isArray(data)) {
                    for (var emt in data) convertDateStringsToDates(emt);
                }
                else convertDateStringsToDates(data);
                deferred.resolve(data);
            }, function (rejection) {
                rejection = rejection.data;
                deferred.reject(rejection);
            })
            return deferred.promise;
        }
    };
});

