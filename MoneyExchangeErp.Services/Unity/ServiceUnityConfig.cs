﻿using Microsoft.Practices.Unity;
using MoneyExchangeErp.Core.Helpers;
using MoneyExchangeErp.Data.Unity;

namespace MoneyExchangeErp.Services.Unity
{
    public class ServiceUnityConfig
    {
        private static IUnityContainer _container;

        public static void RegisterTypes(IUnityContainer container)
        {
            DataUnityConfig.RegisterTypes(container);

            container.RegisterType<IAppExceptionService, AppExceptionService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IDashboardService, DashbordService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IUserService, UserService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<ICompanyService, CompanyService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IAgencyService, AgencyService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IOperatorService, OperatorService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<ICustomerService, CustomerService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<ITransactionService, TransactionService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IOperationService, OperationService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IExpenseService, ExpenseService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<ICategoryService, CategoryService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<ICashBoxService, CashBoxService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IReportService, ReportService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<ITaskService, TaskService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<IAttendanceService, AttendanceService>(new UnityPerRequestLifetimeManager());
            container.RegisterType<INotificationService, NotificationService>(new UnityPerRequestLifetimeManager());

            _container = container;
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
