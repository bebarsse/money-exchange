﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Core.Helpers;
using MoneyExchangeErp.Data;
using MoneyExchangeErp.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static MoneyExchangeErp.Core.Exceptions.BusinessException;

namespace MoneyExchangeErp.Services
{
    public interface ICompanyService
    {
        IRepository<CompanyModel, Company> Repository { get; }
        void CreateCompany(CompanyModel companyModel);
        void UpdateCompany(CompanyModel companyModel);
        void DeleteCompany(CompanyModel companyModel);
        IEnumerable<CompanyModel> GetCompanies(DatatableInModel inModel, out int total, out int totalFiltered);

    }

    public class CompanyService : ICompanyService
    {
        public IRepository<CompanyModel, Company> Repository { get; }
        protected MoneyExchangeEntities _db;
        protected IUserService _userService;


        public CompanyService(MoneyExchangeEntities db, IRepository<CompanyModel, Company> repository, IUserService userService)
        {
            _db = db;
            Repository = repository;
            _userService = userService;
        }

        public void CreateCompany(CompanyModel companyModel)
        {
            if (_userService.UsernameExist(companyModel.Administrator.Username))
                throw new BusinessException(BusinessExceptionType.UsernameExist);

            //Company
            var company = Mapper.Map<Company>(companyModel);
            company.IsDeleted = false;
            company.CreatedAt = DateTime.Now;

            //Clone Company on Agency
            var agency = new Agency()
            {
                Name = company.CompanyName,
                City = company.City,
                Address = company.Address,
                Phone = company.Phone,
                Cell = company.Cell,
                Email = company.Email,
                IsMain = true,
                CreatedAt = DateTime.Now,
                IsDeleted = false,
            };

            //Users
            var user = Mapper.Map<User>(companyModel.Administrator);
            user.Roles = "Admin|User";
            user.IsBlocked = false;
            user.IsMain = true;
            user.CreatedAt = DateTime.Now;
            user.IsDeleted = false;

            //Operators
            foreach (var operatorItem in Mapper.Map<IEnumerable<Operator>>(Constants._DefalutOperatorsCashPlus()))
            {
                company.Operators.Add(operatorItem);
            }

            agency.Users.Add(user);
            company.Agencies.Add(agency);
            _db.Companies.Add(company);

            _db.SaveChanges();
        }

        public void UpdateCompany(CompanyModel companyModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == companyModel.Id);
            var admin = company.Agencies.SingleOrDefault(a => a.IsMain == true).Users.SingleOrDefault(u => u.Roles.Split('|').Contains("Admin"));
            // Check if username already exist
            if (admin.Username != companyModel.Administrator.Username)
            {
                if (_userService.UsernameExist(companyModel.Administrator.Username))
                    throw new BusinessException(BusinessExceptionType.UsernameExist);
            }
            admin = Mapper.Map(companyModel.Administrator, admin);

            company = Mapper.Map(companyModel, company);

            _db.SaveChanges();

        }

        public void DeleteCompany(CompanyModel companyModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == companyModel.Id);

            // Delete Agencies
            foreach (var agency in company.Agencies)
            {
                foreach (var user in agency.Users)
                {
                    user.IsDeleted = true;
                    user.DeletedAt = DateTime.Now;
                    foreach (var task in user.Tasks)
                    {
                        task.IsDeleted = true;
                        task.DeletedAt = DateTime.Now;
                    }
                    foreach (var attendance in user.Attendances)
                    {
                        attendance.IsDeleted = true;
                        attendance.DeletedAt = DateTime.Now;
                    }
                }

                agency.IsDeleted = true;
                agency.DeletedAt = DateTime.Now;
            }

            // Delete Operators
            foreach (var operatorItem in company.Operators)
            {
                operatorItem.IsDeleted = true;
                operatorItem.DeletedAt = DateTime.Now;
            }

            // Delete Customers
            foreach (var customer in company.Customers)
            {
                customer.IsDeleted = true;
                customer.DeletedAt = DateTime.Now;
            }

            company.IsDeleted = true;
            company.DeletedAt = DateTime.Now;

            _db.SaveChanges();
        }

        public IEnumerable<CompanyModel> GetCompanies(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            IEnumerable<Company> companiesToShow = _db.Companies.Where(c => c.IsDeleted == false);
            total = companiesToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                companiesToShow = companiesToShow.Where(c => c.CompanyName.ToLower().Contains(inModel.search.value) || c.VirtualAdministratorName.ToLower().Contains(inModel.search.value) || c.VirtualReverseAdministratorName.ToLower().Contains(inModel.search.value) || c.Phone.Contains(inModel.search.value) || c.Cell.Contains(inModel.search.value) || c.Email.ToLower().Contains(inModel.search.value));
            }
            
            //Filters
            if (inModel.filters != null)
            {
                foreach (var filter in inModel.filters)
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("City"))
                    {
                        companiesToShow = companiesToShow.Where(c => c.City == filter.value);
                        continue;
                    }
                    if (filter.name.Equals("ParentCompany"))
                    {
                        companiesToShow = companiesToShow.Where(c => c.ParentCompany == filter.value);
                        continue;
                    }
                }
            }

            totalFiltered = companiesToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<CompanyModel>>(companiesToShow.OrderByDescending(c => c.Id)) : Mapper.Map<IEnumerable<CompanyModel>>(companiesToShow.OrderByDescending(c => c.Id).Skip(inModel.start).Take(inModel.length));
        }
        
    }
}
