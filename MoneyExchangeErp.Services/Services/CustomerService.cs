﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Data;
using MoneyExchangeErp.Data.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static MoneyExchangeErp.Core.Exceptions.BusinessException;

namespace MoneyExchangeErp.Services
{
    public interface ICustomerService
    {
        int CreateCustomer(CustomerModel customerModel);
        void UpdateCustomer(CustomerModel customerModel);
        void DeleteCustomer(CustomerModel customerModel);
        IEnumerable<CustomerModel> GetCustomers(DatatableInModel inModel, out int total, out int totalFiltered);
        CustomerModel GetCustomerById(int customerId);
        CustomerModel GetCustomerByCin(int companyId, string cin);
        void SetCustomerFilePath(int customerId, string filePath);
        IEnumerable<CustomerModel> GetBestCustomers(int companyId);
        IEnumerable<CustomerModel> GetLastCustomers(int companyId);
        IRepository<CustomerModel, Customer> Repository { get; }
    }

    public class CustomerService : ICustomerService
    {
        protected MoneyExchangeEntities _db;
        public IRepository<CustomerModel, Customer> Repository { get; }

        public CustomerService(MoneyExchangeEntities db, IRepository<CustomerModel, Customer> repository)
        {
            _db = db;
            Repository = repository;
        }

        public int CreateCustomer(CustomerModel customerModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == customerModel.Company_Id);
            // Check if customer already exist
            if (company.Customers.Count(c => c.CIN == customerModel.CIN) != 0)
                throw new BusinessException(BusinessExceptionType.CustomerExist);

            var agency = _db.Agencies.SingleOrDefault(a => a.Id == customerModel.Agency_Id);
            var customer = Mapper.Map<Customer>(customerModel);
            customer.CreatedAt = DateTime.Now;
            customer.IsDeleted = false;

            company.Customers.Add(customer);
            agency.Customers.Add(customer);

            _db.SaveChanges();

            return customer.Id;
        }

        public void UpdateCustomer(CustomerModel customerModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == customerModel.Company_Id);
            var customer = _db.Customers.SingleOrDefault(c => c.Id == customerModel.Id);

            // Check if customer already exist
            if (customer.CIN != customerModel.CIN)
            {
                if (company.Customers.Count(c => c.CIN == customerModel.CIN) != 0)
                    throw new BusinessException(BusinessExceptionType.CustomerExist);
            }

            if (customerModel.Agency_Id  == customer.AgencyId || customerModel.Agency_Id == 0)
            {
                customer = Mapper.Map(customerModel, customer);
            }
            else
            {
                if(customer.Agency != null)
                {
                    customer.Agency.Customers.Remove(customer);
                }
                customer = Mapper.Map(customerModel, customer);
                _db.Agencies.SingleOrDefault(a => a.Id == customerModel.Agency_Id).Customers.Add(customer);
            }
            _db.SaveChanges();
        }

        public void DeleteCustomer(CustomerModel customerModel)
        {
            var customer = _db.Customers.SingleOrDefault(c => c.Id == customerModel.Id);
            customer.IsDeleted = true;
            customer.DeletedAt = DateTime.Now;
            _db.SaveChanges();
        }

        public IEnumerable<CustomerModel> GetCustomers(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            User user = _db.Users.SingleOrDefault(u => u.Id == inModel.userId);
            IEnumerable<Customer> customersToShow = company.Customers.Where(o => o.IsDeleted == false);

            total = customersToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                customersToShow = customersToShow.Where(c =>  c.CIN.ToLower().Contains(inModel.search.value) ||  c.VirtualCustomerName.ToLower().Contains(inModel.search.value) || c.VirtualReverseCustomerName.ToLower().Contains(inModel.search.value) || ( c.Phone1 != null && c.Phone1.Contains(inModel.search.value))  || ( c.Phone2 != null && c.Phone2.Contains(inModel.search.value)) || (c.Email != null && c.Email.ToLower().Contains(inModel.search.value)));
            }

            //Filters
            if (inModel.filters != null)
            {
                foreach (var filter in inModel.filters)
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("BirthDay"))
                    {
                        customersToShow = customersToShow.Where(o => o.BirthDay == DateTime.Parse(filter.value.Split(' ')[0]));
                        continue;
                    }
                    if (filter.name.Equals("Agency"))
                    {
                        customersToShow = customersToShow.Where(c => c.AgencyId == int.Parse(filter.value));
                        continue;
                    }
                }
            }
            else
            {
                if (!user.Roles.Split('|').Contains("Admin"))
                    customersToShow = customersToShow.Where(c => c.AgencyId == user.AgencyId);
            }

            totalFiltered = customersToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<CustomerModel>>(customersToShow.OrderByDescending(c => c.Id)) : Mapper.Map<IEnumerable<CustomerModel>>(customersToShow.OrderByDescending(c => c.Id).Skip(inModel.start).Take(inModel.length));
        }

        public CustomerModel GetCustomerById(int customerId)
        {
            var customer = _db.Customers.SingleOrDefault(c => c.Id == customerId && c.IsDeleted == false);
            return Mapper.Map<CustomerModel>(customer);
        }

        public CustomerModel GetCustomerByCin(int companyId, string cin)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == companyId && c.IsDeleted == false);
            return Mapper.Map<CustomerModel>(company.Customers.SingleOrDefault(c =>  c.IsDeleted == false && c.CIN.Trim().ToLower() == cin.Trim().ToLower()));
        }

        public void SetCustomerFilePath(int customerId, string filePath)
        {
            var customer = _db.Customers.SingleOrDefault(c => c.Id == customerId && c.IsDeleted == false);
            if(customer.FilePath != null)
            {
                if (!File.Exists(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\App_Data\\Customers\\", customer.FilePath)))
                {
                    throw new FileNotFoundException();
                }
                File.Delete(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\App_Data\\Customers\\", customer.FilePath));
            }
            customer.FilePath = filePath;
            _db.SaveChanges();
        }

        public IEnumerable<CustomerModel> GetBestCustomers(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<CustomerModel>>(company.Customers.Where(c => c.IsDeleted == false).OrderByDescending(c => c.NumberOfTransactions).Take(10));
        }

        public IEnumerable<CustomerModel> GetLastCustomers(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<CustomerModel>>(company.Customers.OrderByDescending(c => c.CreatedAt).Take(20));

        }
        
    }
}
