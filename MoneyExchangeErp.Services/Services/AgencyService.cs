﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Data;
using MoneyExchangeErp.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using static MoneyExchangeErp.Core.Exceptions.BusinessException;

namespace MoneyExchangeErp.Services
{
    public interface IAgencyService
    {
        IRepository<AgencyModel, Agency> Repository { get; }
        void CreateAgency(AgencyModel agencyModel);
        void UpdateAgency(AgencyModel agencyModel);
        void DeleteAgency(AgencyModel agencyModel);
        IEnumerable<AgencyModel> GetAgencies(DatatableInModel inModel, out int total, out int totalFiltered);
        IEnumerable<AgencyModel> GetAgencies(int companyId);
        bool CreateAgencyAllowed(int companyId);
    }

    public class AgencyService : IAgencyService
    {
        public IRepository<AgencyModel, Agency> Repository { get; }
        protected MoneyExchangeEntities _db;

        public AgencyService(MoneyExchangeEntities db, IRepository<AgencyModel, Agency> repository)
        {
            _db = db;
            Repository = repository;
        }

        public void CreateAgency(AgencyModel agencyModel)
        {
            var company = _db.Users.SingleOrDefault(u => u.Id == agencyModel.ConnectedUserId).Agency.Company;
            if (company.Agencies.Where(a => a.IsDeleted == false).Count() >= company.MaxAgencies)
                throw new BusinessException(BusinessExceptionType.ActionUnauthorized);
            agencyModel.IsMain = false;
            agencyModel.CreatedAt = DateTime.Now;
            agencyModel.IsDeleted = false;
            company.Agencies.Add(Mapper.Map<Agency>(agencyModel));

            _db.SaveChanges();
        }

        public void UpdateAgency(AgencyModel agencyModel)
        {
            var agency = _db.Agencies.SingleOrDefault(a => a.Id == agencyModel.Id);
            agency = Mapper.Map(agencyModel, agency);
            _db.SaveChanges();
        }

        public void DeleteAgency(AgencyModel agencyModel)
        {
            var agency = _db.Agencies.SingleOrDefault(a => a.Id == agencyModel.Id);
            if (agency.IsMain)
                throw new BusinessException(BusinessExceptionType.ActionUnauthorized);

            // Delete users
            foreach (var user in agency.Users)
            {
                user.IsDeleted = true;
                user.DeletedAt = DateTime.Now;
                foreach (var task in user.Tasks)
                {
                    task.IsDeleted = true;
                    task.DeletedAt = DateTime.Now;
                }
                foreach (var attendance in user.Attendances)
                {
                    attendance.IsDeleted = true;
                    attendance.DeletedAt = DateTime.Now;
                }
            }

            // Decoupling customers
            foreach (var customer in agency.Customers)
            {
                customer.AgencyId = null;
            }


            agency.IsDeleted = true;
            agency.DeletedAt = DateTime.Now;

            _db.SaveChanges();
        }

        public IEnumerable<AgencyModel> GetAgencies(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company companny = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            IEnumerable<Agency> agenciesToShow = companny.Agencies.Where(a => a.IsDeleted == false);

            total = agenciesToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                agenciesToShow = agenciesToShow.Where(e => e.Name.ToLower().Contains(inModel.search.value) || e.Phone.Contains(inModel.search.value) || (e.Cell != null && e.Cell.Contains(inModel.search.value) ) || (e.Email != null && e.Email.ToLower().Contains(inModel.search.value)) );
            }

            //Filters
            if (inModel.filters != null)
            {
                foreach (var filter in inModel.filters)
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("City"))
                    {
                        agenciesToShow = agenciesToShow.Where(a => a.City == filter.value);
                        continue;
                    }
                }
            }

            totalFiltered = agenciesToShow.Count();
            
            return (inModel.length == -1) ? Mapper.Map<IEnumerable<AgencyModel>>(agenciesToShow.OrderByDescending(c => c.Id)) : Mapper.Map<IEnumerable<AgencyModel>>(agenciesToShow.OrderByDescending(c => c.Id).Skip(inModel.start).Take(inModel.length));
        }

        public IEnumerable<AgencyModel> GetAgencies(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<AgencyModel>>(company.Agencies.Where(a => a.IsDeleted == false));
        }

        public bool CreateAgencyAllowed(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            if (company.Agencies.Where(a => a.IsDeleted == false).Count() < company.MaxAgencies)
                return true;
            else
                return false;

        }
    }
}
