﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MoneyExchangeErp.Services
{

    public interface IOperationService
    {
        void CreateOperation(OperationModel operationModel);
        void UpdateOperation(OperationModel operationModel);
        void DeleteOperation(OperationModel operationModel);
        IEnumerable<OperationModel> GetOperations(DatatableInModel inModel, out int total, out int totalFiltered, string type);
        IEnumerable<OperationModel> GetLastOperations(int companyId);
        IEnumerable<DashboardOperationModel> GetOperationsTotalAmount(int companyId);
    }

    public class OperationService : IOperationService
    {
        protected MoneyExchangeEntities _db;
        protected ICashBoxService _cashBoxService;
        protected INotificationService _notificationService;

        public OperationService(MoneyExchangeEntities db, ICashBoxService cashBoxService, INotificationService notificationService)
        {
            _db = db;
            _cashBoxService = cashBoxService;
            _notificationService = notificationService;
        }

        public void CreateOperation(OperationModel operationModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == operationModel.Company_Id);
            var agency = _db.Agencies.SingleOrDefault(a => a.Id == operationModel.Agency_Id);

            var operation = Mapper.Map<Operation>(operationModel);
            operation.CreatedAt = DateTime.Now;
            operation.IsDeleted = false;

            company.Operations.Add(operation);
            agency.Operations.Add(operation);

            _db.SaveChanges();

            if (operationModel.Type == "T")
            {
                _cashBoxService.UpdateCashBoxes(operation.Date.Date, (int)operation.Receiver);
                // Create notification
                foreach (var user in _db.Agencies.SingleOrDefault(a => a.Id == operation.Receiver).Users.Where(u => u.IsDeleted == false))
                {
                    _notificationService.CreateNotification(NotificationType.Transfer, user.Id);
                }
            }
                

             _cashBoxService.UpdateCashBoxes(operation.Date.Date, operation.AgencyId);
        }

        public void UpdateOperation(OperationModel operationModel)
        {
            var operation = _db.Operations.SingleOrDefault(o => o.Id == operationModel.Id);

            if (operation.AgencyId != operationModel.Agency_Id)
            {
                operation.Agency.Operations.Remove(operation);
                operation = Mapper.Map(operationModel, operation);
                _db.Agencies.SingleOrDefault(a => a.Id == operationModel.Agency_Id).Operations.Add(operation);
            }
            else
            {
                operation = Mapper.Map(operationModel, operation);
            }

            _db.SaveChanges();

           if(operationModel.Type == "T")
                _cashBoxService.UpdateCashBoxes(operation.Date.Date, (int)operation.Receiver);

            _cashBoxService.UpdateCashBoxes(operation.Date.Date, operation.AgencyId);
        }

        public void DeleteOperation(OperationModel operationModel)
        {
            var operation = _db.Operations.SingleOrDefault(o => o.Id == operationModel.Id);
            operation.IsDeleted = true;
            operation.DeletedAt = DateTime.Now;

            _db.SaveChanges();

            if (operationModel.Type == "T")
                _cashBoxService.UpdateCashBoxes(operation.Date.Date, (int)operation.Receiver);

            _cashBoxService.UpdateCashBoxes(operation.Date.Date, operation.AgencyId);
        }

        public IEnumerable<OperationModel> GetOperations(DatatableInModel inModel, out int total, out int totalFiltered, string type)
        {
            Company companny = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            User user = _db.Users.SingleOrDefault(u => u.Id == inModel.userId);
            IEnumerable<Operation> operationsToShow = companny.Operations.Where(o => o.IsDeleted == false && o.Type == type);

            total = operationsToShow.Count();

            // Filters
            if (inModel.filters != null)
            {
                // Date Filter
                operationsToShow = operationsToShow.Where(o => o.Date >= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "StartDate").value.Split(' ')[0] + " 00:00:00") && o.Date <= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "EndDate").value.Split(' ')[0] + " 23:59:59"));

                foreach (var filter in inModel.filters.Where(f => f.name != "StartDate" && f.name != "EnDate"))
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("Agency"))
                    {
                        operationsToShow = operationsToShow.Where(o => o.AgencyId == int.Parse(filter.value));
                        continue;
                    }
                    if (filter.name.Equals("Receiver"))
                    {
                        operationsToShow = operationsToShow.Where(o => o.Receiver == int.Parse(filter.value));
                        continue;
                    }
                }
            }
            // Get transactions of the current day
            else
            {
                operationsToShow = operationsToShow.Where(t => t.Date >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0) && t.Date <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59));
                if (!user.Roles.Split('|').Contains("Admin"))
                    operationsToShow = operationsToShow.Where(o => o.AgencyId == user.AgencyId);
            }

            totalFiltered = operationsToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<OperationModel>>(operationsToShow.OrderByDescending(c => c.Date)) : Mapper.Map<IEnumerable<OperationModel>>(operationsToShow.OrderByDescending(c => c.Date).Skip(inModel.start).Take(inModel.length));
        }

        public IEnumerable<OperationModel> GetLastOperations(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<OperationModel>>(company.Operations.OrderByDescending(o => o.Date).Take(20));
        }

        public IEnumerable<DashboardOperationModel> GetOperationsTotalAmount(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            List<DashboardOperationModel> dashboardOperations = new List<DashboardOperationModel>();

            foreach (var agency in company.Agencies.Where(a => a.IsDeleted == false))
            {
                dashboardOperations.Add(new DashboardOperationModel()
                {
                    AgencyName = agency.Name,
                    MonthlyProvisions = agency.Operations.Where(o => o.Date.Month == DateTime.Now.Month && o.Date.Year == DateTime.Now.Year && o.Type == "P" && o.IsDeleted == false).Sum(o => o.Amount) + company.Operations.Where(o => o.Date.Month == DateTime.Now.Month && o.Date.Year == DateTime.Now.Year && o.Type == "T" && o.Receiver == agency.Id && o.IsDeleted  == false).Sum(o => o.Amount),
                    MonthlyWithdrawals = agency.Operations.Where(o => o.Date.Month == DateTime.Now.Month && o.Date.Year == DateTime.Now.Year && ( o.Type == "W" || (o.Type == "T" && o.Id == agency.Id)) && o.IsDeleted == false).Sum(o => o.Amount)
                });
            }

            return dashboardOperations;
        }

    }
}
