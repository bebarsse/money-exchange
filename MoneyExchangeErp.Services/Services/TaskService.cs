﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{

    public interface ITaskService
    {
        void CreateTask(TaskModel taskModel);
        void UpdateTask(TaskModel taskModel);
        void DeleteTask(TaskModel taskModel);
        IEnumerable<TaskModel> GetTasks(DatatableInModel inModel, out int total, out int totalFiltered);
        UserTaskModel GetUserTasks(int userId);
        void CloseTask(TaskModel taskModel);
    }

    public class TaskService : ITaskService
    {
        protected MoneyExchangeEntities _db;
        protected INotificationService _notificationService;

        public TaskService(MoneyExchangeEntities db, INotificationService notificationService)
        {
            _db = db;
            _notificationService = notificationService;
        }

        public void CreateTask(TaskModel taskModel)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == taskModel.Company_Id);
            User user = _db.Users.SingleOrDefault(u => u.Id == taskModel.User_Id);

            var task = Mapper.Map<Data.Task>(taskModel);
            task.Status = "Pending";
            task.CreatedAt = DateTime.Now;
            task.IsDeleted = false;

            company.Tasks.Add(task);
            user.Tasks.Add(task);

            // Create notification
            _notificationService.CreateNotification(NotificationType.Task, taskModel.User_Id);

            _db.SaveChanges();
        }

        public void UpdateTask(TaskModel taskModel)
        {
            Data.Task task = _db.Tasks.SingleOrDefault(t => t.Id == taskModel.Id);
            if (task.UserId == taskModel.User_Id)
            {
                task = Mapper.Map(taskModel, task);
            }
            else
            {
                task.User.Tasks.Remove(task);
                task = Mapper.Map(taskModel, task);
                _db.Users.SingleOrDefault(u => u.Id == taskModel.User_Id).Tasks.Add(task);
            }

            _db.SaveChanges();
        }

        public void DeleteTask(TaskModel taskModel)
        {
            Data.Task task = _db.Tasks.SingleOrDefault(t => t.Id == taskModel.Id);
            task.IsDeleted = true;
            task.DeletedAt = DateTime.Now;

            _db.SaveChanges();
        }

        public IEnumerable<TaskModel> GetTasks(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            IEnumerable<Data.Task> tasksToShow = company.Tasks.Where(t => t.IsDeleted == false);

            total = tasksToShow.Count();

            // Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                tasksToShow = tasksToShow.Where(t => t.Title.ToLower().Contains(inModel.search.value) || t.UserName.ToLower().Contains(inModel.search.value));
            }

            // Filters
            if (inModel.filters != null)
            {
                // Date Filter
                tasksToShow = tasksToShow.Where(t => t.Date >= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "StartDate").value.Split(' ')[0] + " 00:00:00") && t.Date <= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "EndDate").value.Split(' ')[0] + " 23:59:59"));

                foreach (var filter in inModel.filters.Where(f => f.name != "StartDate" && f.name != "EnDate"))
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("Importance"))
                    {
                        tasksToShow = tasksToShow.Where(t => t.Importance == filter.value);
                        continue;
                    }
                    if (filter.name.Equals("Status"))
                    {
                        tasksToShow = tasksToShow.Where(t => t.Status == filter.value);
                        continue;
                    }
                }
            }
            // Get transactions of the current day
            else
            {
                //tasksToShow = tasksToShow.Where(t => t.Date >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0) && t.Date <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59));
                tasksToShow = tasksToShow.Where(t => t.Date.Date == DateTime.Now.Date);
            }

            totalFiltered = tasksToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<TaskModel>>(tasksToShow.OrderByDescending(c => c.Id)) : Mapper.Map<IEnumerable<TaskModel>>(tasksToShow.OrderByDescending(c => c.Id).Skip(inModel.start).Take(inModel.length));
        }

        public UserTaskModel GetUserTasks(int userId)
        {
            var user = _db.Users.SingleOrDefault(u => u.Id == userId);
            var tasksToShow = user.Tasks.Where(t => t.Status == "Pending" && t.IsDeleted == false).OrderByDescending(t => t.Id).Take(50);

            return new UserTaskModel()
            {
                TasksCount = tasksToShow.Count(),
                LowTasksCount = tasksToShow.Where(t => t.Importance == "Low").Count(),
                MediumTasksCount = tasksToShow.Where(t => t.Importance == "Medium").Count(),
                HighTasksCount = tasksToShow.Where(t => t.Importance == "High").Count(),
                Tasks = Mapper.Map<IEnumerable<TaskModel>>(tasksToShow)
            };

        }

        public void CloseTask(TaskModel taskModel)
        {
            Data.Task task = _db.Tasks.SingleOrDefault(t => t.Id == taskModel.Id);
            task.Status = "Completed";

            _db.SaveChanges();
        }
    }
}
