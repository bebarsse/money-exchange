﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Data;
using MoneyExchangeErp.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MoneyExchangeErp.Core.Exceptions.BusinessException;

namespace MoneyExchangeErp.Services
{
    public interface IOperatorService
    {
        void CreateOperator(OperatorModel operatorModel);
        void UpdateOperator(OperatorModel operatorModel);
        void DeleteOperator(OperatorModel operatorModel);
        IEnumerable<OperatorModel> GetOperators(DatatableInModel inModel, out int total, out int totalFiltered);
        IEnumerable<OperatorModel> GetOperators(int companyId);
        OperatorModel GetOperatorById(int operatorId);
    }
    public class OperatorService : IOperatorService
    {
        protected MoneyExchangeEntities _db;
        public IRepository<OperatorModel, Operator> Repository { get; }

        public OperatorService(MoneyExchangeEntities db, IRepository<OperatorModel, Operator> repository)
        {
            _db = db;
            Repository = repository;
        }

        public void CreateOperator(OperatorModel operatorModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == operatorModel.Company_Id);
            var operatorEnity = Mapper.Map<Operator>(operatorModel);
            operatorEnity.IsMain = false;
            operatorEnity.CreatedAt = DateTime.Now;
            operatorEnity.IsDeleted = false;

            company.Operators.Add(operatorEnity);
            _db.SaveChanges();
        }

        public void UpdateOperator(OperatorModel operatorModel)
        {
            var operatorEntity = _db.Operators.SingleOrDefault(o => o.Id == operatorModel.Id);
            if (operatorEntity.IsMain)
                throw new BusinessException(BusinessExceptionType.ActionUnauthorized);
            operatorEntity = Mapper.Map(operatorModel, operatorEntity);
            _db.SaveChanges();
        }

        public void DeleteOperator(OperatorModel operatorModel)
        {
            var operatorEntity = _db.Operators.SingleOrDefault(o => o.Id == operatorModel.Id);
            if (operatorEntity.IsMain)
                throw new BusinessException(BusinessExceptionType.ActionUnauthorized);
            operatorEntity.IsDeleted = true;
            operatorEntity.DeletedAt = DateTime.Now;
            _db.SaveChanges();
        }

        public IEnumerable<OperatorModel> GetOperators(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company companny = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            IEnumerable<Operator> operatorsToShow = companny.Operators.Where( o => o.IsDeleted == false);

            total = operatorsToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                operatorsToShow = operatorsToShow.Where(o => o.Name.ToLower().Contains(inModel.search.value));
            }

            //Filters
            if(inModel.filters != null)
            {
                foreach (var filter in inModel.filters)
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("TransactionType"))
                    {
                        operatorsToShow = operatorsToShow.Where(o => o.TransactionType == filter.value);
                        continue;
                    }
                        
                    if (filter.name.Equals("IsByPercentage"))
                    {
                        operatorsToShow = operatorsToShow.Where(o => o.IsByPercentage == Convert.ToBoolean(filter.value));
                        continue;
                    }

                    if (filter.name.Equals("Category"))
                    {
                        operatorsToShow = operatorsToShow.Where(o => o.Category == filter.value);
                        continue;
                    }

                }
            }

            totalFiltered = operatorsToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<OperatorModel>>(operatorsToShow.OrderBy(c => c.Id)) : Mapper.Map<IEnumerable<OperatorModel>>(operatorsToShow.OrderBy(c => c.Id).Skip(inModel.start).Take(inModel.length));
        }

        public IEnumerable<OperatorModel> GetOperators(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<OperatorModel>>(company.Operators.Where(o => o.IsDeleted == false));
        }

        public OperatorModel GetOperatorById(int operatorId)
        {
            var operatorEntity = _db.Operators.SingleOrDefault(o => o.Id == operatorId && o.IsDeleted == false);
            return Mapper.Map<OperatorModel>(operatorEntity);
        }
    }
}
