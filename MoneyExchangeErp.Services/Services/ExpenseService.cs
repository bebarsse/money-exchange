﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{
    public interface IExpenseService
    {
        void CreateExpense(ExpenseModel expenseModel);
        void DeleteExpense(ExpenseModel expenseModel);
        void UpdateExpense(ExpenseModel expenseModel);
        IEnumerable<ExpenseModel> GetExpenses(DatatableInModel inModel, out int total, out int totalFiltered);
        IEnumerable<DashboardExpenseModel> GetDashboardExpenses(int companyId);
        IEnumerable<ExpenseModel> GetLastExpenses(int companyId);
    }

    public class ExpenseService : IExpenseService
    {
        protected MoneyExchangeEntities _db;
        protected ICashBoxService _cashBoxService;

        public ExpenseService(MoneyExchangeEntities db, ICashBoxService cashBoxService)
        {
            _db = db;
            _cashBoxService = cashBoxService;
        }

        public void CreateExpense(ExpenseModel expenseModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == expenseModel.Company_Id);
            var agency = _db.Agencies.SingleOrDefault(a => a.Id == expenseModel.Agency_Id);
            var category = _db.Categories.SingleOrDefault(c => c.Id == expenseModel.Category_Id);

            var expense = Mapper.Map<Expense>(expenseModel);
            expense.CreatedAt = DateTime.Now;
            expense.IsDeleted = false;

            company.Expenses.Add(expense);
            agency.Expenses.Add(expense);
            category.Expenses.Add(expense);

            _db.SaveChanges();

            _cashBoxService.UpdateCashBoxes(expense.Date.Date, expense.AgencyId);
        }

        public void UpdateExpense(ExpenseModel expenseModel)
        {
            bool agencyChanged = false, categoryChanged = false;

            var expense = _db.Expenses.SingleOrDefault(e => e.Id == expenseModel.Id);

            if (expense.AgencyId != expenseModel.Agency_Id)
            {
                expense.Agency.Expenses.Remove(expense);
                agencyChanged = true;
            }

            if (expense.CategoryId != expenseModel.Category_Id)
            {
                expense.Category.Expenses.Remove(expense);
                categoryChanged = true;
            }

            expense = Mapper.Map(expenseModel, expense);

            if (agencyChanged)
                _db.Agencies.SingleOrDefault(a => a.Id == expenseModel.Agency_Id).Expenses.Add(expense);
            if (categoryChanged)
                _db.Categories.SingleOrDefault(c => c.Id == expenseModel.Category_Id).Expenses.Add(expense);

            _db.SaveChanges();

            _cashBoxService.UpdateCashBoxes(expense.Date.Date, expense.AgencyId);

        }

        public void DeleteExpense(ExpenseModel expenseModel)
        {
            var expense = _db.Expenses.SingleOrDefault(e => e.Id == expenseModel.Id);
            expense.IsDeleted = true;
            expense.DeletedAt = DateTime.Now;

            _db.SaveChanges();

            _cashBoxService.UpdateCashBoxes(expense.Date.Date, expense.AgencyId);
        }

        public IEnumerable<ExpenseModel> GetExpenses(DatatableInModel inModel , out int total, out int totalFiltered)
        {
            Company companny = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            User user = _db.Users.SingleOrDefault(u => u.Id == inModel.userId);
            IEnumerable<Expense> expensesToShow = companny.Expenses.Where(o => o.IsDeleted == false);

            total = expensesToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                expensesToShow = expensesToShow.Where(e => e.Name.ToLower().Contains(inModel.search.value));
            }

            // Filters
            if (inModel.filters != null)
            {
                // Date Filter
                expensesToShow = expensesToShow.Where(e => e.Date >= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "StartDate").value.Split(' ')[0] + " 00:00:00") && e.Date <= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "EndDate").value.Split(' ')[0] + " 23:59:59"));

                foreach (var filter in inModel.filters.Where(f => f.name != "StartDate" && f.name != "EnDate"))
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("Agency"))
                    {
                        expensesToShow = expensesToShow.Where(e => e.AgencyId == int.Parse(filter.value));
                        continue;
                    }
                    if (filter.name.Equals("Category"))
                    {
                        expensesToShow = expensesToShow.Where(e => e.CategoryId == int.Parse(filter.value));
                        continue;
                    }
                }
            }
            // Get expenses of the current day
            else
            {
                expensesToShow = expensesToShow.Where(t => t.Date >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0) && t.Date <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59));
                if (!user.Roles.Split('|').Contains("Admin"))
                    expensesToShow = expensesToShow.Where(e => e.AgencyId == user.AgencyId);
            }

            totalFiltered = expensesToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<ExpenseModel>>(expensesToShow.OrderByDescending(c => c.Date)) : Mapper.Map<IEnumerable<ExpenseModel>>(expensesToShow.OrderByDescending(c => c.Date).Skip(inModel.start).Take(inModel.length));
        }

        public IEnumerable<DashboardExpenseModel> GetDashboardExpenses(int companyId)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            List<DashboardExpenseModel> dashboardExpenses = new List<DashboardExpenseModel>();

            foreach (var agency in company.Agencies.Where(a => a.IsDeleted == false))
            {
                dashboardExpenses.Add(new DashboardExpenseModel()
                {
                    AgencyName = agency.Name,
                    DailyExpenses = agency.Expenses.Where(e => e.Date.Date == DateTime.Now.Date && e.IsDeleted == false).Sum(e => e.Amount),
                    MonthlyExpenses = agency.Expenses.Where(e => e.Date.Month == DateTime.Now.Month && e.Date.Year == DateTime.Now.Year && e.IsDeleted == false).Sum(e => e.Amount)
                });
            }

            return dashboardExpenses;
        }

        public IEnumerable<ExpenseModel> GetLastExpenses(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<ExpenseModel>>(company.Expenses.OrderByDescending(e => e.Date).Take(20));
        }
    }
}
