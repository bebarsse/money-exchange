﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{

    public interface INotificationService
    {
        void CreateNotification(NotificationType notificationType, int userId);
        NotificationAlertModel GetNotificationsAlert(int userId);
        IEnumerable<NotificationModel> GetNotifications(int userId);
        void SetViewed(int userId);
    }


    public class NotificationService : INotificationService
    {
        protected MoneyExchangeEntities _db;

        public NotificationService(MoneyExchangeEntities db)
        {
            _db = db;
        }

        public void CreateNotification(NotificationType notificationType, int userId)
        {
            User user = _db.Users.SingleOrDefault(u => u.Id == userId);

            Notification notification = new Notification();
            switch (notificationType)
            {
                case NotificationType.Task:
                    notification.Title = "Nouvelle Tâche";
                    notification.Message = "une nouvelle tâche vous a été assigné";
                    notification.Type = "Task";
                    break;
                case NotificationType.Transfer:
                    notification.Title = "Nouveau Transfert";
                    notification.Message = "Une nouvelle opération de transfert a été effectué pour votre agence";
                    notification.Type = "Transfer";
                    break;
                default:
                    break;
            }
            notification.IsViewed = false;
            notification.CreatedAt = DateTime.Now;
            notification.IsDeleted = false;
            user.Notifications.Add(notification);

            _db.SaveChanges();
        }

        public NotificationAlertModel GetNotificationsAlert(int userId)
        {
            User user = _db.Users.SingleOrDefault(u => u.Id == userId);
            return new NotificationAlertModel()
            {
                NotificationsCount = user.Notifications.Where(n => n.IsViewed == false && n.IsDeleted == false).Count(),
                LastNotifications = Mapper.Map<IEnumerable<NotificationModel>>(user.Notifications.Where(n => n.IsDeleted == false).OrderByDescending(n => n.CreatedAt).Take(3))
            };
        }

        public IEnumerable<NotificationModel> GetNotifications(int userId)
        {
            User user = _db.Users.SingleOrDefault(u => u.Id == userId);
            return Mapper.Map<IEnumerable<NotificationModel>>(user.Notifications.Where(n => n.IsDeleted == false).OrderByDescending(n => n.CreatedAt).Take(20));
        }

        public void SetViewed(int userId)
        {
            User user = _db.Users.SingleOrDefault(u => u.Id == userId);
            foreach (var notification in user.Notifications.Where(n => n.IsViewed == false))
            {
                notification.IsViewed = true;
                _db.SaveChanges();
            }
        }

    }
}
