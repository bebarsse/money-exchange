﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{
    public interface IAttendanceService
    {
        void CreateAttendance(AttendanceModel attendanceModel);
        void UpdateAttendance(AttendanceModel attendanceModel);
        void DeleteAttendance(AttendanceModel attendanceModel);
        void DeleteAttendaces(HourRecordModel hourRecordModel);
        IEnumerable<HourRecordModel> GetAttendances(DatatableInModel inModel, out int total, out int totalFiltered);
        AttendanceModel GetLastAttendance(int userId);
    }

    public class AttendanceService : IAttendanceService
    {
        protected MoneyExchangeEntities _db;

        public AttendanceService(MoneyExchangeEntities db)
        {
            _db = db;
        }

        public void CreateAttendance(AttendanceModel attendanceModel)
        {
            User user = _db.Users.SingleOrDefault(u => u.Id == attendanceModel.User_Id && u.IsDeleted == false);

            Attendance attendance = Mapper.Map<Attendance>(attendanceModel);
            attendance.CreatedAt = DateTime.Now;
            attendance.IsDeleted = false;

            user.Attendances.Add(attendance);
            _db.SaveChanges();
        }

        public void UpdateAttendance(AttendanceModel attendanceModel)
        {
            Attendance attendance = _db.Attendances.SingleOrDefault(a => a.Id == attendanceModel.Id);
            attendance = Mapper.Map(attendanceModel, attendance);

            _db.SaveChanges();
        }

        public void DeleteAttendance(AttendanceModel attendanceModel)
        {
            Attendance attendance = _db.Attendances.SingleOrDefault(a => a.Id == attendanceModel.Id);
            attendance.IsDeleted = true;
            attendance.DeletedAt = DateTime.Now;

            _db.SaveChanges();
        }

        public void DeleteAttendaces(HourRecordModel hourRecordModel)
        {
            foreach (var attendanceModel in hourRecordModel.Attendances)
            {
                Attendance attendance = _db.Attendances.SingleOrDefault(a => a.Id == attendanceModel.Id);
                attendance.IsDeleted = true;
                attendance.DeletedAt = DateTime.Now;
                _db.SaveChanges();
            }

        }

        public IEnumerable<HourRecordModel> GetAttendances(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            IEnumerable<Attendance> attendancesToShow = company.Agencies.Where(a => a.IsDeleted == false).SelectMany(a => a.Users.Where(u => u.IsDeleted == false).SelectMany(u => u.Attendances.Where(c => c.IsDeleted == false)));

            // Filters
            if (inModel.filters != null)
            {
                attendancesToShow = attendancesToShow.Where(a => a.Date.Date == DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "Day").value).Date);
            }
            // Get transactions of the current day
            else
            {
                attendancesToShow = attendancesToShow.Where(a => a.Date.Date == DateTime.Now.Date);
            }

            List<HourRecordModel> hourRecordModel = new List<HourRecordModel>();

            foreach (var attendanceGroup in attendancesToShow.GroupBy(a => a.UserId))
            {
                hourRecordModel.Add(new HourRecordModel()
                {
                    Date = attendanceGroup.First().Date,
                    UserName = attendanceGroup.First().User.FullName,
                    AgencyName = attendanceGroup.First().User.AgencyName,
                    Attendances = Mapper.Map<IEnumerable<AttendanceModel>>(attendanceGroup.Where(a => a.IsDeleted == false).OrderBy(a => a.Date))
                });
            }

            // Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                hourRecordModel = hourRecordModel.Where(h => h.UserName.ToLower().Contains(inModel.search.value)).ToList();
            }

            total = totalFiltered = hourRecordModel.Count();

            return (inModel.length == -1) ? hourRecordModel : hourRecordModel.Skip(inModel.start).Take(inModel.length);
        }

        public AttendanceModel GetLastAttendance(int userId)
        {
            User user = _db.Users.SingleOrDefault(u => u.Id == userId);
            Attendance lastAttendance = (user.Attendances.Where(a => a.Date.Date == DateTime.Now.Date && a.IsDeleted == false).Count() != 0) ? user.Attendances.Where(a => a.Date.Date == DateTime.Now.Date && a.IsDeleted == false).OrderByDescending(a => a.Date).First() : null;

            return Mapper.Map<AttendanceModel>(lastAttendance);
        }
    }
}
