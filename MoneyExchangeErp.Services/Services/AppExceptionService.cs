﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using MoneyExchangeErp.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{
    public interface IAppExceptionService
    {
        IRepository<AppExceptionModel, AppException> Repository { get; }
    }

    public class AppExceptionService : IAppExceptionService
    {
        protected MoneyExchangeEntities _db;
        public IRepository<AppExceptionModel, AppException> Repository { get;}

        public AppExceptionService(MoneyExchangeEntities db, IRepository<AppExceptionModel, AppException> repository)
        {
            _db = db;
            Repository = repository;
        }
    }
}
