﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{

    public interface ICashBoxService
    {
        void UpdateCashBoxes(DateTime date, int agencyId);
        IEnumerable<CashBoxModel> GetCashBoxes(int companyId);
    }

    public class CashBoxService : ICashBoxService
    {
        protected MoneyExchangeEntities _db;

        public CashBoxService(MoneyExchangeEntities db)
        {
            _db = db;
        }

        public void UpdateCashBoxes(DateTime date, int agencyId)
        {
            var agency = _db.Agencies.SingleOrDefault(a => a.Id == agencyId);

            if (agency.CashBoxes.SingleOrDefault(c => c.Date == date) == null)
            {
                var cashBoxEntity = new CashBox()
                {
                    Date = date,
                    Amount = 0,
                    CreatedAt = DateTime.Now,
                    IsDeleted = false
                };
                agency.Company.CashBoxes.Add(cashBoxEntity);
                agency.CashBoxes.Add(cashBoxEntity);
                _db.SaveChanges();
            }

            foreach (var cashBox in agency.CashBoxes.Where(c => c.Date >= date).OrderBy(c => c.Date))
            {
                SetCashBox(agency, cashBox);
            }
        }

        public void SetCashBox(Agency agency,  CashBox cashBox)
        {
            // Initial box amount
            var initialBox = agency.CashBoxes.Where(c => c.Date.Date < cashBox.Date.Date && c.IsDeleted == false).OrderByDescending(c => c.Date).FirstOrDefault();
            var initialBoxAmount = (initialBox == null) ? 0 : initialBox.Amount;

            // Operations
            double provisionsAmount = agency.Operations.Where(p => p.Date.Date == cashBox.Date.Date && p.Type == "P" && p.IsDeleted == false).Sum(p => p.Amount);
            double withdrawalsAmount = agency.Operations.Where(w => w.Date.Date == cashBox.Date.Date && w.Type == "W" && w.IsDeleted == false).Sum(w => w.Amount);
            double outTransfersAmount = agency.Operations.Where(t => t.Date.Date == cashBox.Date.Date && t.Type == "T" && t.IsDeleted == false).Sum(t => t.Amount);

            var endDay = cashBox.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            double inTransfersAmount = (_db.Operations.Where(o => cashBox.Date.Date <= o.Date && o.Date <= endDay && o.Receiver == agency.Id && o.IsDeleted == false).Any()) ? _db.Operations.Where(o => cashBox.Date.Date <= o.Date && o.Date <= endDay && o.Receiver == agency.Id && o.IsDeleted == false).Sum(o => o.Amount) : 0;

            // Expenses
            double expensesAmount = agency.Expenses.Where(e => e.Date.Date == cashBox.Date.Date && e.IsDeleted == false).Sum(e => e.Amount);

            // Transactions
            double inTransactionsAmount = agency.Transactions.Where(t => t.Date.Date == cashBox.Date.Date && t.IsDeleted == false && t.TransactionType == "I").Sum(t => t.Amount + t.Fees);
            double ouTransactionsAmount = agency.Transactions.Where(t => t.Date.Date == cashBox.Date.Date && t.IsDeleted == false && t.TransactionType == "O").Sum(t => t.Amount);

            cashBox.Amount = (initialBoxAmount + inTransactionsAmount + provisionsAmount + inTransfersAmount) - (ouTransactionsAmount + withdrawalsAmount + outTransfersAmount + expensesAmount);

            _db.SaveChanges();

        }

        public IEnumerable<CashBoxModel> GetCashBoxes(int companyId)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<CashBoxModel>>(company.CashBoxes.Where(c => c.IsDeleted == false).GroupBy(c => c.AgencyId).Select(e => e.OrderByDescending(i => i.Date).FirstOrDefault()));
        }

    }
}
