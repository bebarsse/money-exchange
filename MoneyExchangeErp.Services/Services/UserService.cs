﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Exceptions;
using MoneyExchangeErp.Core.Helpers;
using MoneyExchangeErp.Data;
using MoneyExchangeErp.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MoneyExchangeErp.Core.Exceptions.BusinessException;

namespace MoneyExchangeErp.Services
{
    public interface IUserService
    {
        IRepository<UserModel, User> Repository { get; }
        UserModel Authenticate(string email, string password);
        void CreateUser(UserModel userModel);
        void UpdateUser(UserModel userModel);
        void DeleteUser(UserModel userModel);
        void ToggleBanUser(UserModel userModel);
        IEnumerable<UserModel> GetUsers(DatatableInModel inModel, out int total, out int totalFiltered);
        void UpdateProfilInformations(UserModel userModel);
        void UpdateProfilPassword(UserModel userModel);
        IEnumerable<UserModel> GetUsersByCompany(int companyId);
        bool UsernameExist(string username);
    }

    public class UserService : IUserService
    {
        protected MoneyExchangeEntities _db;
        public IRepository<UserModel, User> Repository { get; }

        public UserService(MoneyExchangeEntities db, IRepository<UserModel, User> repository)
        {
            _db = db;
            Repository = repository;
        }

        public UserModel Authenticate(string username, string password)
        {
            var passwordHash = Security.CalculateMD5Hash(password);
            var user = _db.Users.SingleOrDefault(u => u.Username == username &&  u.Password ==  passwordHash && u.IsDeleted == false && u.IsBlocked == false);
            if (user.Equals(null)) throw new Exception();
            return Mapper.Map<UserModel>(user);
        }

        public void CreateUser(UserModel userModel)
        {
            if (UsernameExist(userModel.Username))
                throw new BusinessException(BusinessExceptionType.UsernameExist);

            var agency = _db.Agencies.SingleOrDefault(a => a.Id == userModel.Agency_Id);
            var user = Mapper.Map<User>(userModel);
            user.IsBlocked = false;
            user.IsMain = false;
            user.CreatedAt = DateTime.Now;
            user.IsDeleted = false;

            agency.Users.Add(user);

            _db.SaveChanges();
        }

        public void UpdateUser(UserModel userModel)
        {

            var user = _db.Users.SingleOrDefault(u => u.Id == userModel.Id);
            // Check if username already exist
            if (user.Username != userModel.Username)
            {
                if (UsernameExist(userModel.Username))
                    throw new BusinessException(BusinessExceptionType.UsernameExist);
            }

            if (userModel.Agency_Id == user.AgencyId)
            {
                user = Mapper.Map(userModel, user);
            }
            else
            {
                if (user.IsMain)
                    throw new BusinessException(BusinessExceptionType.ActionUnauthorized);
                var agency = user.Agency;
                agency.Users.Remove(user);
                user = Mapper.Map(userModel, user);
                _db.Agencies.SingleOrDefault(a => a.Id == userModel.Agency_Id).Users.Add(user);
            }

            _db.SaveChanges();
        }

        public void DeleteUser(UserModel userModel)
        {
            var user = _db.Users.SingleOrDefault(u => u.Id == userModel.Id);
            if (user.IsMain)
                throw new BusinessException(BusinessExceptionType.ActionUnauthorized);

            foreach (var task in user.Tasks)
            {
                task.IsDeleted = true;
                task.DeletedAt = DateTime.Now;
            }
            foreach (var attendance in user.Attendances)
            {
                attendance.IsDeleted = true;
                attendance.DeletedAt = DateTime.Now;
            }
            user.IsDeleted = true;
            user.DeletedAt = DateTime.Now;

            _db.SaveChanges();
        }

        public void ToggleBanUser(UserModel userModel)
        {
            var user = _db.Users.SingleOrDefault(u => u.Id == userModel.Id);
            if (user.IsMain)
                throw new BusinessException(BusinessExceptionType.ActionUnauthorized);
            user.IsBlocked = !user.IsBlocked;
            _db.SaveChanges();
        }

        public IEnumerable<UserModel> GetUsers(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            IEnumerable<User> usersToShow = company.Agencies.SelectMany(a => a.Users.Where(u => u.IsDeleted == false));

            total = usersToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                usersToShow = usersToShow.Where(u => u.VirtualFullName.ToLower().Contains(inModel.search.value) || u.VirtualReverseFullName.ToLower().Contains(inModel.search.value) || u.Username.ToLower().Contains(inModel.search.value));
            }

            //Filters
            if (inModel.filters != null)
            {
                foreach (var filter in inModel.filters)
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("Agency"))
                    {
                        usersToShow = usersToShow.Where(a => a.AgencyId == int.Parse(filter.value));
                        continue;
                    }
                }
            }

            totalFiltered = usersToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<UserModel>>(usersToShow.OrderByDescending(c => c.Id)) : Mapper.Map<IEnumerable<UserModel>>(usersToShow.OrderByDescending(c => c.Id).Skip(inModel.start).Take(inModel.length));
        }

        public void UpdateProfilInformations(UserModel userModel)
        {
            var user = _db.Users.SingleOrDefault(u => u.Id == userModel.Id);
            user.Civility = userModel.Civility;
            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.Username = userModel.Username;
            _db.SaveChanges();
        }

        public void UpdateProfilPassword(UserModel userModel)
        {
            var user = _db.Users.SingleOrDefault(u => u.Id == userModel.Id);
            if (user.Password != Security.CalculateMD5Hash(userModel.CurrentPassword))
                throw new BusinessException(BusinessExceptionType.IncorrectPassword);
            else
                user.Password = Security.CalculateMD5Hash(userModel.PasswordToHash);

            _db.SaveChanges();
        }

        public IEnumerable<UserModel> GetUsersByCompany(int companyId)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<UserModel>>(company.Agencies.Where(a => a.IsDeleted == false).SelectMany(a => a.Users.Where(u => u.IsDeleted == false)));
        }

        public bool UsernameExist(string username)
        {
            return (_db.Users.SingleOrDefault(u => u.Username == username && u.IsDeleted == false) == null) ? false : true;
        }

    }
}
