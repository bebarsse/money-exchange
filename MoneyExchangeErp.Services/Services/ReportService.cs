﻿using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Core.Helpers;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{
    public interface IReportService
    {
        IEnumerable<DailyReportModel> GetDailyReport(int companyId, DateTime day);
        IEnumerable<MonthlyReportModel> GetMonthlyReport(int companyId, DateTime month);
        IEnumerable<ExpenseReportModel> GetExpenseReport(int companyId, DateTime year);
        IEnumerable<ProfitReportModel> GetProfitReport(int companyId, DateTime year);
    }

    public class ReportService : IReportService
    {
        protected MoneyExchangeEntities _db;

        public ReportService(MoneyExchangeEntities db)
        {
            _db = db;
        }

        public IEnumerable<DailyReportModel> GetDailyReport(int companyId, DateTime day)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            List<DailyReportModel> dailyReports = new List<DailyReportModel>();

            double grandTotalIn = 0, grandTotalOut = 0, grandTotalProfit = 0, grandTotalExpenses = 0, initialCashBox = 0, finalCashBox = 0;

            foreach (var agency in company.Agencies.Where(a => a.IsDeleted == false))
            {
                // Transactions
                List<DailyTransactionsModel> dailyTransactions = new List<DailyTransactionsModel>();
                foreach (var transactionGroup in agency.Transactions.Where(t => t.Date.Date == day.Date && t.IsDeleted == false).GroupBy(o => o.OperatorId))
                {
                    foreach (var element in transactionGroup.GroupBy(t => t.TransactionType))
                    {
                        dailyTransactions.Add(new DailyTransactionsModel()
                        {
                            OperatorName = element.First().OperatorName,
                            TransactionType = element.First().TransactionType,
                            OperatorType = element.First().OperatorType,
                            Number = element.Count(),
                            Volume = element.Sum(t => t.Amount + t.Fees),
                            Profit = element.Sum(t => t.Profit)
                        });

                        if (element.Key == "I")
                            grandTotalIn += element.Sum(t => t.Amount + t.Fees);
                        if (element.Key == "O")
                            grandTotalOut += element.Sum(t => t.Amount + t.Fees);
                    }

                    grandTotalProfit += transactionGroup.Sum(t => t.Profit);
                }

                // Operations
                List<DailyOperationModel> dailyOperations = new List<DailyOperationModel>();
                dailyOperations.Add(new DailyOperationModel() {
                    OperationName = "Alimentations",
                    Volume = agency.Operations.Where(o => o.Date.Date == day.Date && o.Type == "P" && o.IsDeleted == false).Sum(o => o.Amount) + company.Operations.Where(o => o.Date.Month == day.Month && o.Date.Year == day.Year && o.Type == "T" && o.Receiver == agency.Id && o.IsDeleted == false).Sum(o => o.Amount),
                    Number = agency.Operations.Where(o => o.Date.Date == day.Date && o.Type == "P" && o.IsDeleted == false).Count() + company.Operations.Where(o => o.Date.Month == day.Month && o.Date.Year == day.Year && o.Type == "T" && o.Receiver == agency.Id && o.IsDeleted == false).Count(),
                });
                dailyOperations.Add(new DailyOperationModel()
                {
                    OperationName = "Retraits",
                    Volume = agency.Operations.Where(o => o.Date.Date == day.Date &&  (o.Type == "W" || (o.Type == "T" && o.Id == agency.Id)) && o.IsDeleted == false).Sum(o => o.Amount),
                    Number = agency.Operations.Where(o => o.Date.Date == day.Date && (o.Type == "W" || (o.Type == "T" && o.Id == agency.Id)) && o.IsDeleted == false).Count()
                });

                // Expenses
                List<DailyExpenseModel> dailyExpenses = new List<DailyExpenseModel>();
                foreach (var expenseGroup in agency.Expenses.Where(e => e.Date.Date == day.Date && e.IsDeleted == false).GroupBy(e => e.Category))
                {
                    dailyExpenses.Add(new DailyExpenseModel()
                    {
                        CategoryName = expenseGroup.First().Category.Name,
                        Number = expenseGroup.Count(),
                        Volume = expenseGroup.Sum(e => e.Amount)
                    });

                    grandTotalExpenses += expenseGroup.Sum(e => e.Amount);
                }

                // Initial box amount
                initialCashBox  = (agency.CashBoxes.Where(c => c.Date.Date < day.Date && c.IsDeleted == false).OrderByDescending(c => c.Date).FirstOrDefault() == null) ? 0 : agency.CashBoxes.Where(c => c.Date.Date < day.Date).OrderByDescending(c => c.Date).FirstOrDefault().Amount;

                // Final Box Amount
                finalCashBox = (agency.CashBoxes.SingleOrDefault(c => c.Date.Date == day.Date && c.IsDeleted == false) == null) ? 0 : agency.CashBoxes.SingleOrDefault(c => c.Date == day.Date && c.IsDeleted == false).Amount;


                dailyReports.Add(new DailyReportModel()
                {
                    AgencyName = agency.Name,
                    DailyTransactions = dailyTransactions,
                    DailyOperations = dailyOperations,
                    DailyExpenses = dailyExpenses,
                    GrandTotalIn = grandTotalIn,
                    GrandTotalOut = grandTotalOut,
                    GrandTotalExpenses = grandTotalExpenses,
                    GrandTotalProfit = grandTotalProfit,
                    InitialCashBox = initialCashBox,
                    FinalCashBox = finalCashBox
                });

                grandTotalIn = 0; grandTotalOut = 0; grandTotalProfit = 0; grandTotalExpenses = 0; initialCashBox = 0; finalCashBox = 0;

            }

            return dailyReports;
        }

        public IEnumerable<MonthlyReportModel> GetMonthlyReport(int companyId, DateTime month)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            List<MonthlyReportModel> monthlyReports = new List<MonthlyReportModel>();
            double grandTotalProfit = 0, grandTotalExpenses = 0;

            foreach (var agency in company.Agencies.Where(a => a.IsDeleted == false))
            {
                // Transactions
                List<MonthlyTransactionsModel> monthlyTransactions = new List<MonthlyTransactionsModel>();
                foreach (var transactionGroup in agency.Transactions.Where(t => t.Date.Month == month.Month && t.Date.Year == month.Year && t.IsDeleted == false).GroupBy(o => o.OperatorId))
                {
                    foreach (var element in transactionGroup.GroupBy(t => t.TransactionType))
                    {
                        monthlyTransactions.Add(new MonthlyTransactionsModel()
                        {
                            OperatorName = element.First().OperatorName,
                            TransactionType = element.First().TransactionType,
                            OperatorType = element.First().OperatorType,
                            Number = element.Count(),
                            Volume = element.Sum(t => t.Amount + t.Fees),
                            Profit = element.Sum(t => t.Profit)
                        });
                    }

                    grandTotalProfit += transactionGroup.Sum(t => t.Profit);
                }

                // Expenses
                List<MonthlyExpenseModel> monthlyExpenses = new List<MonthlyExpenseModel>();
                foreach (var expenseGroup in agency.Expenses.Where(e => e.Date.Month == month.Month && e.Date.Year == month.Year && e.IsDeleted == false).GroupBy(e => e.Category))
                {
                    monthlyExpenses.Add(new MonthlyExpenseModel()
                    {
                        CategoryName = expenseGroup.First().Category.Name,
                        Number = expenseGroup.Count(),
                        Volume = expenseGroup.Sum(e => e.Amount)
                    });

                    grandTotalExpenses += expenseGroup.Sum(e => e.Amount);
                }

                monthlyReports.Add(new MonthlyReportModel()
                {
                    AgencyName = agency.Name,
                    MonthlyTransactions = monthlyTransactions,
                    MonthlyExpenses = monthlyExpenses,
                    GrandTotalExpenses = grandTotalExpenses,
                    GrandTotalProfit = grandTotalProfit
                });

                grandTotalProfit = 0; grandTotalExpenses = 0;
            }

            return monthlyReports;
        }

        public IEnumerable<ExpenseReportModel> GetExpenseReport(int companyId, DateTime year)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            List<ExpenseReportModel> expenseReport = new List<ExpenseReportModel>();

            foreach (var agency in company.Agencies.Where(a => a.IsDeleted == false))
            {
                List<MonthlyExpenseReportModel> monthlyExpenses = new List<MonthlyExpenseReportModel>();
                List<CategoryExpenseReportModel> categoryExpenses = new List<CategoryExpenseReportModel>();

                foreach (var expensesGroup in agency.Expenses.Where(e => e.Date.Year == year.Year && e.IsDeleted == false).GroupBy(e => e.Date.Month).OrderBy(e => e.Key))
                {
                    monthlyExpenses.Add(new MonthlyExpenseReportModel()
                    {
                        Month = expensesGroup.First().Date.ToString("MMMM").ToTitleCase(),
                        Volume = expensesGroup.Sum(e => e.Amount)
                    });
                }

                foreach (var expensesCategoryGroup in agency.Expenses.Where(e => e.Date.Year == year.Year && e.IsDeleted == false).GroupBy(e => e.CategoryId))
                {
                    categoryExpenses.Add(new CategoryExpenseReportModel()
                    {
                        CategoryName = expensesCategoryGroup.First().Category.Name,
                        Volume = expensesCategoryGroup.Sum(e => e.Amount)
                    });
                }

                expenseReport.Add(new ExpenseReportModel()
                {
                    AgencyName = agency.Name,
                    MonthlyExpenses = monthlyExpenses,
                    CategoryExpenses = categoryExpenses.OrderBy(e => e.CategoryName),
                    GrandTotal = monthlyExpenses.Sum(e => e.Volume)
                });
            }

            return expenseReport;
        }

        public IEnumerable<ProfitReportModel> GetProfitReport(int companyId, DateTime year)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            List<ProfitReportModel> profitReport = new List<ProfitReportModel>();

            foreach (var agency in company.Agencies.Where(a => a.IsDeleted == false))
            {
                List<MonthlyProfitReportModel> monthlyProfits = new List<MonthlyProfitReportModel>();
                List<OperatorProfitReportModel> operatorProfits = new List<OperatorProfitReportModel>();
                List<MonthlyExpenseReportModel> monthlyExpenses = new List<MonthlyExpenseReportModel>();

                foreach (var transactionGroup in agency.Transactions.Where(t => t.Date.Year == year.Year && t.IsDeleted == false).GroupBy(t => t.Date.Month).OrderBy(t => t.Key))
                {
                    monthlyProfits.Add(new MonthlyProfitReportModel()
                    {
                        Month = transactionGroup.First().Date.ToString("MMMM").ToTitleCase(),
                        Volume = transactionGroup.Sum(t => t.Profit)
                    });
                }

                foreach (var transactionGroup in agency.Transactions.Where(t => t.Date.Year == year.Year && t.IsDeleted == false).GroupBy(t => t.OperatorId))
                {
                    operatorProfits.Add(new OperatorProfitReportModel()
                    {
                        OperatorName = transactionGroup.First().OperatorName,
                        Volume = transactionGroup.Sum(t => t.Profit)
                    });
                }

                foreach (var expensesGroup in agency.Expenses.Where(e => e.Date.Year == year.Year && e.IsDeleted == false).GroupBy(e => e.Date.Month).OrderBy(e => e.Key))
                {
                    monthlyExpenses.Add(new MonthlyExpenseReportModel()
                    {
                        Month = expensesGroup.First().Date.ToString("MMMM").ToTitleCase(),
                        Volume = expensesGroup.Sum(e => e.Amount)
                    });
                }


                profitReport.Add(new ProfitReportModel()
                {
                    AgencyName = agency.Name,
                    MonthlyProfits = monthlyProfits,
                    OperatorProfits = operatorProfits,
                    MonthlyExpenses = monthlyExpenses,
                    GrandTotalProfit = monthlyProfits.Sum(e => e.Volume),
                    GrandTotalExpense = monthlyExpenses.Sum(e => e.Volume),
                });

            }

            return profitReport;
        }
    }
}
