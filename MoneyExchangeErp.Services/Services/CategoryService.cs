﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{

    public interface ICategoryService
    {
        int CreateCategory(CategoryModel categoryModel);
        void UpdateCategory(CategoryModel categoryModel);
        void DeleteCategory(CategoryModel categoryModel);
        IEnumerable<CategoryModel> GetCategories(DatatableInModel inModel, out int total, out int totalFiltered);
        IEnumerable<CategoryModel> GetCategories(int companyId);
    }

    public class CategoryService : ICategoryService
    {
        protected MoneyExchangeEntities _db;

        public CategoryService(MoneyExchangeEntities db)
        {
            _db = db;
        }

        public int CreateCategory(CategoryModel categoryModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == categoryModel.Company_Id);
            var category = Mapper.Map<Category>(categoryModel);
            category.CreatedAt = DateTime.Now;
            category.IsDeleted = false;

            company.Categories.Add(category);
            _db.SaveChanges();

            return category.Id;
        }

        public void UpdateCategory(CategoryModel categoryModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == categoryModel.Company_Id);
            var category = _db.Categories.SingleOrDefault(c => c.Id == categoryModel.Id);
            category = Mapper.Map(categoryModel, category);

            _db.SaveChanges();
        }

        public void DeleteCategory(CategoryModel categoryModel)
        {
            var category = _db.Categories.SingleOrDefault(c => c.Id == categoryModel.Id);
            category.IsDeleted = true;
            category.DeletedAt = DateTime.Now;

            _db.SaveChanges();
        }

        public IEnumerable<CategoryModel> GetCategories(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            IEnumerable<Category> categoriesToShow = company.Categories.Where(o => o.IsDeleted == false);

            total = categoriesToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                categoriesToShow = categoriesToShow.Where(c => c.Name.ToLower().Contains(inModel.search.value));
            }
            

            totalFiltered = categoriesToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<CategoryModel>>(categoriesToShow.OrderByDescending(c => c.Id)) : Mapper.Map<IEnumerable<CategoryModel>>(categoriesToShow.OrderByDescending(c => c.Id).Skip(inModel.start).Take(inModel.length));
        }

        public IEnumerable<CategoryModel> GetCategories(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<CategoryModel>>(company.Categories.Where(a => a.IsDeleted == false));
        }
    }
}
