﻿using AutoMapper;
using MoneyExchangeErp.Core.DTO;
using MoneyExchangeErp.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyExchangeErp.Services
{
    public interface ITransactionService
    {
        int CreateTransaction(TransactionModel transactionModel);
        void UpdateTransaction(TransactionModel transactionModel);
        void DeleteTransaction(TransactionModel transactionModel);
        IEnumerable<TransactionModel> GetTransactions(DatatableInModel inModel, out int total, out int totalFiltered);
        IEnumerable<DashboardTransactionModel> GetDashboardTransactions(int companyId);
        IEnumerable<TransactionModel> GetLastTransactions(int companyId);
        void SetTransactionFilePath(int transactionId, string filePath);
    }

    public class TransactionService : ITransactionService
    {
        protected MoneyExchangeEntities _db;
        protected ICashBoxService _cashBoxService;

        public TransactionService(MoneyExchangeEntities db, ICashBoxService cashBoxService)
        {
            _db = db;
            _cashBoxService = cashBoxService;
        }

        public int CreateTransaction(TransactionModel transactionModel)
        {
            var company = _db.Companies.SingleOrDefault(c => c.Id == transactionModel.Company_Id);
            var agency = _db.Agencies.SingleOrDefault(c => c.Id == transactionModel.Agency_Id);
            var operatorEntity = _db.Operators.SingleOrDefault(a => a.Id == transactionModel.Operator_Id);
            var customer = _db.Customers.SingleOrDefault(a => a.Id == transactionModel.Customer_Id);

            var transaction = Mapper.Map<Transaction>(transactionModel);
            transaction.CreatedAt = DateTime.Now;
            transaction.IsDeleted = false;

            company.Transactions.Add(transaction);
            agency.Transactions.Add(transaction);
            operatorEntity.Transactions.Add(transaction);
            customer.Transactions.Add(transaction);

            _db.SaveChanges();

            _cashBoxService.UpdateCashBoxes(transaction.Date.Date, transaction.AgencyId);

            return transaction.Id;
        }

        public void UpdateTransaction(TransactionModel transactionModel)
        {
            bool operatorChanged = false, customerChanged = false;

            var transaction = _db.Transactions.SingleOrDefault(t => t.Id == transactionModel.Id);

            if (transaction.OperatorId != transactionModel.Operator_Id)
            {
                transaction.Operator.Transactions.Remove(transaction);
                operatorChanged = true;
            }

            if (transaction.CustomerId != transactionModel.Customer_Id)
            {
                transaction.Customer.Transactions.Remove(transaction);
                customerChanged = true;
            }

            transaction = Mapper.Map(transactionModel, transaction);

            if (operatorChanged)
                _db.Operators.SingleOrDefault(o => o.Id == transactionModel.Operator_Id).Transactions.Add(transaction);
            if (customerChanged)
                _db.Customers.SingleOrDefault(c => c.Id == transactionModel.Customer_Id).Transactions.Add(transaction);

            _db.SaveChanges();

            _cashBoxService.UpdateCashBoxes(transaction.Date.Date, transaction.AgencyId);
        }

        public void DeleteTransaction(TransactionModel transactionModel)
        {
            var transaction = _db.Transactions.SingleOrDefault(t => t.Id == transactionModel.Id);
            transaction.IsDeleted = true;
            transaction.DeletedAt = DateTime.Now;

            _db.SaveChanges();

            _cashBoxService.UpdateCashBoxes(transaction.Date.Date, transaction.AgencyId);
        }
        
        public IEnumerable<TransactionModel> GetTransactions(DatatableInModel inModel, out int total, out int totalFiltered)
        {
            Company companny = _db.Companies.SingleOrDefault(c => c.Id == inModel.companyId);
            User user = _db.Users.SingleOrDefault(u => u.Id == inModel.userId);
            IEnumerable<Transaction> transactionsToShow = companny.Transactions.Where(t => t.IsDeleted == false);

            total = transactionsToShow.Count();

            //Search
            if (inModel.search.value != null)
            {
                inModel.search.value = inModel.search.value.Trim().ToLower();
                transactionsToShow = transactionsToShow.Where( t => t.Customer.CIN.ToLower().Contains(inModel.search.value) || t.VirtualCustomerName.ToLower().Contains(inModel.search.value) || t.VirtualReverseCustomerName.ToLower().Contains(inModel.search.value) || t.AddedBy.ToLower().Contains(inModel.search.value));
            }

            // Filters
            if (inModel.filters != null)
            {
                // Date Filter
                transactionsToShow = transactionsToShow.Where(t => t.Date >= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "StartDate").value.Split(' ')[0] + " 00:00:00") && t.Date <= DateTime.Parse(inModel.filters.SingleOrDefault(f => f.name == "EndDate").value.Split(' ')[0] + " 23:59:59"));

                foreach (var filter in inModel.filters.Where(f => f.name != "StartDate" && f.name != "EnDate"))
                {
                    if (filter.value == null || filter.value.Equals("*"))
                        continue;
                    if (filter.name.Equals("TransactionType"))
                    {
                        transactionsToShow = transactionsToShow.Where(t => t.TransactionType == filter.value);
                        continue;
                    }
                    if (filter.name.Equals("Agency"))
                    {
                        transactionsToShow = transactionsToShow.Where(t => t.AgencyId == int.Parse(filter.value));
                        continue;
                    }
                    if (filter.name.Equals("Operator"))
                    {
                        transactionsToShow = transactionsToShow.Where(t => t.OperatorId == int.Parse(filter.value));
                        continue;
                    }
                }
            }
            // Get transactions of the current day
            else
            {
                transactionsToShow = transactionsToShow.Where(t => t.Date >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0) && t.Date <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59));
                if (!user.Roles.Split('|').Contains("Admin"))
                    transactionsToShow = transactionsToShow.Where(t => t.AgencyId == user.AgencyId);

            }

            totalFiltered = transactionsToShow.Count();

            return (inModel.length == -1) ? Mapper.Map<IEnumerable<TransactionModel>>(transactionsToShow.OrderByDescending(c => c.Date)) : Mapper.Map<IEnumerable<TransactionModel>>(transactionsToShow.OrderByDescending(c => c.Date).Skip(inModel.start).Take(inModel.length));
        }

        public void SetTransactionFilePath(int transactionId, string filePath)
        {
            var transaction = _db.Transactions.SingleOrDefault(c => c.Id == transactionId && c.IsDeleted == false);
            if (transaction.FilePath != null)
            {
                if (!File.Exists(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\App_Data\\Transactions\\", transaction.FilePath)))
                {
                    throw new FileNotFoundException();
                }
                File.Delete(string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\App_Data\\Transactions\\", transaction.FilePath));
            }
            transaction.FilePath = filePath;
            _db.SaveChanges();
        }

        public IEnumerable<DashboardTransactionModel> GetDashboardTransactions(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            List<DashboardTransactionModel> dashboardTransactions = new List<DashboardTransactionModel>();

            foreach (var agency in company.Agencies.Where(a => a.IsDeleted == false))
            {
                dashboardTransactions.Add(new DashboardTransactionModel()
                {
                    AgencyName = agency.Name,
                    DailyTransactions = agency.Transactions.Where(t => t.Date.Date == DateTime.Now.Date && t.IsDeleted == false).Count(),
                    MonthlyTransactions = agency.Transactions.Where(t => t.Date.Month == DateTime.Now.Month && t.Date.Year == DateTime.Now.Year && t.IsDeleted == false).Count(),
                    MonthlyVolume = agency.Transactions.Where(t => t.Date.Month == DateTime.Now.Month && t.Date.Year == DateTime.Now.Year && t.IsDeleted == false).Sum(t => t.Amount)
                });
            }

            return dashboardTransactions.OrderByDescending(t => t.MonthlyTransactions);
        }

        public IEnumerable<TransactionModel> GetLastTransactions(int companyId)
        {
            Company company = _db.Companies.SingleOrDefault(c => c.Id == companyId);
            return Mapper.Map<IEnumerable<TransactionModel>>(company.Transactions.OrderByDescending(t => t.Date).Take(20));
        }

    }
}
